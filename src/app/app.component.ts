import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EvvServiceService } from './services/evv-service.service';
import { AppService } from './services/app.service';
import { Keepalive } from '@ng-idle/keepalive';
import { Router } from '@angular/router';
import { ModalDirective, BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
import { DashboardService } from './services/dashboard.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  responseData: any;
  urljson = 'assets/url.json';


  constructor(private http: HttpClient,
              private evvService: EvvServiceService,private dashBoradService:DashboardService,private idle: Idle, private modalService: BsModalService,
              private keepalive: Keepalive,private router: Router,private appService: AppService,private _dashboardService:DashboardService) {


                idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

                idle.onIdleEnd.subscribe(() => {
                  this.timePopupexist = true;
                  this.idleState = 'No longer idle.'
                  console.log(this.idleState);
                  this.reset();
                });

                idle.onTimeout.subscribe(() => {
                  // this.childModal.hide();
                  this.bsmodelRef.hide();
                  this.idleState = 'Session expired!';
                  this.timedOut = true;
                  console.log(this.idleState);
                  sessionStorage.clear();
                  setTimeout(() => {
                    console.log("Hello from setTimeout");
                    this.modalService._hideModal(1);
                    this.router.navigateByUrl('login');
                    this.appService.setUserLoggedIn(false);
                    window.location.reload();
                    this.router.navigateByUrl('login');

                  }, 100);
                });

                idle.onIdleStart.subscribe(() => {
                  console.log("popupflag", this.timePopupexist)
                  this.idleState = 'You\'ve gone idle!'
                  console.log(this.idleState);
                  // this.childModal.show();
                  this.timePopupexist ? this.bsmodelRef.hide() : ''
                  this.bsmodelRef = this.modalService.show(
                    this.template,
                    Object.assign({}, { class: 'timepopup' })
                  );
                  this.timePopupexist = true;
                });
                idle.onTimeoutWarning.subscribe((countdown) => {
                  this.idleState = 'You session will expire in ' + countdown + ' seconds! '
                  console.log(this.idleState);
                });
                // sets the ping interval to 15 seconds
                keepalive.interval(15);
                keepalive.onPing.subscribe(() => this.lastPing = new Date());
                this.appService.getUserLoggedIn().subscribe(userLoggedIn => {
                  if (userLoggedIn) {
                    idle.watch();
                    this.timedOut = false;
                  } else {
                    idle.stop();
                  }
                })

               }
  title = 'EVV-Portal';

  @ViewChild('template') template;
  childModal2: TemplateRef<any>
  public idleState = 'Not started.';
  public timedOut = false;
  public lastPing?: Date = null;
  public bsmodelRef: BsModalRef;
  public timePopupexist: boolean = false;

  ngOnInit() {
    this.http.get(this.urljson).subscribe(
      response => {
        this.responseData = response;
        const webserviceURL = this.responseData.webserviceURL;
        localStorage.setItem('webserviceURL', webserviceURL);
        let sessiontime = this.responseData.sessionTimeOut;
        console.log("from app component.ts  ")
        this.dashBoradService.geturl();
        this.evvService.geturl();
        this.idle.setIdle(sessiontime * 60)
        this.idle.setTimeout(10);
        console.log(this._dashboardService.loggedIn())
        if(this._dashboardService.loggedIn()){
          console.log('is login true')
          this.appService.setUserLoggedIn(true);
        }else{
          this.appService.setUserLoggedIn(false)
        }
      }
    )
  }

  public reset() {
    this.idle.watch();
    this.timedOut = false;
  }

  public hideChildModal(): void {
    this.bsmodelRef.hide();

  }

  public stay() {
    this.bsmodelRef.hide();
    this.reset();
  }
  public logout() {
    this.bsmodelRef.hide();
    this.appService.setUserLoggedIn(false);
    sessionStorage.removeItem('userData');
    setTimeout(() => {
      this.router.navigateByUrl('login');
      this.modalService._hideModal(1);
    }, 100);

  }
}

