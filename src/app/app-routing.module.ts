import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExportVisitsComponent } from './core/export-visits/export-visits.component';
import { InterfaceComponent } from './core/interface/interface.component';
import { InterfaceDatasetComponent } from './core/interface-dataset/interface-dataset.component';
import { InterfaceDatasetElementComponent } from './core/interface-dataset-element/interface-dataset-element.component';
import { LoginComponent } from './shared/login/login.component';
import { EvvDashboardComponent } from './core/evv-dashboard/evv-dashboard.component';
import { DatasetLogComponent } from './core/dataset-log/dataset-log.component';
import { ActivityLogComponent } from './core/activity-log/activity-log.component';
import { LookupListComponent } from './core/lookup-list/lookup-list.component';
import { InterfaceListComponent } from './core/interface-list/interface-list.component';
import { InterfaceViewComponent } from './core/interface-view/interface-view.component';
import { ErrorLogComponent } from './core/error-log/error-log.component';
import { ScreenComponent } from './core/screen/screen.component';
import { ReconcilationComponent } from './core/reconcilation/reconcilation.component';
import { InterfaceJobsComponent } from './core/interface-jobs/interface-jobs.component';
import { InterfaceOfficesComponent } from './core/interface-offices/interface-offices.component';
import { InterfaceTokenCredentialComponent } from './core/interface-token-credential/interface-token-credential.component';
import { VisitDeletedMediumComponent } from './core/visit-deleted-medium/visit-deleted-medium.component';
import { ResubmitTransactionComponent } from './core/resubmit-transaction/resubmit-transaction.component';


import { AutheniticationGuard } from './shared/authenitication.guard'
import { InterfaceJobsListComponent } from './core/interface-jobs-list/interface-jobs-list.component';
import { DashboardChartComponent } from './core/dashboard-chart/dashboard-chart.component';
import { WizardInterfaceComponent } from './core/wizard-interface/wizard-interface.component';
import { AnalyticsComponent } from './core/analytics/analytics.component';
import { MetricsSummeryComponent } from './core/metrics-summery/metrics-summery.component';
import { DbmsJobsComponent } from './core/dbms-jobs/dbms-jobs.component';
import { ExportEvvDataComponent } from './core/export-evv-data/export-evv-data.component';
import { ReconciliationPsComponent } from './core/reconciliation-ps/reconciliation-ps.component';
import { ReconciliationDcsComponent } from './core/reconciliation-dcs/reconciliation-dcs.component';
import { ResubmittransactionPsComponent } from './core/resubmittransaction-ps/resubmittransaction-ps.component';
import { ResubmittransactionDcsComponent } from './core/resubmittransaction-dcs/resubmittransaction-dcs.component';
import { ReconciliationScheduledVisitsComponent } from './core/reconciliation-scheduled-visits/reconciliation-scheduled-visits.component';
import { ResubmitScheduledVisitsComponent } from './core/resubmit-scheduled-visits/resubmit-scheduled-visits.component';
import { LaevvActivityLogComponent } from './core/laevv-activity-log/laevv-activity-log.component';
import { LaevvSummaryLogComponent } from './core/laevv-summary-log/laevv-summary-log.component';
import { ClientFileFromSriComponent } from './core/la-evv/client-file-from-sri/client-file-from-sri.component';
import { PaFileFromSriComponent } from './core/la-evv/pa-file-from-sri/pa-file-from-sri.component';
import { ClientPaFromSriComponent } from './core/la-evv/client-pa-from-sri/client-pa-from-sri.component';
import { ServiceMessageFileSriComponent } from './core/la-evv/service-message-file-sri/service-message-file-sri.component';
import { JobLogComponent } from './core/la-evv/job-log/job-log.component';
import { ProcessedPaRecordsComponent } from './core/la-evv/processed-pa-records/processed-pa-records.component';
import { EvvExportVisitsComponent } from './core/la-evv/evv-export-visits/evv-export-visits.component';
import { ExportDcsComponent } from './core/la-evv/export-dcs/export-dcs.component';
import { PendingReasonComponent } from './core/pending/pending-reason-completed/pending-reason.component';
import { PendingDashboardComponent } from './core/pending/pending-dashboard/pending-dashboard.component';

const routes: Routes = [
  { path: "dashboard", canActivate: [AutheniticationGuard], component: EvvDashboardComponent },
  { path: "activityLog/:datasetName/:interfaceDefinitionId/:interfaceDatasetId", canActivate: [AutheniticationGuard], component: ActivityLogComponent },
  { path: "activityLog/:datasetName/:interfaceDefinitionId", canActivate: [AutheniticationGuard], component: ActivityLogComponent },
  // { path: "datasetLog/:datasetName/:interfaceDefinitionId", canActivate: [AutheniticationGuard], component: DatasetLogComponent },
  { path: "errorlog/:datasetName/:interfaceDefinitionId", canActivate: [AutheniticationGuard], component: ErrorLogComponent },
  // { path:"lookUpList", component:LookupListComponent},
  { path: "interfaceList", canActivate: [AutheniticationGuard], component: InterfaceListComponent },
  { path: "view", canActivate: [AutheniticationGuard], component: InterfaceViewComponent },

  { path: 'view/:id/:IntrfaceId/:interfaceName', canActivate: [AutheniticationGuard], component: InterfaceViewComponent },


  { path: '', redirectTo: '/login', pathMatch: 'full' },

  { path: "login", component: LoginComponent },
  { path: "exports", canActivate: [AutheniticationGuard], component: ExportVisitsComponent },
  { path: "interface", canActivate: [AutheniticationGuard], component: InterfaceComponent },
  { path: "interface-dataset", canActivate: [AutheniticationGuard], component: InterfaceDatasetComponent },
  // {path:"interface-dataset-element",canActivate: [AutheniticationGuard],component:InterfaceDatasetElementComponent},
  { path: "interface-job-list", canActivate: [AutheniticationGuard], component: InterfaceJobsListComponent },
  { path: "dbms-jobs", canActivate: [AutheniticationGuard], component: DbmsJobsComponent },
  // {path:"interface-offices",component:InterfaceOfficesComponent},
  // {path:"interface-token",component:InterfaceTokenCredentialComponent},
  // {path:"visit-medium",component:VisitDeletedMediumComponent},
  { path: 'wizard', canActivate: [AutheniticationGuard], component: WizardInterfaceComponent },
  { path: "chart", canActivate: [AutheniticationGuard], component: DashboardChartComponent },
  { path: "analytics", canActivate: [AutheniticationGuard], component: AnalyticsComponent },
  { path: "metrics-summary", canActivate: [AutheniticationGuard], component: MetricsSummeryComponent },
  { path: "export-evv", canActivate: [AutheniticationGuard], component: ExportEvvDataComponent },
  { path: "reconcilation", canActivate: [AutheniticationGuard], component: ReconcilationComponent },
  { path: "reconcilation-schedule", canActivate: [AutheniticationGuard], component: ReconciliationScheduledVisitsComponent },
  { path: "reconciliation-ps", canActivate: [AutheniticationGuard], component: ReconciliationPsComponent },
  { path: "reconciliation-dcs", canActivate: [AutheniticationGuard], component: ReconciliationDcsComponent },
  { path: "resubmit-transaction", canActivate: [AutheniticationGuard], component: ResubmitTransactionComponent },
  { path: "resubmit-scheduled", canActivate: [AutheniticationGuard], component: ResubmitScheduledVisitsComponent },
  { path: "resubmit-ps", canActivate: [AutheniticationGuard], component: ResubmittransactionPsComponent },
  { path: "resubmit-dcs", canActivate: [AutheniticationGuard], component: ResubmittransactionDcsComponent },
  { path: "laevv-activity-log", canActivate: [AutheniticationGuard], component: LaevvActivityLogComponent },
  { path: "laevv-summary-log", canActivate: [AutheniticationGuard], component: LaevvSummaryLogComponent },
  {path:'client-file-from-sri',component:ClientFileFromSriComponent},
  {path:'client-pa-from-sri',component:ClientPaFromSriComponent},
  {path:'export-dcs',component:ExportDcsComponent},
  {path:'evv-export-visits',component:EvvExportVisitsComponent},
  // {path:'pa-details-from-sri',component:PaDetailsFrom},
  {path:'job-log',component:JobLogComponent},
  {path:'pa-file-from-sri',component:PaFileFromSriComponent},
  {path:'processed-pa-records',component:ProcessedPaRecordsComponent},
  {path:'service-message-file-sri',component:ServiceMessageFileSriComponent},
  {path:'pending-completed',component:PendingReasonComponent},
  {path:'pending-dashboard',component:PendingDashboardComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true, relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
