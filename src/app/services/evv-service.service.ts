import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})

export class EvvServiceService {
  httpOptions = {
    headers: new HttpHeaders({
      // 'Content-Type': 'application/json'
      'Content-Type': 'text/plain'

    }),

  };
  public webserviceUrl;
  public baseURL: string;

  constructor(private http: HttpClient) {
    //  this.baseURL = environment.baseUrl.url;
    this.geturl();
  }
  geturl() {
    this.webserviceUrl = localStorage.getItem('webserviceURL');
    this.baseURL = this.webserviceUrl + '/maintenancerestapi/';
  }
  public getInterfaceDetailsList(data: any): Observable<object> {
    return this.http.get<object>(this.baseURL + 'getInterfaceDetailsList?jsonObject=' + data, { responseType: 'json' }).pipe(catchError(this.errorHandler));
  }

  public getEvvDashBoardData(data: any): Observable<object> {
    return this.http.get<object>(this.webserviceUrl + "/dashboardrestapi/getDashboardDetails?jsonObj=" + data, { responseType: 'json' }).pipe(catchError(this.errorHandler));

  }
  public getInterfaceLookUp(data: any): Observable<object> {
    return this.http.get<object>(this.baseURL + 'getLookupList?jsonObj=' + data, { responseType: 'json' }).pipe(catchError(this.errorHandler));
  }
  public getInterfaceDetails(id: any): Observable<any> {
    return this.http.get<object>(this.baseURL + 'getInterfaceDetails?id=' + id, { responseType: 'json' }).pipe(catchError(this.errorHandler));
  }
  public createInterfaceByUserId(userId: any, payload): Observable<any> {
    return this.http.post(this.baseURL + 'saveInterfaceDetails?userId=' + userId, payload, { responseType: 'json' }).pipe(catchError(this.errorHandler));
  }
  public getInterfaceDetailsDataSetsList(data: any): Observable<any> {
    return this.http.get<object>(this.baseURL + 'getInterfaceDatasetList?jsonObject=' + data, { responseType: 'json' }).pipe(catchError(this.errorHandler));
  }
  public getInterfaceDetailsDataSetById(id: any): Observable<any> {
    return this.http.get<any>(this.baseURL + 'getInterfaceDataset?id=' + id, { responseType: 'json' }).pipe(catchError(this.errorHandler));
  }
  public createInterfaceDataSetByUserId(userId: any, payload): Observable<any> {
    return this.http.post(this.baseURL + 'saveInterfaceDataset?userId=' + userId, payload, { responseType: 'json' }).pipe(catchError(this.errorHandler));
  }
  public getLookUpData(str): Observable<any> {
    return this.http.get<object>(this.baseURL+`getLookupData?tableNames=` + str, { responseType: 'json' }).pipe(catchError(this.errorHandler));
  }

  public getInterfaceElementsList(data: any): Observable<object> {
    return this.http.get<object>(this.baseURL + 'getInterfaceElements?jsonObject=' + data, { responseType: 'json' }).pipe(catchError(this.errorHandler));
  }

  public getInterfaceElementById(id): Observable<object> {
    return this.http.get<object>(this.baseURL + 'getInterfaceElements?id=' + id, { responseType: 'json' }).pipe(catchError(this.errorHandler));
  }

  public saveInterfaceElements(id: any, interfaceDefinitionId: any, interfaceDatasetId: any, userId: any, payload): Observable<any> {

    return this.http.post(this.baseURL + 'saveInterfaceElements?id=' + id + '&interfaceDefinitionId=' +
      interfaceDefinitionId + '&interfaceDatasetId=' + interfaceDatasetId + '&userId=' + userId, payload, { responseType: 'json' }).pipe(catchError(this.errorHandler));
  }

  public getLookUpDataForElements(): Observable<object> {
    return this.http.get<object>(this.baseURL + 'getLookupData?tableNames=data_type,date_format,lookup', { responseType: 'json' }).pipe(catchError(this.errorHandler));
  }

  public getPOCDataSets(): Observable<object> {
    return this.http.get<object>(this.baseURL + 'getPOCDataSets', { responseType: 'json' }).pipe(catchError(this.errorHandler));
  }
  public getPOCElements(): Observable<object> {
    return this.http.get<object>(this.baseURL + 'getPOCElements', { responseType: 'json' }).pipe(catchError(this.errorHandler));
  }

  public authenticateUser(payload: any): Observable<object> {
    return this.http.post<object>(this.webserviceUrl + '/authenticationrestapi/authenticateUser', payload, this.httpOptions).pipe(catchError(this.errorHandler));
  }
  public getLookupDetailsList(params): Observable<any> {
    return this.http.get<object>(this.baseURL + 'getLookupDetailsList?jsonObj=' + params, { responseType: 'json' }).pipe(catchError(this.errorHandler));
  }
  public saveLookupDetails(params) {
    return this.http.post(this.baseURL + 'saveLookupDetails', params, { responseType: 'json' }).pipe(catchError(this.errorHandler));

  }
  public saveLookup(params) {
    return this.http.post(this.baseURL + 'saveLookup', params, { responseType: 'json' }).pipe(catchError(this.errorHandler));

  }
  public getLookupById(params) {
    return this.http.get<object>(this.baseURL + 'getLookupById?jsonObj=' + params, { responseType: 'json' }).pipe(catchError(this.errorHandler));

  }
  public getLookupDetailsById(params) {
    return this.http.get<object>(this.baseURL + 'getLookupDetailsById?jsonObj=' + params, { responseType: 'json' }).pipe(catchError(this.errorHandler));

  }

  public getExportPOCLookupList() {
    return this.http.get<object>(this.baseURL + 'getExportPOCLookupList', { responseType: 'json' }).pipe(catchError(this.errorHandler));
  }
  public getExportPOCLookupDetailsById(params) {
    return this.http.get<object>(this.baseURL + 'getExportPOCLookupDetailsById?jsonObj=' + params, { responseType: 'json' }).pipe(catchError(this.errorHandler));
  }
  public saveExportLookupMapping(params) {
    return this.http.post(this.baseURL + 'saveExportLookupMapping', params, { responseType: 'json' }).pipe(catchError(this.errorHandler));

  }
  public deleteInterfaceElementsDetails(paramas){
    return this.http.get(this.webserviceUrl+"/maintenancerestapi/deleteInterfaceElementsDetails?id="+paramas).pipe(catchError(this.errorHandler))

   }
   public getInterfaceExportedTables():Observable<any>{
    return this.http.get(this.webserviceUrl+`/maintenancerestapi/getInterfaceExportedTables`).pipe(catchError(this.errorHandler));
  }
  private errorHandler(error: HttpErrorResponse) {
    console.log("error in EVV service", error);
    return throwError(error);
  }

}
