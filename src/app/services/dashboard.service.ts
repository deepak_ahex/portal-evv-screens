import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  public httpOptions = {
    headers: new HttpHeaders({
      // 'Content-Type': 'application/json'
      'Content-Type': 'text/plain'

    }),

  };
  public headerInterfaceDatsetName: string = '';
  public webserviceUrl: string;
  public baseURL: string;
  constructor(
    private http: HttpClient
  ) {
    this.geturl();
  }
  //dashboard service
  geturl() {
    this.webserviceUrl = localStorage.getItem('webserviceURL');
    console.log("++", this.webserviceUrl)
    this.baseURL = this.webserviceUrl + '/activitylogrestapi/';
  }

  getDatsetName() {
    return this.headerInterfaceDatsetName;
  }

  public getActivityLogList(params) {
    this.geturl();

    return this.http.get(this.baseURL + 'getActivityLogList?jsonObject=' + params).pipe(catchError(this.errorHandler));
  }
  public getActivityErrorLogList(params) {
    return this.http.get(this.baseURL + 'getActivityErrorLogList?jsonObject=' + params).pipe(catchError(this.errorHandler));
  }

  public getInterfaceDatasetList(id): Observable<any> {
    let obj = { "name": "", "active": "", "interfaceDefinitionId": id }
    console.log("Hello")
    this.geturl();
    return this.http.get(this.webserviceUrl + '/maintenancerestapi/getInterfaceDatasetList?jsonObject=' + JSON.stringify(obj)).pipe(catchError(this.errorHandler));
  }

  public getAttachmentContent(id) {
    return this.http.get(this.baseURL + 'getAttachmentContent?attachmentId=' + id).pipe(catchError(this.errorHandler));
  }
  public getBatchDetails(id, interfaceBatchInfoId) {
    return this.http.get(this.baseURL + 'getBatchDetails?attachmentId=' + id + '&interfaceBatchInfoId=' + interfaceBatchInfoId).pipe(catchError(this.errorHandler));
  }


  public getReconciliationCompVisitList(params) {
    return this.http.get(this.baseURL + 'getReconciliationCompVisitList?jsonObject=' + params).pipe(catchError(this.errorHandler))
  }
  public getReconciliationCompVisitLog(params):Observable<any>{
    return this.http.get(this.baseURL + 'getReconciliationCompVisitLog?jsonObject=' + params).pipe(catchError(this.errorHandler))

  }
  public getReconciliationSchdVisitList(params){
    return this.http.get(this.baseURL + 'getReconciliationSchdVisitList?jsonObject=' + params).pipe(catchError(this.errorHandler))

  }
  public getFilterDataByDataSet(id,datasetName): Observable<any> {
    return this.http.get(this.webserviceUrl +`/activitylogrestapi/getFilterDataByDataSet?jsonObject={"interfaceDefinitionId":${id},"dataSetName":${datasetName}}`).pipe(catchError(this.errorHandler))

  }


  public getjobs() {
    return this.http.get('assets/job_runtime.json')
  }

  public getInterfaceStaticValuesList(id): Observable<any> {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getInterfaceStaticValuesList?jsonObject={"interfaceDefinitionId":${id}}`).pipe(catchError(this.errorHandler))
  }
  public getInterfacePayerDetailsList(id): Observable<any> {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getInterfacePayerDetailsList?jsonObject={"interfaceDefinitionId":${id}}`).pipe(catchError(this.errorHandler))
  }
  public saveInterfaceStaticValue(obj): Observable<any> {
    return this.http.post(this.webserviceUrl + `/maintenancerestapi/saveInterfaceStaticValues`, obj, this.httpOptions).pipe(catchError(this.errorHandler))
  }
  public getInterfacePayerDetailsById(id) {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getInterfacePayerDetailsById?jsonObject={"id":${id}}`).pipe(catchError(this.errorHandler))

  }
  public saveInterfacePayerDetails(obj) {
    return this.http.post(this.webserviceUrl + `/maintenancerestapi/saveInterfacePayerDetails`, obj, this.httpOptions).pipe(catchError(this.errorHandler))

  }

  public getInterfaceServicesList(id, interfacePayerDetailsId) {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getInterfaceServicesList?jsonObject={"interfaceDefinitionId":${id},"interfacePayerDetailsId":${interfacePayerDetailsId}}`).pipe(catchError(this.errorHandler))

  } public getInterfaceServicesById(id) {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getInterfaceServicesById?jsonObject={"id":${id}}`).pipe(catchError(this.errorHandler))

  }
  public saveInterfaceServices(obj) {
    return this.http.post(this.webserviceUrl + `/maintenancerestapi/saveInterfaceServices`, obj, this.httpOptions).pipe(catchError(this.errorHandler))

  }
  public getInterfacePocPayersMapList(id) {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getInterfacePocPayersMapList?jsonObject={"interfacePayerDetailsId":${id}}`).pipe(catchError(this.errorHandler))

  }
  public getIntarfacePayorMappingById(id) {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getIntarfacePayorMappingById?jsonObject={"id":${id}}`).pipe(catchError(this.errorHandler))
  }

  public saveInterfacePayorMapping(obj) {
    return this.http.post(this.webserviceUrl + `/maintenancerestapi/saveInterfacePocPayerMapping`, obj, this.httpOptions).pipe(catchError(this.errorHandler))

  }

  public getInterfaceOffices(id) {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getInterfaceOffices?jsonObject={"interfaceDefinitionId":${id}}`).pipe(catchError(this.errorHandler))

  }
  public getPayorPlanList(id) {

    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getPayorPlanList?jsonObject={"officeId":${id}}`).pipe(catchError(this.errorHandler))

  }

  public getInterfacePocServiceMapList(id): Observable<any> {
    return this.http.get(`${this.webserviceUrl}/maintenancerestapi/getInterfacePocServiceMapList?jsonObject={"interfaceServicesId":${id}}`)
  }

  public getMappedPayorPlanList(officeId, interfaceDefinitionId): Observable<any> {
    return this.http.get(`${this.webserviceUrl}/maintenancerestapi/getMappedPayorPlanList?jsonObject={"officeId":${officeId},"interfaceDefinitionId":${interfaceDefinitionId}}`)

  }
  public getServiceList(officeId, payorPlanId): Observable<any> {
    return this.http.get(`${this.webserviceUrl}/maintenancerestapi/getServiceList?jsonObject={"officeId":${officeId},"payorPlanId":${payorPlanId}}`)
  }
  public saveInterfacePocServiceMap(obj): Observable<any> {
    return this.http.post(`${this.webserviceUrl}/maintenancerestapi/saveInterfacePocServiceMap`, obj, this.httpOptions).pipe(catchError(this.errorHandler))
  }


  public getProviderDetailsList(id): Observable<any> {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getProviderDetailsList?jsonObject={"interfaceDefinitionId":${id}}`).pipe(catchError(this.errorHandler))

  }
  public saveProviderDetails(obj): Observable<any> {
    return this.http.post(this.webserviceUrl + `/maintenancerestapi/saveProviderDetails`, obj, this.httpOptions).pipe(catchError(this.errorHandler))

  }
  public getInterfaceMediumDetailsList(id): Observable<any> {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getInterfaceMediumDetailsList?jsonObject={"interfaceDefinitionId":${id}}`).pipe(catchError(this.errorHandler))

  }
  public saveInterfaceMediumDetails(obj): Observable<any> {
    return this.http.post(`${this.webserviceUrl}/maintenancerestapi/saveInterfaceMediumDetails`, obj, this.httpOptions).pipe(catchError(this.errorHandler))
  }
  public getInterfaceOfficesList(id): Observable<any> {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getInterfaceOfficesList?jsonObject={"interfaceDefinitionId":${id}}`).pipe(catchError(this.errorHandler))

  }

  public createInterfaceOffices(obj) {
    return this.http.post(this.webserviceUrl + `/maintenancerestapi/createInterfaceOffices`, obj, this.httpOptions).pipe(catchError(this.errorHandler))
  }
  public saveInterfaceOffices(obj): Observable<any> {
    return this.http.post(`${this.webserviceUrl}/maintenancerestapi/saveInterfaceOffices`, obj, this.httpOptions).pipe(catchError(this.errorHandler))
  }
  public getInterfaceTokenCredentialsList(id): Observable<any> {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getInterfaceTokenCredentialsList?jsonObject={"interfaceDefinitionId":${id}}`).pipe(catchError(this.errorHandler))

  }

  public saveInterfaceTokenCredentials(obj): Observable<any> {
    return this.http.post(this.webserviceUrl + `/maintenancerestapi/saveInterfaceTokenCredentials`, obj, this.httpOptions).pipe(catchError(this.errorHandler));
  }

  public getVisitDeletedMediumDetailsList(id): Observable<any> {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getVisitDeletedMediumDetailsList?jsonObject={"interfaceDefinitionId":${id}}`).pipe(catchError(this.errorHandler))
  }

  public saveVisitDeleteMediumDetails(obj): Observable<any> {
    return this.http.post(this.webserviceUrl + `/maintenancerestapi/saveVisitDeleteMediumDetails`, obj, this.httpOptions).pipe(catchError(this.errorHandler));

  }
  public getInterfaceDocumentationList(id): Observable<any> {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getInterfaceDocumentationList?jsonObject={"interfaceDefinitionId":${id}}`).pipe(catchError(this.errorHandler))

  }
  public saveInterfaceDocumentation(obj): Observable<any> {
    return this.http.post(this.webserviceUrl + `/maintenancerestapi/saveInterfaceDocumentation`, obj, this.httpOptions).pipe(catchError(this.errorHandler));

  }
  public getInterfaceDocument(id): Observable<any> {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getInterfaceDocument?jsonObject={"interfaceAttachmentId":${id}}`).pipe(catchError(this.errorHandler))

  }
  public getInterfaceJobRunTimesList(id): Observable<any> {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getInterfaceJobRunTimesList?jsonObject={"interfaceDefinitionId":${id}}`).pipe(catchError(this.errorHandler))
  }

  public getInterfaceJobLog(interfaceId, id): Observable<any> {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getInterfaceJobLog?jsonObject={"interfaceDefinitionId":${interfaceId},"interfaceDatasetId":${id}}`).pipe(catchError(this.errorHandler))

  }

  public getPOCOffices(): Observable<any> {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getPOCOffices`).pipe(catchError(this.errorHandler))
  }
  public getInterfaceExportedTables(): Observable<any> {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getInterfaceExportedTables`).pipe(catchError(this.errorHandler));
  }
  private errorHandler(error: HttpErrorResponse) {
    console.log("error in EVV dashbord service", error);
    Swal.fire({
      html: error.error,
      icon: 'error'
    })
    return throwError(error);
  }


  public getPSProviderDetailsList(id): Observable<any> {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getPSProviderDetailsList?jsonObject={"interfaceDefinitionId":${id}}`).pipe(catchError(this.errorHandler))

  }
  public savePSProviderDetails(obj): Observable<any> {
    return this.http.post(this.webserviceUrl + `/maintenancerestapi/savePSProviderDetails`, obj, this.httpOptions).pipe(catchError(this.errorHandler))

  }
  public getPSList(id): Observable<any> {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getPSList?jsonObject={"officeId":${id}}`).pipe(catchError(this.errorHandler))

  }


  public getDCSProviderDetailsList(id): Observable<any> {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getDCSProviderDetailsList?jsonObject={"interfaceDefinitionId":${id}}`).pipe(catchError(this.errorHandler))

  }

  public saveDCSProviderDetails(obj): Observable<any> {
    return this.http.post(this.webserviceUrl + `/maintenancerestapi/saveDCSProviderDetails`, obj, this.httpOptions).pipe(catchError(this.errorHandler))

  }
  public getDCSList(id): Observable<any> {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getDCSList?jsonObject={"officeId":${id}}`).pipe(catchError(this.errorHandler))

  }

  public getOfficeProviderDetails(interfaceId, officeId): Observable<any> {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getOfficeProviderDetails?jsonObject={"interfaceDefinitionId":${interfaceId},"officeId":${officeId}}`).pipe(catchError(this.errorHandler))
  }

  public getStatusMediumDetailsList(id): Observable<any> {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getStatusMediumDetailsList?jsonObject={"interfaceDefinitionId":${id}}`).pipe(catchError(this.errorHandler))

  }

  public saveStatusMediumDetails(obj): Observable<any> {
    return this.http.post(this.webserviceUrl + `/maintenancerestapi/saveStatusMediumDetails`, obj, this.httpOptions).pipe(catchError(this.errorHandler))

  }
  public loggedIn(): boolean {
    return !!sessionStorage.getItem('userData');
  }

  public getExportedDataDetails(id, interfaceDefinationId, interfaceDatasetId,tableName): Observable<any> {
    return this.http.get(this.baseURL + `getExportedDataDetails?jsonObject={"id":${id},"interfaceDefinitionId":${interfaceDefinationId},"interfaceDatasetId":${interfaceDatasetId},"tableName":"${tableName}"}`).pipe(catchError(this.errorHandler))
  }

  public getRejectedVisitList(obj): Observable<any> {
    return this.http.get(this.webserviceUrl + `/activitylogrestapi/getRejectedVisitList?jsonObject=` + obj).pipe(catchError(this.errorHandler));
  }
  public getCorrectionFileds(obj): Observable<any> {
    return this.http.get(this.webserviceUrl + `/activitylogrestapi/getCorrectionFileds?jsonObject=` + obj).pipe(catchError(this.errorHandler));
  }
  public resubmitCorrectionRecord(obj): Observable<any> {
    return this.http.post(this.webserviceUrl + `/activitylogrestapi/resubmitCorrectionRecord`, obj, this.httpOptions).pipe(catchError(this.errorHandler));
  }
  public repushRejectedRecords(obj): Observable<any> {
    return this.http.post(this.webserviceUrl + `/activitylogrestapi/repushRejectedRecords`, obj, this.httpOptions).pipe(catchError(this.errorHandler));
  }
  public getEVVMetrics(obj): Observable<any> {
    return this.http.get(this.webserviceUrl + '/activitylogrestapi/getEVVMetrics?jsonObject=' + obj).pipe(catchError(this.errorHandler));
  }
  public getEVVMetricsDetails(obj): Observable<any> {
    return this.http.get(this.webserviceUrl + '/activitylogrestapi/getEVVMetricsDetails?jsonObject=' + obj).pipe(catchError(this.errorHandler));
  }
  public getEVVMetricsSummery(obj): Observable<any> {
    return this.http.get(this.webserviceUrl + '/activitylogrestapi/getEVVMetricsSummery?jsonObject=' + obj).pipe(catchError(this.errorHandler));
  }
  public getEVVMetricsSummeryDetails(obj): Observable<any> {
    return this.http.get(this.webserviceUrl + '/activitylogrestapi/getEVVMetricsSummeryDetails?jsonObject=' + obj).pipe(catchError(this.errorHandler));
  }
  public updateInterfacejobRunTimes(obj): Observable<any> {
    return this.http.post(this.webserviceUrl + `/maintenancerestapi/updateInterfacejobRunTimes`, obj, this.httpOptions).pipe(catchError(this.errorHandler));

  }
  public getInterfaceSchedulerJobs(): Observable<any> {
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/getInterfaceSchedulerJobs`).pipe(catchError(this.errorHandler))

  }
  public runSchedulerJob(obj): Observable<any> {
    console.log(obj);
    return this.http.get(this.webserviceUrl + `/maintenancerestapi/runSchedulerJob?jsonObject=${obj}`).pipe(catchError(this.errorHandler))

  }
  public getExportingVisitList(obj): Observable<any> {
    return this.http.get(this.webserviceUrl + `/exportapi/getExportingVisitList?jsonObj=${obj}`).pipe(catchError(this.errorHandler))

  } public getFileFormatDetails(id): Observable<any> {
    return this.http.get(this.webserviceUrl + `/exportapi/getFileFormatDetails?interfaceDatasetId=${id}`).pipe(catchError(this.errorHandler))

  } public getInterfaceList(): Observable<any> {
    return this.http.get(this.webserviceUrl + `/exportapi/getInterfaceList?downloadFileFlag=1`).pipe(catchError(this.errorHandler))

  }
  public saveDownloadingVisitData(obj): Observable<any> {
    return this.http.post(this.webserviceUrl + `/exportapi/saveDownloadingVisitData`, obj, this.httpOptions).pipe(catchError(this.errorHandler));

  }
  public getInterfaceByDataSet(datasetName):Observable<any>{
    return this.http.get(this.webserviceUrl+`/activitylogrestapi/getInterfaceByDataSet?jsonObject={"dataSetName":${datasetName}}`).pipe(catchError(this.errorHandler));
  }

  public getReconciliationPSList(obj):Observable<any>{
    return this.http.get(this.webserviceUrl+`/activitylogrestapi/getReconciliationPSList?jsonObject=${obj}`).pipe(catchError(this.errorHandler));
  }
  public getReconciliationDCSList(obj):Observable<any>{
    return this.http.get(this.webserviceUrl+`/activitylogrestapi/getReconciliationDCSList?jsonObject=${obj}`).pipe(catchError(this.errorHandler));

  }
  public getResubmitSchdVisitList(obj):Observable<any>{
    return this.http.get(this.webserviceUrl+`/activitylogrestapi/getResubmitSchdVisitList?jsonObject=${obj}`).pipe(catchError(this.errorHandler))
  }
  public resubmitCorrectionSchdVisitRec(obj):Observable<any>{
    return this.http.post(this.webserviceUrl + `/activitylogrestapi/resubmitCorrectionSchdVisitRec`, obj, this.httpOptions).pipe(catchError(this.errorHandler));

  }
  public repushSchdVisitRecords(obj):Observable<any>{
    return this.http.post(this.webserviceUrl + `/activitylogrestapi/repushRejectedRecords`, obj, this.httpOptions).pipe(catchError(this.errorHandler));
  }
  public getResubmitPSList(obj):Observable<any>{
    return this.http.get(this.webserviceUrl+`/activitylogrestapi/getResubmitPSList?jsonObject=${obj}`).pipe(catchError(this.errorHandler))
  }


  public getResubmitDCSList(obj):Observable<any>{
    return this.http.get(this.webserviceUrl+`/activitylogrestapi/getResubmitDCSList?jsonObject=${obj}`).pipe(catchError(this.errorHandler))
  }
 public repushPSRecords(obj):Observable<any>{
   return this.http.get(this.webserviceUrl+`/activitylogrestapi/repushPSRecords?jsonObject=${obj}`).pipe(catchError(this.errorHandler))
 }
 public repushDCSRecords(obj):Observable<any>{
  return this.http.get(this.webserviceUrl+`/activitylogrestapi/repushDCSRecords?jsonObject=${obj}`).pipe(catchError(this.errorHandler))
}
public dismissRejectedRecords(obj):Observable<any>{
  return this.http.post(this.webserviceUrl+`/activitylogrestapi/dismissRejectedRecords`,obj,this.httpOptions).pipe(catchError(this.errorHandler))

}



//below all calls are Related to LAEVV sscreens

public getLAEVVActivityList(obj):Observable<any>{
  return this.http.get(this.webserviceUrl+`/laevvrestapi/getLAEVVActivityList?jsonObject=${obj}`).pipe(catchError(this.errorHandler))
}

public getBatchDetailsLaEvv(attachmentId,laevvBatchInfoId):Observable<any>{
  return this.http.get(this.webserviceUrl+`/laevvrestapi/getBatchDetails?attachmentId=${attachmentId}&laevvBatchInfoId=${laevvBatchInfoId}`).pipe(catchError(this.errorHandler))
}

public getSummaryCounts():Observable<any>{
  return this.http.get(this.webserviceUrl+`/laevvrestapi/getSummaryCounts`).pipe(catchError(this.errorHandler))
}
public getClientList():Observable<any>{
  return this.http.get(this.webserviceUrl+`/laevvrestapi/getClientList`).pipe(catchError(this.errorHandler))
}
public getClientAltList():Observable<any>{
  return this.http.get(this.webserviceUrl+`/laevvrestapi/getClientAltList`).pipe(catchError(this.errorHandler))
}
public getClientNoPSList():Observable<any>{
  return this.http.get(this.webserviceUrl+`/laevvrestapi/getClientNoPSList`).pipe(catchError(this.errorHandler))
}
public getAuthServiceMatchedList():Observable<any>{
  return this.http.get(this.webserviceUrl+`/laevvrestapi/getAuthServiceMatchedList`).pipe(catchError(this.errorHandler))
}
public getAuthServiceNotMatchedList():Observable<any>{
  return this.http.get(this.webserviceUrl+`/laevvrestapi/getAuthServiceNotMatchedList`).pipe(catchError(this.errorHandler))
}
public getTotalAuthUnitsNotMatchedList():Observable<any>{
  return this.http.get(this.webserviceUrl+`/laevvrestapi/getTotalAuthUnitsNotMatchedList`).pipe(catchError(this.errorHandler))
}
public getWeeklyUnitsNotMatchedList():Observable<any>{
  return this.http.get(this.webserviceUrl+`/laevvrestapi/getWeeklyUnitsNotMatchedList`).pipe(catchError(this.errorHandler))
}
public getUnMatchedAuths():Observable<any>{
  return this.http.get(this.webserviceUrl+`/laevvrestapi/getUnMatchedAuths`).pipe(catchError(this.errorHandler))
}
public getRejectedBySRIVisitList(obj):Observable<any>{
  return this.http.get(this.webserviceUrl+`/laevvrestapi/getRejectedBySRIVisitList?jsonObject=${obj}`).pipe(catchError(this.errorHandler))
}
public getRejectedBySRIVisitLog(obj):Observable<any>{
  return this.http.get(this.webserviceUrl+`/laevvrestapi/getRejectedBySRIVisitLog?jsonObject=${obj}`).pipe(catchError(this.errorHandler))
}

}

