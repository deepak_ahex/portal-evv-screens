import { TestBed } from '@angular/core/testing';

import { EvvServiceService } from './evv-service.service';

describe('EvvServiceService', () => {
  let service: EvvServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EvvServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
