import { Component, OnInit } from '@angular/core';
declare var $:any;
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor() {
    this.icons();
   }

  ngOnInit(): void {
    this.icons();
  }
  icons(){
    $(".sublist-wrapper").click(function(){
      // $('.side-menu-inner-item:has(.active)')
      if($(this).hasClass("active")){
        $(this).siblings().removeClass("active");
        $(this).parent().parent().addClass("active");
      }
      if($(this).parent().parent().siblings().hasClass("active")){
        $(this).parent().parent().siblings().removeClass("active");
      }
    });
  }
}