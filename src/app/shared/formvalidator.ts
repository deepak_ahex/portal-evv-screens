import { AbstractControl } from '@angular/forms'

export function formTrimValidation(control: AbstractControl): { [key: string]: any } | null {
  if (control.value != null) {
    const length = control.value.trim().length;
    return length > 0 ? { 'forbidenname': { value: control.value } } : null;
  }
  else{
    return { 'forbidenname': { value: null } };
  }
}
