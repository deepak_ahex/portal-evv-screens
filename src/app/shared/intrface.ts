export interface job_log   {
  "ID": number,
  "INTERFACE_DATASET_MAP_ID": number,
  "PROCEDURE_NAME": string
  "METHOD_NAME": string
  "RESPONSE_MSG": string,
  "RESPONSE_DATE": string,
  "INTERFACE_DEFINITION_ID": number,
  "INTERFACE_DATASET_ID": number,
  "UPDATED_USER": string,
  "LAST_UPDATED": string,
  "DATA_FLOW_ID": string,
  "IS_JOB_COMPLETED": number
}
