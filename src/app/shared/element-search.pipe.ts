import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'elementSearch'
})
export class ElementSearchPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (!args) {
      return value;
    }
    console.log(value)
    return value.filter((val) => {
      let rVal = (val.interfaceDatasetName.toLocaleLowerCase().includes(args)) || (val.interfaceDatasetName.toLocaleLowerCase().includes(args));
      return rVal;
    })

  }

}