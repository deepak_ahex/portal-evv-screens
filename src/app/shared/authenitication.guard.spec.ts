import { TestBed } from '@angular/core/testing';

import { AutheniticationGuard } from './authenitication.guard';

describe('AutheniticationGuard', () => {
  let guard: AutheniticationGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AutheniticationGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
