import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/services/app.service';
import { DashboardService } from 'src/app/services/dashboard.service';
import { EvvServiceService } from 'src/app/services/evv-service.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private appService: AppService,private dashboardService: DashboardService,
              private evvServiceService: EvvServiceService) {
                if (this.dashboardService.loggedIn()) {
                  this.router.navigateByUrl('/dashboard');
                }
              }
  public payLoad: any = {};
  public submitted = false;
  public ShowPassword = false;

  ngOnInit(): void {
  }

  login(): void {
    this.submitted = true;
    this.evvServiceService.authenticateUser(JSON.stringify(this.payLoad)).subscribe(res => {
      const loginData = JSON.stringify(res);
      const JsonData = JSON.parse(loginData);
      if (!JsonData.errorMsg) {
        this.appService.setUserLoggedIn(true)
        sessionStorage.setItem('userData', JSON.stringify(res));
        this.router.navigate(['/dashboard']);
      }else{
        Swal.fire({
          title:'warning',
          text: JsonData.errorMsg,
          icon: 'warning',
          timer:1200,
          showConfirmButton: false
        });
        // alert(JsonData.errorMsg);
      }
    });
  }
  public showpassword() {
    this.ShowPassword = !this.ShowPassword;
  }
}
