import { Component, OnInit ,Input} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService } from 'src/app/services/app.service';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
public dataSetName: string='';


  constructor(private router: Router,private activatedRoute: ActivatedRoute,private appService:AppService,public dashboardService:DashboardService) {
    // this.dataSetName= this.activatedRoute.snapshot.params.datasetName;

   }
userData: any;
  ngOnInit(): void {
this.userData=JSON.parse(sessionStorage.getItem('userData'));
  }
logout(){
  this.appService.setUserLoggedIn(false);
  sessionStorage.removeItem('userData');

  this.router.navigate(['/login']);
}
}
