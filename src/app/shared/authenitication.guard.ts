import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AppService } from '../services/app.service';
import { DashboardService } from '../services/dashboard.service';

@Injectable({
  providedIn: 'root'
})
export class AutheniticationGuard implements CanActivate {

  constructor(private dashboardService: DashboardService,private appService:AppService,
    private _router: Router,){}
  canActivate(): boolean {
    console.log("canactivate working")
    if (this.dashboardService.loggedIn()) {
      return true;
    } else {
      this._router.navigateByUrl('/login');
      this.appService.setUserLoggedIn(false);
      return false;
    }

  }

}
