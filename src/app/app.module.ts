import { BrowserModule } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InterfaceComponent } from './core/interface/interface.component';
import { InterfaceDatasetComponent } from './core/interface-dataset/interface-dataset.component';
import { InterfaceDatasetElementComponent } from './core/interface-dataset-element/interface-dataset-element.component';
import { ExportVisitsComponent } from './core/export-visits/export-visits.component';
import { LoginComponent } from './shared/login/login.component';
import { HeaderComponent } from './shared/header/header.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { ModalModule, BsModalRef } from 'ngx-bootstrap/modal';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SearchPipe } from './shared/search.pipe';
import { ElementSearchPipe } from './shared/element-search.pipe';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { EvvDashboardComponent } from './core/evv-dashboard/evv-dashboard.component';
import { DatasetLogComponent } from './core/dataset-log/dataset-log.component';
import { ActivityLogComponent } from './core/activity-log/activity-log.component';
import { LookupListComponent } from './core/lookup-list/lookup-list.component';
import { InterfaceListComponent } from './core/interface-list/interface-list.component';
import { InterfaceViewComponent } from './core/interface-view/interface-view.component';
import { NgxSpinnerModule, NgxSpinnerService } from "ngx-spinner";
import { HttpInterceptorService } from './services/http-interceptor.service';
import { ErrorLogComponent } from './core/error-log/error-log.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { DatePipe, APP_BASE_HREF, TitleCasePipe } from '@angular/common';
import { DomsanitizePipe } from '../app/shared/pipes/domsanitize.pipe';
import { ScreenComponent } from './core/screen/screen.component';
import { ReconcilationComponent } from './core/reconcilation/reconcilation.component';
import { OrderModule } from 'ngx-order-pipe';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { InterfaceJobsComponent } from './core/interface-jobs/interface-jobs.component';
import { PayorDetailsComponent } from './core/payor-details/payor-details.component';
import { InterfacePayorMappingComponent } from './core/interface-payor-mapping/interface-payor-mapping.component';
import { InterfacePocServicesMapComponent } from './core/interface-poc-services-map/interface-poc-services-map.component';
import { InterfaceServicesComponent } from './core/interface-services/interface-services.component';
import { InterfaceOfficeDetailsComponent } from './core/interface-office-details/interface-office-details.component';
import { InterfaceMediumComponent } from './core/interface-medium/interface-medium.component';
import { InterfaceOfficesComponent } from './core/interface-offices/interface-offices.component';
import { InterfaceTokenCredentialComponent } from './core/interface-token-credential/interface-token-credential.component';
import { VisitDeletedMediumComponent } from './core/visit-deleted-medium/visit-deleted-medium.component';
import { ResubmitTransactionComponent } from './core/resubmit-transaction/resubmit-transaction.component';
import { DcoumentComponent } from './core/dcoument/dcoument.component';
import { InterfaceProviderDetailsComponent } from './core/interface-provider-details/interface-provider-details.component';
import { InterfaceStaticValuesComponent } from './core/interface-static-values/interface-static-values.component';
import { PsProviderDetailsComponent } from './core/ps-provider-details/ps-provider-details.component';
import { DcsProviderDetailsComponent } from './core/dcs-provider-details/dcs-provider-details.component';
import { InterfaceStatusComponent } from './core/interface-status/interface-status.component';
import { InterfaceJobsListComponent } from './core/interface-jobs-list/interface-jobs-list.component';
import { WizardInterfaceComponent } from './core/wizard-interface/wizard-interface.component'
import { TabsModule } from 'ngx-bootstrap/tabs';

import { NgxTrimDirectiveModule } from 'ngx-trim-directive';
import { DashboardChartComponent } from './core/dashboard-chart/dashboard-chart.component';
import { ChartsModule } from 'ng2-charts';
import { AnalyticsComponent } from './core/analytics/analytics.component';
import { MetricsSummeryComponent } from './core/metrics-summery/metrics-summery.component';
import { MetricDetailsSummaryComponent } from './core/metric-details-summary/metric-details-summary.component';
import { MetricSummaryDetailsComponent } from './core/metric-summary-details/metric-summary-details.component';
import { DbmsJobsComponent } from './core/dbms-jobs/dbms-jobs.component';
import { ExportEvvDataComponent } from './core/export-evv-data/export-evv-data.component';
import { ReconciliationPsComponent } from './core/reconciliation-ps/reconciliation-ps.component';
import { ReconciliationDcsComponent } from './core/reconciliation-dcs/reconciliation-dcs.component';
import { ResubmittransactionPsComponent } from './core/resubmittransaction-ps/resubmittransaction-ps.component';
import { ResubmittransactionDcsComponent } from './core/resubmittransaction-dcs/resubmittransaction-dcs.component';
import { ReconciliationScheduledVisitsComponent } from './core/reconciliation-scheduled-visits/reconciliation-scheduled-visits.component';
import { ResubmitScheduledVisitsComponent } from './core/resubmit-scheduled-visits/resubmit-scheduled-visits.component';
import { LaevvActivityLogComponent } from './core/laevv-activity-log/laevv-activity-log.component';
import { LaevvSummaryLogComponent } from './core/laevv-summary-log/laevv-summary-log.component';
import {TableModule} from 'primeng/table';
import {DropdownModule} from 'primeng/dropdown';
import {InputTextModule} from 'primeng/inputtext';
import {CheckboxModule} from 'primeng/checkbox';
import { ClientFileFromSriComponent } from './core/la-evv/client-file-from-sri/client-file-from-sri.component';
import { PaFileFromSriComponent } from './core/la-evv/pa-file-from-sri/pa-file-from-sri.component';
import { ClientPaFromSriComponent } from './core/la-evv/client-pa-from-sri/client-pa-from-sri.component';
import { ServiceMessageFileSriComponent } from './core/la-evv/service-message-file-sri/service-message-file-sri.component';
import { JobLogComponent } from './core/la-evv/job-log/job-log.component';
import { ProcessedPaRecordsComponent } from './core/la-evv/processed-pa-records/processed-pa-records.component';
import { ReviewComponent } from './core/la-evv/popup/review/review.component';
import { LaEvvClientsComponent } from './core/la-evv/la-evv-clients/la-evv-clients.component';
import {TabViewModule} from 'primeng/tabview';
import {RadioButtonModule} from 'primeng/radiobutton';
import { EvvExportVisitsComponent } from './core/la-evv/evv-export-visits/evv-export-visits.component';
import { ExportDcsComponent } from './core/la-evv/export-dcs/export-dcs.component';
import { LaevvMasterComponent } from './core/la-evv/popup/laevv-master/laevv-master.component';
import { LaevvMasterPaComponent } from './core/la-evv/popup/laevv-master-pa/laevv-master-pa.component';
import { PendingReasonComponent } from './core/pending/pending-reason-completed/pending-reason.component';
import { PendingDashboardComponent } from './core/pending/pending-dashboard/pending-dashboard.component';

@NgModule({
    declarations: [
        AppComponent,
        InterfaceComponent,
        InterfaceDatasetComponent,
        InterfaceDatasetElementComponent,
        ExportVisitsComponent,
        LoginComponent,
        HeaderComponent,
        SidebarComponent,
        SearchPipe,
        ElementSearchPipe,
        EvvDashboardComponent,
        DatasetLogComponent,
        ActivityLogComponent,
        LookupListComponent,
        InterfaceListComponent,
        InterfaceViewComponent,
        ErrorLogComponent,
        DomsanitizePipe,
        ScreenComponent,
        ReconcilationComponent,
        InterfaceJobsComponent,
        PayorDetailsComponent,
        InterfacePayorMappingComponent,
        InterfacePocServicesMapComponent,
        InterfaceServicesComponent,
        InterfaceOfficeDetailsComponent,
        InterfaceMediumComponent,
        InterfaceOfficesComponent,
        InterfaceTokenCredentialComponent,
        VisitDeletedMediumComponent,
        ResubmitTransactionComponent,
        DcoumentComponent,
        InterfaceProviderDetailsComponent,
        InterfaceStaticValuesComponent,
        PsProviderDetailsComponent,
        DcsProviderDetailsComponent,
        InterfaceStatusComponent,
        InterfaceJobsListComponent,
        WizardInterfaceComponent,
        DashboardChartComponent,
        AnalyticsComponent,
        MetricsSummeryComponent,
        MetricDetailsSummaryComponent,
        MetricSummaryDetailsComponent,
        DbmsJobsComponent,
        ExportEvvDataComponent,
        ReconciliationPsComponent,
        ReconciliationDcsComponent,
        ResubmittransactionPsComponent,
        ResubmittransactionDcsComponent,
        ReconciliationScheduledVisitsComponent,
        ResubmitScheduledVisitsComponent,
        LaevvActivityLogComponent,
        LaevvSummaryLogComponent,
        ClientFileFromSriComponent,
        PaFileFromSriComponent,
        ClientPaFromSriComponent,
        ServiceMessageFileSriComponent,
        JobLogComponent,
        ProcessedPaRecordsComponent,
        ReviewComponent,
        LaEvvClientsComponent,
        ExportDcsComponent,
        EvvExportVisitsComponent,
        LaevvMasterComponent,
        LaevvMasterPaComponent,
        PendingReasonComponent,
        PendingDashboardComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule,
        ModalModule.forRoot(),
        NgIdleKeepaliveModule.forRoot(),
        NgxPaginationModule,
        Ng2SearchPipeModule,
        NgxSpinnerModule,
        BsDatepickerModule.forRoot(),
        TooltipModule.forRoot(),
        OrderModule,
        AutocompleteLibModule,
        TabsModule,
        NgxTrimDirectiveModule,
        ChartsModule,
        TableModule,
        DropdownModule,
        InputTextModule,
        CheckboxModule,
        TabViewModule,
        RadioButtonModule
    ],
    providers: [BsModalRef, DatePipe, {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpInterceptorService,
            multi: true
        }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    bootstrap: [AppComponent]
})
export class AppModule { }
