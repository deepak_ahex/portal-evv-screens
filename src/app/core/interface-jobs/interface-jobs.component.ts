import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { DashboardService } from 'src/app/services/dashboard.service';
declare  var $ :any;
@Component({
  selector: 'app-interface-jobs',
  templateUrl: './interface-jobs.component.html',
  styleUrls: ['./interface-jobs.component.scss']
})
export class InterfaceJobsComponent implements OnInit {
  public interfaceId: number;
  public interfaceJobRunTimes: Array<any> = [];
  public jobForm: UntypedFormGroup;
  public interfaceName: string = '';
  public formError: boolean = false;
  public interfaceJobLogList = [
    {isJobCompleted: 0,
      lastUpdated: "01/20/2021",
      methodName: "exportDCSToInterface",
      procedureName: "exportDCSToInterface",
      responseDate: "01/20/2021",
      responseMsg: "ORA-29273: HTTP" }
  ];

  constructor(public dashboardService: DashboardService, public _fb: UntypedFormBuilder) { }

  ngOnInit(): void {
    this.getInterFaceDetails()

  }

  public createForm() {
    this.formError = false;
    this.jobForm = this._fb.group({
      jobName: new UntypedFormControl('', [Validators.required]),
      interface: new UntypedFormControl(''),
      interfaceDataset: new UntypedFormControl('', [Validators.required]),
      active: new UntypedFormControl(1),
      id: new UntypedFormControl(0)

    })
  }
  get formControls() {
    return this.jobForm.controls;
  }
  public updateJobForm(obj) {
    this.jobForm.get('jobName').setValue(obj.jobName);
    this.jobForm.get('interface').setValue(obj.interfaceDefinationId);
    this.jobForm.get('interfaceDataset').setValue(obj.interfaceDatasetId);
    this.jobForm.get('active').setValue(obj.active);
    this.jobForm.get('id').setValue(obj.id);

  }
  public saveForm() {
    this.formError = true;
    if (this.jobForm.valid)
      this.interfaceJobRunTimes.push(this.jobForm.value)
  }
  public activeToggle(event) {
    this.jobForm.get('active').setValue(event.target.checked ? 1 : 0);

  }
  public getInterfaceJobRunTimesList() {
    try {
      this.dashboardService.getInterfaceJobRunTimesList(this.interfaceId).subscribe(res => {
        this.interfaceJobRunTimes = res;
        console.log(res)
      })
    } catch (error) {

    }
  }
  public datasetName='';
  public getInterfaceJobLog(data,template) {
    this.interfaceJobLogList=[];
    this.datasetName=data.interfaceDataset;
    try {
      this.dashboardService.getInterfaceJobLog(this.interfaceId, data.interfaceDatasetId).subscribe(
        res => {
          this.interfaceJobLogList = res;


        }
      )
    } catch (error) {

    }
  }
  public getInterFaceDetails() {
    var data = JSON.parse(sessionStorage.getItem('interfaceData'));

    // console.log(data.name)
    this.interfaceName = data?.label;
    this.interfaceId=data?.id;
    this.createForm();
    this.getInterfaceJobRunTimesList();
  }
}
