import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DatasetLogComponent } from './dataset-log.component';

describe('DatasetLogComponent', () => {
  let component: DatasetLogComponent;
  let fixture: ComponentFixture<DatasetLogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DatasetLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatasetLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
