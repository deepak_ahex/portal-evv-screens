import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dataset-log',
  templateUrl: './dataset-log.component.html',
  styleUrls: ['./dataset-log.component.scss']
})
export class DatasetLogComponent implements OnInit {

  public activityEndDate: Date = new Date();
  public activityStartDate: Date = new Date();
  public dataSet: string = '';
  public dataSetlist: Array<any> = [];



  constructor() {
    for (let i = 0; i < 20; i++) {
      let object = { dataset: 'Employees', direction: 'Exported', user: 'Person', activityDate: '	09/21/2020 5:30 PM', requestFile: 'TEST_VISITS_NJ_ProviderTaxID_20201029101858  ', responseFile: 'Response_VISITS_NJ_ProviderTaxID_20201029101858 ' }
      this.dataSetlist.push(object)
    }
    this.activityStartDate.setDate(this.activityEndDate.getDate() - 7);

  }

  ngOnInit(): void {
  }


}
