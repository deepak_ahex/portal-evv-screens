import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, Validators, PatternValidator } from '@angular/forms';
import { DashboardService } from 'src/app/services/dashboard.service';
import Swal from 'sweetalert2';
import {formTrimValidation} from '../../shared/formvalidator'


@Component({
  selector: 'app-interface-static-values',
  templateUrl: './interface-static-values.component.html',
  styleUrls: ['./interface-static-values.component.scss']
})
export class InterfaceStaticValuesComponent implements OnInit {

  public interfaceName: string = '';
  public formError: boolean = false;
  public userData: any;
  public staicValuesList = [];
  public interfaceId: number;
  public staticvalueForm: UntypedFormGroup;
  public pattern=/^[^ ][\w\W ]*[^ ]/;
  constructor(private fb: UntypedFormBuilder, private _dasboardService: DashboardService) {
    this.getInterFaceDetails();
  }

  ngOnInit(): void {
    this.createForm();
  }

  public createForm() {
    this.formError = false;
    this.staticvalueForm = this.fb.group({
      name: new UntypedFormControl(null, [Validators.required]),
      value: new UntypedFormControl('', [Validators.required]),
      id: new UntypedFormControl(0),
      sortOrder: new UntypedFormControl(''),
      active: new UntypedFormControl(1)

    })
  }
  public onActivechange(event) {
    this.staticvalueForm.get('active').setValue(event.target.checked ? 1 : 0);
  }
  public updateStaticValue(obj) {
    this.staticvalueForm.get('name').setValue(obj.name);
    this.staticvalueForm.get('value').setValue(obj.value);
    this.staticvalueForm.get('id').setValue(obj.id);
    // this.staticvalueForm.get('active').setValue(0);

  }
  get formcontrols() {

    return this.staticvalueForm.controls;
  }

  public getInterfaceStaticValuesList() {
    try {
      this._dasboardService.getInterfaceStaticValuesList(this.interfaceId).subscribe(data => {
        // let res:any=data;
        this.staicValuesList = data
      })
    } catch (error) {

    }
  }
  public saveInterfaceStaticValue() {

    this.formError = true;
    if (this.staticvalueForm.valid) {
      try {
        let obj = { "interfaceDefinitionId": this.interfaceId, "id": this.staticvalueForm.value.id, "name": this.staticvalueForm.value.name, "value": this.staticvalueForm.value.value, "sortOrder": this.staticvalueForm.value.sortOrder, "active": this.staticvalueForm.value.active, "userId": this.userData?.userId }
        this._dasboardService.saveInterfaceStaticValue(obj).subscribe(
          res => {
            console.log(res)

            let data:any=res;
            Swal.fire({
              title: 'success',
              text: data.saveMsg,
              icon: 'success',
              timer: 1200,
              showConfirmButton: false
            });
            this.getInterfaceStaticValuesList();
            this.createForm()
          }
        )

      } catch (error) {

      }
    }
  }

  public getInterFaceDetails() {
    console.log("statuc values");
    this.createForm();
    this.userData = JSON.parse(sessionStorage.getItem('userData'));
    var data = JSON.parse(sessionStorage.getItem('interfaceData'));
    // console.log(data.name)
    this.interfaceName = data?.label;
    this.interfaceId = data?.id;
    this.getInterfaceStaticValuesList();
  }
}
