import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InterfaceStaticValuesComponent } from './interface-static-values.component';

describe('InterfaceStaticValuesComponent', () => {
  let component: InterfaceStaticValuesComponent;
  let fixture: ComponentFixture<InterfaceStaticValuesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InterfaceStaticValuesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterfaceStaticValuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
