import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DbmsJobsComponent } from './dbms-jobs.component';

describe('DbmsJobsComponent', () => {
  let component: DbmsJobsComponent;
  let fixture: ComponentFixture<DbmsJobsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DbmsJobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DbmsJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
