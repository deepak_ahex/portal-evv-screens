import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dbms-jobs',
  templateUrl: './dbms-jobs.component.html',
  styleUrls: ['./dbms-jobs.component.scss']
})
export class DbmsJobsComponent implements OnInit {
  public schedulerJobsList: Array<schedularJobs> = [];
  public userData;
  public isJobsButtonEnabled:boolean=false;

  constructor(private dashboadService: DashboardService) {
    this.userData = JSON.parse(sessionStorage.getItem('userData'));

  }

  ngOnInit(): void {
    this.getInterfaceSchedulerJobs();
  }
  public getInterfaceSchedulerJobs() {
    try {
      this.dashboadService.getInterfaceSchedulerJobs().subscribe(res => {
        console.log(res);
        this.schedulerJobsList = res.schedulerJobs;
        this.isJobsButtonEnabled=res.isJobsButtonEnabled==1?true:false;
      })
    } catch (error) {

    }
  }
  public runSchedulerJob(name) {
    Swal.fire({
      title: 'Are you sure you want to Run Job ?',
      // text: "you want to Logout?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Run Job!'
    }).then((result) => {
      if (result.isConfirmed) {
        let obj = { "jobName": name, "userId": this.userData.userId }
        console.log(obj);
        try {
          this.dashboadService.runSchedulerJob(JSON.stringify(obj)).subscribe(res => {
            console.log(res);
            if (res.saveFlag == 0) {
              this.isJobsButtonEnabled=false;
              Swal.fire('', res.saveMsg, 'error');
            } else {
              Swal.fire('', res.saveMsg, 'success');
              this.getInterfaceSchedulerJobs();
            }
          })
        } catch (error) {
        }
      }
    })


  }
}

export interface schedularJobs {
  id: number;
  jobName: string;
  jobTime: string;
  procedureName: string;
  description: string
}
