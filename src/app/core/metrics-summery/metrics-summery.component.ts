import { DatePipe } from '@angular/common';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DashboardService } from 'src/app/services/dashboard.service';
import { EvvServiceService } from 'src/app/services/evv-service.service';
import Swal from 'sweetalert2';
import{MetricSummaryDetailsComponent } from'src/app/core/metric-summary-details/metric-summary-details.component'
@Component({
  selector: 'app-metrics-summery',
  templateUrl: './metrics-summery.component.html',
  styleUrls: ['./metrics-summery.component.scss']
})
export class MetricsSummeryComponent implements OnInit {
  public interfaceList = [];
  public modelref: BsModalRef;
  public startDate: Date = new Date();
  public endDate: Date = new Date();
  public userData: any;

  public interfaceId = null;
  private appliedStartDate = '';
  private appliedEndDate = '';
  private appliedinterfaceId = 0;

  public evvMetricsArray = [];

  constructor(public evvServiceService: EvvServiceService, private dashboardService: DashboardService, public datepipe: DatePipe, public modalservice: BsModalService) {
    this.startDate.setDate(this.endDate.getDate() - 7)
    this.userData = JSON.parse(sessionStorage.getItem('userData'));
  }

  ngOnInit(): void {
    this.getAllInterfaces();
    this.onGo();
  }

  public onGo() {
    console.log(this.interfaceId);
    let startdate = Date.parse(this.datepipe.transform(this.startDate, 'MM/dd/yyyy'));
    let endate = Date.parse(this.datepipe.transform(this.endDate, 'MM/dd/yyyy'));
    let sysdate = new Date
    if (this.endDate == null || this.startDate == null) {
      Swal.fire({
        title: 'Invalid Dates',
        text: 'Start Date and End Date are Mandatory fields',
        icon: 'warning',
        // timer: 1200,
        showConfirmButton: true
      });
    } else if (startdate > endate || endate > Date.parse(this.datepipe.transform(sysdate, 'MM/dd/yyyy'))) {
      Swal.fire({
        title: 'Invalid Dates',
        text: endate > Date.parse(this.datepipe.transform(sysdate, 'MM/dd/yyyy')) ? `End Date cannot be greater than ${this.datepipe.transform(sysdate, 'MM/dd/yyyy')} ` : 'Start Date cannot be greater than End Date',
        icon: 'warning',
        // timer: 1200,
        showConfirmButton: true
      });
    } else {
      this.appliedStartDate = this.datepipe.transform(this.startDate, 'MM/dd/yyyy');
      this.appliedEndDate = this.datepipe.transform(this.endDate, 'MM/dd/yyyy');
      this.appliedinterfaceId = this.interfaceId != null ? this.interfaceId : 0;
      this.getEVVMetricsSummery();

    }
  }
  public getEVVMetricsSummery() {
    let obj = { "interfaceDefinitionId": this.appliedinterfaceId, "startDate": this.appliedStartDate, "endDate": this.appliedEndDate }

    try {
      this.dashboardService.getEVVMetricsSummery(JSON.stringify(obj)).subscribe(res => {
        console.log(res)
        this.evvMetricsArray = res.evvMetricsSummeryArray;
      })
    } catch (error) {

    }
  }

  public opentemplate(data, status) {
    console.log('hii')
    this.modelref = this.modalservice.show(MetricSummaryDetailsComponent, {
      class: 'metrics-modal',
      initialState: {
        modalData: { "interfaceDefinitionId": data.interfaceDefinitionId, "startDate": this.appliedStartDate, "endDate": this.appliedEndDate, "status": status }
      }
    })
  }
  public getAllInterfaces(): void {
    let payload = { name: '', active: '' };
    this.evvServiceService.getInterfaceDetailsList(JSON.stringify(payload)).subscribe((res) => {
      let data: any = res;
      console.log(data)
      this.interfaceList = data;
    });
  }

}
