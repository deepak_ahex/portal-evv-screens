import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MetricsSummeryComponent } from './metrics-summery.component';

describe('MetricsSummeryComponent', () => {
  let component: MetricsSummeryComponent;
  let fixture: ComponentFixture<MetricsSummeryComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MetricsSummeryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetricsSummeryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
