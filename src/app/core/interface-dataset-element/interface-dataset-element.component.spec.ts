import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InterfaceDatasetElementComponent } from './interface-dataset-element.component';

describe('InterfaceDatasetElementComponent', () => {
  let component: InterfaceDatasetElementComponent;
  let fixture: ComponentFixture<InterfaceDatasetElementComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InterfaceDatasetElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterfaceDatasetElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
