import { Component, OnInit } from '@angular/core';
import { EvvServiceService } from 'src/app/services/evv-service.service';
import { UntypedFormGroup, UntypedFormArray, UntypedFormControl, UntypedFormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-interface-dataset-element',
  templateUrl: './interface-dataset-element.component.html',
  styleUrls: ['./interface-dataset-element.component.scss']
})
export class InterfaceDatasetElementComponent implements OnInit {
  private elementId: any;
  private payload: any;
  public InterfaceElementList: any = [];
  public singleInterfaceDetails: any = {};
  public saveInterface: any = {};
  public lookUpData: any = {};
  public pocDataSets: any;
  public pocElements: any;
  public InterfaceElementById: any = [];
  public showUpdate = true;
  public lookupEle: any = {};
  public userData: any;
  public lookupEleForm: UntypedFormGroup;
  public submitted = false;
  public searchTerm: any;
  public perPage: any = 10;
  public getLength: number;
  public showDeleteIcon: boolean;
  public elementItems: any = [];
  public addScheduleOverrideForm: UntypedFormGroup;

  constructor(private evvServiceService: EvvServiceService,
              private formBuilder: UntypedFormBuilder,
              private router: Router) {
                  this.userData = JSON.parse(sessionStorage.getItem('userData'));
                  if (this.userData){
                    return;
                  }else{
                    this.router.navigate(['/login']);
                  }
               }

  ngOnInit(): void {
    this.lookupEleForm = this.formBuilder.group({
      interfaceDefinition: ['', Validators.required],
      interfaceDataset: ['', Validators.required],
    });
    this.addScheduleOverrideForm = new UntypedFormGroup({
      openHours: new UntypedFormArray([this.createForm({})], [Validators.required]),
    });
    this.userData = JSON.parse(sessionStorage.getItem('userData'));
    this.getAllInterfaceElements();
    this.getLookUpDataForElements();
    this.getPOCDataSets();
    this.getPOCElements();
  }
  refresh() {
    this.getAllInterfaceElements();
  }

  p(e) {
  }
  setUpScheduleOverrideForm(data: any) {
    return new UntypedFormGroup({
      openHours: new UntypedFormArray(data.interfaceElementsDetailsList.map((f) => this.createForm(f))),
    });
  }

  createForm(f) {
    return new UntypedFormGroup({
      id: new UntypedFormControl(f.id || 0),
      elementName: new UntypedFormControl(f.elementName || '', Validators.required),
      pocDatasetName: new UntypedFormControl(f.pocDatasetName || null, Validators.required),
      pocElementName: new UntypedFormControl(f.pocElementName || null, Validators.required),
      sortOrder: new UntypedFormControl(f.sortOrder || null, Validators.required),
    });
  }


  get openHoursArray() {
    return (this.addScheduleOverrideForm.get('openHours') as UntypedFormArray);
  }
  changeDataSet(e) {
    if(e==undefined){
       return;
    }else{
      this.elementItems=[];
      this.pocElements.forEach(ele => {
            if (ele.tableName === e.tableName) {
              this.elementItems.push(ele);
            }
          });
    }
  }
  addOpenHours() {
    const newfield = {};
    this.openHoursArray.push(this.createForm(newfield));
  }
  removeOpenHours(j) {
    this.openHoursArray.removeAt(j);
  }

  getAllInterfaceElements(): void {
    this.payload = { interfaceDefinitionId: 0, interfaceDatasetId: 0 };
    this.evvServiceService.getInterfaceElementsList(JSON.stringify(this.payload)).subscribe((res) => {
      this.InterfaceElementList = res;
    });
  }

  getAllInterfaceElementById(id): void {
    this.elementId = id;
    this.showUpdate = false;
    this.evvServiceService.getInterfaceElementById(JSON.stringify(id)).subscribe((res) => {
      const data = JSON.stringify(res);
      const newData = JSON.parse(data);
      this.lookupEle.interfaceDataset = newData.interfaceDatasetId;
      this.lookupEle.interfaceDefinition = newData.interfaceDefinitionId;
      this.InterfaceElementById = newData;
      if (newData.interfaceElementsDetailsList.length > 0) {
        this.getLength = newData.interfaceElementsDetailsList.length;
      }
      this.addScheduleOverrideForm = this.setUpScheduleOverrideForm(newData || {});
    });
  }

  saveInterfaceElement(): void {
    this.submitted = true;
    let id = this.elementId ? this.elementId : 0;
    let interfaceDefinitionId = this.lookupEle.interfaceDefinition;
    let interfaceDatasetId = this.lookupEle.interfaceDataset;
    const userId = this.userData.userId;
    const payload = this.addScheduleOverrideForm.value.openHours;
    if(this.addScheduleOverrideForm.valid){
      this.evvServiceService.saveInterfaceElements(JSON.stringify(id), JSON.stringify(interfaceDefinitionId),
      JSON.stringify(interfaceDatasetId), JSON.stringify(userId), JSON.stringify(payload)).subscribe((resp) => {
        if (resp) {
          this.getAllInterfaceElements();
          Swal.fire({
            title: 'success',
            text: 'Saved successfully',
            icon: 'success',
            timer: 1200,
            showConfirmButton: false
          });
          id = 0;
          this.lookupEle = {};
          interfaceDefinitionId = null;
          interfaceDatasetId = null;
          this.showUpdate = true;
          this.submitted = false;
          this.reset();
        }
      });
    }
  }
  reset() {
    this.addScheduleOverrideForm = new UntypedFormGroup({
      openHours: new UntypedFormArray([this.createForm({})]),
    });
    this.addScheduleOverrideForm.reset();
  }
  getLookUpDataForElements(): void {
    this.evvServiceService.getLookUpDataForElements().subscribe((res) => {
      this.lookUpData = res;
    });
  }
  getPOCDataSets(): void {
    this.evvServiceService.getPOCDataSets().subscribe((res) => {
      this.pocDataSets = res;
    });
  }

  getPOCElements(): void {
    this.evvServiceService.getPOCElements().subscribe((res) => {
      this.pocElements = res;
      this.elementItems = this.pocElements;
    });
  }
}
