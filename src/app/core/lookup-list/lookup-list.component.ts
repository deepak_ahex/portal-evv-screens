import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lookup-list',
  templateUrl: './lookup-list.component.html',
  styleUrls: ['./lookup-list.component.scss']
})
export class LookupListComponent implements OnInit {
  public lookupName:any;
  constructor() { }

  ngOnInit(): void {
    this.lookupName=sessionStorage.getItem('lookupName')
  }
data=[{"lookupList":[{"lookupId":2,"interfaceDefinitionId":17,"interfaceDefinitionName":"Sandata","lookupName":"Sandata Adjust Reasons"},
                     {"lookupId":3,"interfaceDefinitionId":17,"interfaceDefinitionName":"Sandata","lookupName":"Sandata Bill Type"},
                     {"lookupId":4,"interfaceDefinitionId":17,"interfaceDefinitionName":"Sandata","lookupName":"Sandata Cancel Code"},
                     {"lookupId":5,"interfaceDefinitionId":17,"interfaceDefinitionName":"Sandata","lookupName":"Sandata Discipline"},
                     {"lookupId":6,"interfaceDefinitionId":17,"interfaceDefinitionName":"Sandata","lookupName":"Sandata Event Codes"},
                     {"lookupId":7,"interfaceDefinitionId":17,"interfaceDefinitionName":"Sandata","lookupName":"Sandata Services"}],
       "totalCount":6}]
}
