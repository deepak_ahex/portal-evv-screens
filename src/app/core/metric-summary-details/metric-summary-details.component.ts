import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { DashboardService } from 'src/app/services/dashboard.service';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver'
@Component({
  selector: 'app-metric-summary-details',
  templateUrl: './metric-summary-details.component.html',
  styleUrls: ['./metric-summary-details.component.scss']
})
export class MetricSummaryDetailsComponent implements OnInit {

  public modalData: any;
  public lowerBound = 1;
  public perPage = 10;
  public upperBound = 10;
  public totalCount = 0;
  constructor(private dashboardService: DashboardService, public bsmodelRef: BsModalRef) { }

  ngOnInit(): void {
    this.getEVVMetricsSummeryDetails();
  }
  public evvMetricsDetailsArray = [
    // {"siteCode":"1723","procedureCode":"S5125U5","scheduledEndDateTime":"02/10/2021 10:15 AM","psName":"dummy, dummy","dcsName":"dummy, dummy","scheduledStartDateTime":"02/10/2021 09:00 AM","reportedStartDateTime":"02/10/2021 09:00 AM","exportedOn":"03/09/2021","billedRevenue":"0.0","batchNumber":506,"reportedEndDateTime":"02/10/2021 10:15 AM"}
  ];

  public getEVVMetricsSummeryDetails() {
    let obj = { "interfaceDefinitionId": this.modalData.interfaceDefinitionId, "startDate": this.modalData.startDate, "endDate": this.modalData.endDate, "status": this.modalData.status, "lowerBound": this.lowerBound, "upperBound": this.upperBound }
    try {
      this.dashboardService.getEVVMetricsSummeryDetails(JSON.stringify(obj)).subscribe(res => {
        console.log(res)
        this.evvMetricsDetailsArray = res.evvMetricsSummeryDetailsArray;
        this.totalCount = res.totalCount;
      })
    } catch (error) {

    }
  }

  public prevpage(): void {
    this.lowerBound = this.lowerBound - this.perPage;
    this.upperBound = this.upperBound - this.perPage;
    this.getEVVMetricsSummeryDetails();

  }
  public nextPage(): void {
    this.lowerBound = this.lowerBound + this.perPage;
    this.upperBound = this.upperBound + this.perPage;
    this.getEVVMetricsSummeryDetails();

  }
  public pageChange() {
    this.lowerBound = 1;
    this.upperBound = this.perPage;
    this.getEVVMetricsSummeryDetails();

  }
  private downloadresponse = [];
  public downloadSheet() {
    this.downloadresponse = [];
    let obj = { "interfaceDefinitionId": this.modalData.interfaceDefinitionId, "startDate": this.modalData.startDate, "endDate": this.modalData.endDate, "status": this.modalData.status, "lowerBound": 0, "upperBound": 0 }
    try {
      this.dashboardService.getEVVMetricsSummeryDetails(JSON.stringify(obj)).subscribe(res => {
        console.log(res)
        this.downloadresponse = res.evvMetricsSummeryDetailsArray;
        this.exportexcel();
      })
    } catch (error) {

    }
  }
  public exportexcel(): void {
    let mappedarray = [];

    if (this.downloadresponse.length > 0) {
      if(this.modalData.status==3){
        mappedarray = this.downloadresponse.map(item => {
          return {
            "Batch #": item.batchNumber,
            "PS": item.psName,
            "DCS": item.dcsName,
            "Procedure Code": item.procedureCode,
            "Scheduled Start": item.scheduledStartDateTime,
            "Scheduled End": item.scheduledEndDateTime,
            "Reported Start": item.reportedStartDateTime,
            "Reported End	": item.reportedEndDateTime,
            "Arrival Call Type": item.arrCallType,
            "Departure Call Type": item.depCallType,
            "Exported On": item.exportedOn,
            "Error Code":item?.exportStatusCode,
            "Rejected Reason":item?.exportStatusMessage,
            "Payor Code":item?.payorCode

          }
        })
      }
      else{
      mappedarray = this.downloadresponse.map(item => {
        return {
          "Batch #": item.batchNumber,
          "PS": item.psName,
          "DCS": item.dcsName,
          "Procedure Code": item.procedureCode,
          "Scheduled Start": item.scheduledStartDateTime,
          "Scheduled End": item.scheduledEndDateTime,
          "Reported Start": item.reportedStartDateTime,
          "Reported End	": item.reportedEndDateTime,
          "Arrival Call Type": item.arrCallType,
          "Departure Call Type": item.depCallType,
          "Exported On": item.exportedOn,

        }
      })
    }
    } else {
      mappedarray = [{
        "Batch #": '',
        "PS": '',
        "DCS": '',
        "Procedure Code": '',
        "Scheduled Start": '',
        "Scheduled End": '',
        "Reported Start": '',
        "Reported End	": '',
        "Arrival Call Type": '',
        "Departure Call Type": '',
        "Exported On": '',

      }]
    }
    var wscols = [
      { wch: 22 },
      { wch: 20 },
      { wch: 20 },
      { wch: 30 },
      { wch: 30 },
      { wch: 30 },
      { wch: 30 },
      { wch: 30 },
      { wch: 30 },
      { wch: 30 },
      { wch: 25 },
      { wch: 25 },
      { wch: 30 },
      { wch: 35 }, { wch: 35 }, { wch: 35 },

    ];

    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(mappedarray);
    worksheet["!cols"] = wscols;
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, 'EVV_Metrics_Details');



  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: 'string' });
    /***********`
    *YOUR EXCEL FILE'S NAME
    */
    FileSaver.saveAs(data, fileName + '.xlsx');
  }
}
