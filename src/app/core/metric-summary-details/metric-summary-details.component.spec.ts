import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MetricSummaryDetailsComponent } from './metric-summary-details.component';

describe('MetricSummaryDetailsComponent', () => {
  let component: MetricSummaryDetailsComponent;
  let fixture: ComponentFixture<MetricSummaryDetailsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MetricSummaryDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetricSummaryDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
