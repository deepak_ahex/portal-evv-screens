import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ResubmitTransactionComponent } from './resubmit-transaction.component';

describe('ResubmitTransactionComponent', () => {
  let component: ResubmitTransactionComponent;
  let fixture: ComponentFixture<ResubmitTransactionComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ResubmitTransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResubmitTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
