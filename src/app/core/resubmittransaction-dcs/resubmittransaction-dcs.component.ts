import { DatePipe } from '@angular/common';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DashboardService } from 'src/app/services/dashboard.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-resubmittransaction-dcs',
  templateUrl: './resubmittransaction-dcs.component copy.html',
  styleUrls: ['./resubmittransaction-dcs.component.scss']
})
export class ResubmittransactionDcsComponent implements OnInit {
  public interfaceId = null;
  public interfaceList = [];
  public dcsId = null;
  public dcsList = [];
  public startDate: Date = new Date();
  public endDate: Date = new Date();
  public modalRef: BsModalRef;
  private appliedStartDate = '';
  private appliedEndDate = '';
  private appliedDCSId = 0;
  private appliedInterface = 0;
  public lowerBound = 1;
  public upperBound = 100;
  public perPage = 100;
  public exportDetailsArray = [];
  // [{"dcsId":766,"emp_code":"","interfaceDefinitionId":8,"interfaceDatasetId":20,"dcsName":"BALDWIN, GLORIA","id":1038,"responseMessage":"Exception in SQL exportDCSToInterface : java.lang.NullPointerException\r\n\tat com.assh.pocevv.export.daos.impl.ExportTransactionDAOImpl$10.doInTransactionWithoutResult(ExportTransactionDAOImpl.java:856)\r\n\tat org.springframework.transaction.support.TransactionCallbackWithoutResult.doInTransaction(TransactionCallbackWithoutResult.java:34)\r\n\tat org.springframework.transaction.support.TransactionTemplate.execute(TransactionTemplate.java:133)\r\n\tat com.assh.pocevv.export.daos.impl.ExportTransactionDAOImpl.exportDCSToInterface(ExportTransactionDAOImpl.java:819)\r\n\tat com.assh.pocevv.export.services.impl.ExportServiceImpl.exportDCSToInterface(ExportServiceImpl.java:319)\r\n\tat com.assh.pocevv.export.model.DCSRunnableThread.run(DCSRunnableThread.java:23)\r\n\tat java.lang.Thread.run(Thread.java:748)\r\n","exportedOn":"04/27/2021 03:30 AM","batchNumber":509},{"dcsId":196334,"emp_code":"","interfaceDefinitionId":8,"interfaceDatasetId":20,"dcsName":"Camelli, Adams","id":55,"responseMessage":"ERROR: The EmployeeEmail value exceeds the maximum length of 64 characters. The length should be between 1 and 64. The record is being rejected.","exportedOn":"03/04/2021 10:33 PM","batchNumber":448},{"dcsId":15930,"emp_code":"","interfaceDefinitionId":8,"interfaceDatasetId":20,"dcsName":"Wallin, Lorri","id":57,"responseMessage":"ERROR: The EmployeeEmail value exceeds the maximum length of 64 characters. The length should be between 1 and 64. The record is being rejected.","exportedOn":"03/04/2021 10:33 PM","batchNumber":448},{"dcsId":2050,"emp_code":"","interfaceDefinitionId":8,"interfaceDatasetId":20,"dcsName":"EASTON, MIMI","id":23,"exportedOn":"03/02/2021 11:34 PM","batchNumber":410},{"dcsId":11413,"emp_code":"","interfaceDefinitionId":8,"interfaceDatasetId":20,"dcsName":"RODRIGUEZ, ANA","id":24,"exportedOn":"03/02/2021 11:34 PM","batchNumber":410},{"dcsId":15801,"emp_code":"","interfaceDefinitionId":8,"interfaceDatasetId":20,"dcsName":"Dzelza-Hopsone, Kristine","id":22,"exportedOn":"03/02/2021 11:34 PM","batchNumber":410}];
  public totalCount = 0;
  public defaultMessage = 0;
  public userData;
  public dcsIds = [];
  public visiitDetailsCheckedAll = false;





  constructor(private modalService: BsModalService, public dashboardservice: DashboardService, private datePipe: DatePipe) {
    this.userData = JSON.parse(sessionStorage.getItem('userData'));
    this.startDate.setDate(this.endDate.getDate() - 7);

  }

  ngOnInit(): void {
    this.getInterfaceByDataSet()
  }

  public getInterfaceByDataSet(): void {

    this.dashboardservice.getInterfaceByDataSet('DCS').subscribe((res) => {
      let data: any = res;
      console.log(data)
      this.interfaceList = data;
    });
  }
  public filterChange() {
    // this.psId != 0 ? this.ps.clear() : '';
    if (this.interfaceId != 0) {
      this.getFilterDataByDataSet();
    }
    this.dcsId = null;

  }
  public getFilterDataByDataSet() {
    this.dashboardservice.getFilterDataByDataSet(this.interfaceId, 'DCS').subscribe(res => {
      console.log(res);
      let data: any = res;
      this.filterDataByDataSetResponse=res;
      if (this.includeInActiveDCS) {
        this.dcsList = data.dcsList;
    } else {
        this.dcsList = data.dcsList.filter(x => x.active == 1);
    }



    })
  }
  public onGo() {
    if (this.interfaceId == null) {
      Swal.fire({
        title: 'Invalid Search',
        text: 'Please select an Interface !',
        icon: 'warning',
        // timer: 1200,
        showConfirmButton: true
      });
    } else if (this.startDate != null && this.endDate != null) {
      let startDate = Date.parse(this.datePipe.transform(this.startDate, 'MM/dd/yyyy'));
      let endDate = Date.parse(this.datePipe.transform(this.endDate, 'MM/dd/yyyy'));
      if (startDate > endDate) {
        Swal.fire('', 'Start date cannot be greater than End date', 'warning')
      } else {
        this.appliedStartDate = this.datePipe.transform(this.startDate, 'MM/dd/yyyy')
        this.appliedEndDate = this.datePipe.transform(this.endDate, 'MM/dd/yyyy');
        this.appliedDCSId = this.dcsId == null ? 0 : this.dcsId;
        this.appliedInterface=this.interfaceId;

        this.defaultMessage++;
        this.onPageReset();

      }
    } else {
      if (this.endDate == null || this.startDate == null) {
        Swal.fire({
          title: 'Invalid Dates',
          text: 'Start Date and End Date are Mandatory fields',
          icon: 'warning',
          // timer: 1200,
          showConfirmButton: true
        });
      }
    }
  }
  public getResubmitDCSList() {
    let jsonObj = { "interfaceDefinitionId": this.appliedInterface, "dcsId": this.appliedDCSId, "startDate": this.appliedStartDate, "endDate": this.appliedEndDate, "lowerBound": this.lowerBound, "upperBound": this.upperBound, "orderBy": this.orderBy, "order": this.orderType }

    try {
      this.dcsIds = [];
      this.visiitDetailsCheckedAll = false;
      this.dashboardservice.getResubmitDCSList(JSON.stringify(jsonObj)).subscribe(res => {
        console.log(res);

        this.exportDetailsArray = res.exportDetailsArray;
        this.totalCount = res.totalCount;
      })
    } catch (error) {

    }
  }

  public onPageReset() {
    this.lowerBound = 1;
    this.upperBound = this.perPage;
    this.getResubmitDCSList()
  }
  public prevpage(): void {
    this.lowerBound = this.lowerBound - this.perPage;
    this.upperBound = this.upperBound - this.perPage;
    this.getResubmitDCSList();

  }
  public nextPage(): void {
    this.lowerBound = this.lowerBound + this.perPage;
    this.upperBound = this.upperBound + this.perPage;
    this.getResubmitDCSList();

  }
  public repushDCSRecords() {
    let jsonObject = {"interfaceDefinitionId":this.appliedInterface,"dcsIds":this.dcsIds.toString(),"userId":this.userData.userId};

    try {
      this.dashboardservice.repushDCSRecords(JSON.stringify(jsonObject)).subscribe(res => {
        console.log(res);
        if (res.saveFlag == '1') {
          Swal.fire('Success', res.saveMsg, 'success');
          this.getResubmitDCSList();
          this.visiitDetailsCheckedAll = false;
        } else {
          Swal.fire('Failed', 'Failed to save Data', 'error');

        }
      })
    } catch (error) {

    }

  }
  public onvisitCheck(visitId, event) {
    if (event.target.checked) {
      this.dcsIds.push(visitId);
      console.log(this.dcsIds)

    } else {
      this.dcsIds = this.dcsIds.filter(x => x != visitId);
      console.log(this.dcsIds)
    }

  }
  public onCheckAll(event) {
    if (event.target.checked) {
      this.visiitDetailsCheckedAll = true;
      this.dcsIds = this.exportDetailsArray.map(x => {
        return x.dcsId
      })
    } else {
      this.visiitDetailsCheckedAll = false;
      this.dcsIds = [];
    }
  }
  public responseMessage='';
  onresponse(temlate: TemplateRef<any>, messsage) {
    this.modalRef = this.modalService.show(temlate, {
      class: '  bd-example-modal-lg'
    })
    this.responseMessage = messsage

  }
  public visitRequestData=[]
  public  getExportedDataDetails(data, recordDetails: TemplateRef<any>) {
    console.log(data)
    try {
      this.dashboardservice.getExportedDataDetails(data.id, data.interfaceDefinitionId, data.interfaceDatasetId,'exported_dcs_data').subscribe(
        res => {
          console.log(res);
          this.visitRequestData = res;
          this.modalRef = this.modalService.show(recordDetails);

        },
        err=>{
          Swal.fire({
            title: 'Failed to Load',
            text: 'Something Went Wrong',
            icon: 'error',
            // timer: 1200,
            showConfirmButton: true
          });        }
      )
    } catch (error) {

    }

  }
  public orderBy = '';
  public orderType = "";
  public currentorderby = "";
  public currentorderType = "";
  public sortorder(orderBy, orderType) {
    if (this.currentorderby != orderBy || this.currentorderType != orderType) {
      this.orderType = orderType;
      this.orderBy = orderBy;
      this.currentorderby = orderBy;
      this.currentorderType = orderType;
      this.onPageReset();
    }
  }


  public filterDataByDataSetResponse: any;
  public includeInActiveDCS = false;
  public onIncludeInactiveChange(event) {
    this.includeInActiveDCS = event;
    console.log(event);
    if (event) {
        this.dcsList = this.filterDataByDataSetResponse.dcsList;
    } else {
        this.dcsList = this.filterDataByDataSetResponse.dcsList.filter(x => x.active == 1);
    }


  }



}
