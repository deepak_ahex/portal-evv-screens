import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ResubmittransactionDcsComponent } from './resubmittransaction-dcs.component';

describe('ResubmittransactionDcsComponent', () => {
  let component: ResubmittransactionDcsComponent;
  let fixture: ComponentFixture<ResubmittransactionDcsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ResubmittransactionDcsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResubmittransactionDcsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
