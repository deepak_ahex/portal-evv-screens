import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DcsProviderDetailsComponent } from './dcs-provider-details.component';

describe('DcsProviderDetailsComponent', () => {
  let component: DcsProviderDetailsComponent;
  let fixture: ComponentFixture<DcsProviderDetailsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DcsProviderDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DcsProviderDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
