import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, Validators } from '@angular/forms';
import { DashboardService } from 'src/app/services/dashboard.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dcs-provider-details',
  templateUrl: './dcs-provider-details.component.html',
  styleUrls: ['./dcs-provider-details.component.scss']
})
export class DcsProviderDetailsComponent implements OnInit {


  public interfaceName = '';
  public formError: boolean = false;
  public interfaceId: number;
  public dcsproviderForm: UntypedFormGroup;
  public dcsProviderDetailsList = [];
  public officeLookup = [];
  public userData: any;
  public providerIdList = [];
  public dcsList = [];

  constructor(private _fb: UntypedFormBuilder, private _dashBoard: DashboardService) {
    this.getInterFaceDetails();
  }
  ngOnInit(): void {
  }
  public createForm() {
    this.formError = false;
    this.dcsproviderForm = this._fb.group({
      officeId: new UntypedFormControl(null, Validators.required),
      dcsId: new UntypedFormControl(null, [Validators.required]),
      providerId: new UntypedFormControl(null, [Validators.required]),
      id: new UntypedFormControl(0)
    })
  }
  get formcontrols() {
    return this.dcsproviderForm.controls;
  }
  public updateProvideForm(obj) {
    this.dcsproviderForm.get('officeId').setValue(obj.officeId);
    this.dcsproviderForm.get('dcsId').setValue(obj.dcsId);
    this.dcsproviderForm.get('providerId').setValue((obj.providerId));
    this.dcsproviderForm.get('id').setValue(obj.id);
    this.onOfficeChange(false);
  }
  public getInterFaceDetails() {
    var data = JSON.parse(sessionStorage.getItem('interfaceData'));
    this.userData = JSON.parse(sessionStorage.getItem('userData'));

    // console.log(data.name)
    this.interfaceName = data?.label;
    this.interfaceId = data?.id;

    this.createForm();
    this.getInterfaceOffices();
    this.getDCSProviderDetailsList();


  }

  public saveDCSProviderDetails() {
    this.formError = true;
    if (this.dcsproviderForm.valid) {
      let obj = {
        "interfaceDefinitionId": this.interfaceId,
        "id": this.dcsproviderForm.value.id,
        "officeId": this.dcsproviderForm.value.officeId,
        "dcsId": this.dcsproviderForm.value.dcsId,
        "providerId": this.dcsproviderForm.value.providerId,
        "userId": this.userData.userId
      }

      try {
        this._dashBoard.saveDCSProviderDetails(obj).subscribe(data => {
          console.log(data);
          if (data.saveFlag == 1) {
            Swal.fire({
              title: 'success',
              text: data.saveMsg,
              icon: 'success',
              timer: 1200,
              showConfirmButton: false
            });
            this.getDCSProviderDetailsList();
            this.createForm();
          } else {
            Swal.fire({
              title: 'Failed',
              text: data.saveMsg,
              icon: 'error',
              timer: 1200,
              showConfirmButton: false
            });
          }

        })
      } catch (error) {

      }
    }

  }

  public getDCSProviderDetailsList() {
    try {
      this._dashBoard.getDCSProviderDetailsList(this.interfaceId).subscribe(res => {
        this.dcsProviderDetailsList = res;
        console.log(res)
      })
    } catch (error) {

    }
  }
  public getInterfaceOffices() {
    try {
      this._dashBoard.getInterfaceOffices(this.interfaceId).subscribe(
        res => {
          console.log(res);
          let data: any = res;
          this.officeLookup = data;
        }
      )
    } catch (error) {

    }
  }

  public getDCSList() {
    try {
      this._dashBoard.getDCSList(this.dcsproviderForm.value.officeId).subscribe(res => {
        console.log(res);
        this.dcsList = res

      })
    } catch (error) {

    }
  }
  public getOfficeProviderDetails() {
    try {
      this._dashBoard.getOfficeProviderDetails(this.interfaceId, this.dcsproviderForm.value.officeId).subscribe(res => {
        console.log(res);
        this.providerIdList = res;

      })
    } catch (error) {

    }
  }
  public onOfficeChange(flag: boolean) {
    if (this.dcsproviderForm.value.officeId == null) {
      this.dcsproviderForm.get('providerId').setValue(null);
      this.dcsproviderForm.get('dcsId').setValue(null);
    } else {
      if (flag) {
        this.dcsproviderForm.get('providerId').setValue(null);
        this.dcsproviderForm.get('dcsId').setValue(null);
      }
      this.getDCSList();
      this.getOfficeProviderDetails();
    }
  }
}
