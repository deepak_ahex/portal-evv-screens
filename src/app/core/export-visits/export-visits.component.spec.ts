import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ExportVisitsComponent } from './export-visits.component';

describe('ExportVisitsComponent', () => {
  let component: ExportVisitsComponent;
  let fixture: ComponentFixture<ExportVisitsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ExportVisitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportVisitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
