import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-interface-office-details',
  templateUrl: './interface-office-details.component.html',
  styleUrls: ['./interface-office-details.component.scss']
})
export class InterfaceOfficeDetailsComponent implements OnInit {

  public interfaceName: string = '';
  public officeDetailsForm:UntypedFormGroup;
  public formError:boolean=false;
  public officeDetailsList = [
    { 'active':1,'interfaceId': 1,dataset:'Dataset' ,'datasetId': 1, 'site': 'site', 'sftpKey': 'brspSftpKey.pem', 'fileName': 'BRSP Visit', 'fileDateFormat': '', 'effectiveFrom': '12/18/2020 06:21:31 pm', 'clientId': '', 'jsonKey': 'visits','officeDetailsId': 1},
    { 'active':0,'interfaceId': 1,dataset:'Dataset' ,'datasetId': 1, 'site': 'site', 'sftpKey': 'brspSftpKey.pem', 'fileName': 'BRSP Visit', 'fileDateFormat': '', 'effectiveFrom': '12/18/2020 06:21:31 pm', 'clientId': '', 'jsonKey': 'visits','officeDetailsId': 1},
    { 'active':1,'interfaceId': 1,dataset:'Dataset' ,'datasetId': 1, 'site': 'site', 'sftpKey': 'brspSftpKey.pem', 'fileName': 'BRSP Visit', 'fileDateFormat': '', 'effectiveFrom': '12/18/2020 06:21:31 pm', 'clientId': '', 'jsonKey': 'visits','officeDetailsId': 1},
    { 'active':0,'interfaceId': 1,dataset:'Dataset' ,'datasetId': 1, 'site': 'site', 'sftpKey': 'brspSftpKey.pem', 'fileName': 'BRSP Visit', 'fileDateFormat': '', 'effectiveFrom': '12/18/2020 06:21:31 pm', 'clientId': '', 'jsonKey': 'visits','officeDetailsId': 1},
    { 'active':0,'interfaceId': 1,dataset:'Dataset' ,'datasetId': 1, 'site': 'site', 'sftpKey': 'brspSftpKey.pem', 'fileName': 'BRSP Visit', 'fileDateFormat': '', 'effectiveFrom': '12/18/2020 06:21:31 pm', 'clientId': '', 'jsonKey': 'visits','officeDetailsId': 1},
    { 'active':1,'interfaceId': 1,dataset:'Dataset' ,'datasetId': 1, 'site': 'site', 'sftpKey': 'brspSftpKey.pem', 'fileName': 'BRSP Visit', 'fileDateFormat': '', 'effectiveFrom': '12/18/2020 06:21:31 pm', 'clientId': '', 'jsonKey': 'visits','officeDetailsId': 1}]
  constructor(private _fb:UntypedFormBuilder) {
    this.getInterFaceDetails();

  }
  ngOnInit(): void {
  }
  public createForm(){
    this.formError=false;
   this.officeDetailsForm=this._fb.group({

    interfaceId:new UntypedFormControl(0),
    datasetId:new UntypedFormControl(0),
    site:new UntypedFormControl(0,[Validators.required]),
    sftpKey:new UntypedFormControl('',[Validators.required]),
    fileName:new UntypedFormControl('',[Validators.required]),
    fileDateFormat:new UntypedFormControl('',[Validators.required]),
    effectiveFrom:new UntypedFormControl('',[Validators.required]),
    clientId:new UntypedFormControl('',[Validators.required]),
    jsonKey:new UntypedFormControl('',[Validators.required]),
    active:new UntypedFormControl(1,[Validators.required]),
    officeDetailsId:new UntypedFormControl('',[Validators.required]),

  })
  }
  get formControls(){
    return this.officeDetailsForm.controls;
  }
  public updateOfficeDetailForm(obj){
    console.log(obj)
    this.officeDetailsForm.get("interfaceId").setValue(obj.interfaceId);
    this.officeDetailsForm.get("datasetId").setValue(obj.datasetId);
    this.officeDetailsForm.get("site").setValue(obj.site);
    this.officeDetailsForm.get("sftpKey").setValue(obj.sftpKey);
    this.officeDetailsForm.get("fileName").setValue(obj.fileName);
    this.officeDetailsForm.get("effectiveFrom").setValue(obj.effectiveFrom);
    this.officeDetailsForm.get("fileDateFormat").setValue(obj.fileDateFormat);
    this.officeDetailsForm.get("clientId").setValue(obj.clientId);
    this.officeDetailsForm.get("jsonKey").setValue(obj.jsonKey);
    this.officeDetailsForm.get("officeDetailsId").setValue(obj.officeDetailsId);
    this.officeDetailsForm.get("active").setValue(obj.active);

  }
  public saveOfficeDetails(){
    this.formError=true;
  }
  public getInterFaceDetails() {
    var data = JSON.parse(sessionStorage.getItem('interfaceData'));
    // console.log(data.name)
    this.interfaceName = data?.label;
    this.createForm();
  }

}
