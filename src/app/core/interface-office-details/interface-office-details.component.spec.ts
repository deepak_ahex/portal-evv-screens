import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InterfaceOfficeDetailsComponent } from './interface-office-details.component';

describe('InterfaceOfficeDetailsComponent', () => {
  let component: InterfaceOfficeDetailsComponent;
  let fixture: ComponentFixture<InterfaceOfficeDetailsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InterfaceOfficeDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterfaceOfficeDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
