import { Component, OnInit } from '@angular/core';
import { EvvServiceService } from '../../services/evv-service.service';
import { UserIdleService } from 'angular-user-idle';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { WizardInterfaceComponent } from '../wizard-interface/wizard-interface.component';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';

@Component({
  selector: 'app-interface',
  templateUrl: './interface.component.html',
  styleUrls: ['./interface.component.scss']
})
export class InterfaceComponent implements OnInit {
  private userData: any;
  private payload: any;
  public formError = false;
  public InterfaceDetailsList: any = [];
  public billingSystem = [];
  public interfaceForm: FormGroup;
  public perPage: any = 10;
  public searchTerm: any;
  public saveInterface: any = {};
  public interFaceId: any;
  constructor(private evvServiceService: EvvServiceService, private _fb: FormBuilder, public modalService: BsModalService,
    private router: Router) {
    this.userData = JSON.parse(sessionStorage.getItem('userData'));
    this.createForm();

    if (this.userData) {
      return;
    } else {
      this.router.navigate(['/login']);

    }

  }
  ngOnInit(): void {
    this.getLookupData();
    this.getAllInterfaces();

    this.interFaceId = sessionStorage.getItem('interfaceId')
    if (this.interFaceId) {
      this.getInterfaceDetails(Number(this.interFaceId));
    }
  }
  public createForm() {
    this.formError = false;
    this.interfaceForm = this._fb.group({
      id: new FormControl(0),
      name: new FormControl(null, [Validators.required, NameValidator.noWhiteSpace]),
      description: new FormControl(null),
      label: new FormControl(null, [Validators.required, NameValidator.noWhiteSpace]),
      active: new FormControl(true),
      schdVisitReinstateFlag: new FormControl(false),
      visitUpdateFlag: new FormControl(false),
      visitCancelFlag: new FormControl(false),
      dcsLicenseNumberRequired: new FormControl(false),
      billingSystemId: new FormControl(null),
      billingAcknowledgement: new FormControl(false),
    })
  }

  get formControls() {
    return this.interfaceForm.controls;
  }
  public viewContentType(id, IntrfaceId, interfaceName, data) {
    sessionStorage.setItem('interfaceData', JSON.stringify(data));
    this.router.navigate(['/view/' + id + '/' + IntrfaceId + '/' + interfaceName]);
  }
  ngOnDestroy() {
    this.interFaceId = '';
    sessionStorage.removeItem('interfaceId')
  }

  public getAllInterfaces(): void {
    this.payload = { name: '', active: '' };
    this.evvServiceService.getInterfaceDetailsList(JSON.stringify(this.payload)).subscribe((res) => {
      this.InterfaceDetailsList = res;
    });
  }

  public getInterfaceDetails(id) {
    this.evvServiceService.getInterfaceDetails(JSON.stringify(id)).subscribe((res) => {
      // this.singleInterfaceDetails = res;
      this.interfaceForm = this._fb.group({
        id: new FormControl(res.id),
        name: new FormControl(res.name, [Validators.required, NameValidator.noWhiteSpace]),
        description: new FormControl(res.description),
        label: new FormControl(res.label, [Validators.required, NameValidator.noWhiteSpace]),
        active: new FormControl(res.active == 1),
        schdVisitReinstateFlag: new FormControl(res.schdVisitReinstateFlag == 1),
        visitUpdateFlag: new FormControl(res.visitUpdateFlag == 1),
        visitCancelFlag: new FormControl(res.visitCancelFlag == 1),

        billingSystemId: new FormControl(res?.billingSystemId == '' ? null : res.billingSystemId),
        billingAcknowledgement: new FormControl(res.billingAcknowledgement == 1),
        dcsLicenseNumberRequired: new FormControl(res.dcsLicenseNumberRequired == 1),
      })
    });
  }

  public createInterface(): void {
    console.log(this.interfaceForm.getRawValue());
    this.formError = true;
    const body = {
      id: this.interfaceForm.value.id,
      name: this.interfaceForm.value.name,
      description: this.interfaceForm.value.description ?? '',
      active: this.interfaceForm.value.active ? 1 : 0,
      label: this.interfaceForm.value.label,
      billingSystemId: this.interfaceForm.value.billingSystemId ?? 0,
      visitCancelFlag: this.interfaceForm.value.visitCancelFlag ? 1 : 0,
      dcsLicenseNumberRequired: this.interfaceForm.value.dcsLicenseNumberRequired ? 1 : 0,
      schdVisitReinstateFlag: this.interfaceForm.value.schdVisitReinstateFlag ? 1 : 0,
      visitUpdateFlag: this.interfaceForm.value.visitUpdateFlag ? 1 : 0,
      billingAcknowledgement: this.interfaceForm.value.billingAcknowledgement ? 1 : 0,
    };
    if (this.interfaceForm.valid) {
      this.evvServiceService.createInterfaceByUserId(JSON.stringify(this.userData.userId), JSON.stringify(body)).subscribe((res) => {
        if (res) {
          console.log(res)
          const swalData: any = res;
          if (swalData.saveFlag == 1) {
            this.getAllInterfaces();
            Swal.fire({
              title: 'success',
              text: swalData.saveMsg,
              icon: 'success',
              timer: 1200,
              showConfirmButton: false
            });
            this.formError = false;
            this.createForm()

          }
          else {
            Swal.fire({
              title: 'Failed',
              text: swalData.saveMsg,
              icon: 'error',
              // timer: 1200,
              showConfirmButton: true
            });
          }

        }
      });
    }
  }
  public getLookupData() {

    this.evvServiceService.getLookUpData('billing_system').subscribe(res => {
      console.log(res);
      this.billingSystem = res.billingSystem;
    })
  }




  p(e) {

  }
  bsmodelRef: BsModalRef;

  public wizardApproach() {

    this.bsmodelRef = this.modalService.show(WizardInterfaceComponent, {
      backdrop: false,
      animated: true,
      keyboard: false,
      ignoreBackdropClick: false,
      class: 'wizard-modal-content',
      initialState: {

      }

    })
  }
}

export class NameValidator {
  static noWhiteSpace(control: AbstractControl): ValidationErrors | null {
    console.log(control.value);
    if (control.value != null) {
      let val = control.value?.trim();
      if (val.length == 0) {
        return { noWhiteSpace: true }

      }
      return null;
    } else {
      return null
    }
  }
}
