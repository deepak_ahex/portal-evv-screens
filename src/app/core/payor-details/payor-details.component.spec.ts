import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PayorDetailsComponent } from './payor-details.component';

describe('PayorDetailsComponent', () => {
  let component: PayorDetailsComponent;
  let fixture: ComponentFixture<PayorDetailsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PayorDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayorDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
