import { Component, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { UntypedFormBuilder } from '@angular/forms';
import { DashboardService } from 'src/app/services/dashboard.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-payor-details',
  templateUrl: './payor-details.component.html',
  styleUrls: ['./payor-details.component.scss']
})
export class PayorDetailsComponent implements OnInit {
  public payorForm: UntypedFormGroup;
  public payorMappingForm: UntypedFormGroup;
  public displayMapping: boolean = false;
  public userData: any;
  public interfacePayerDetailsId:number;

  public payorName: string = '';
  public payorFormError: boolean = false;

  public payerlist = [];

  public payorMappingList = [];

  public interfaceName: string = '';
  public interfaceId: number;
  constructor(private _fb: UntypedFormBuilder, private _dasboardService: DashboardService) {
    this.getInterFaceDetails();
  }


  ngOnInit(): void {
    console.log('payordetails')
  }


  public form() {
    this.payorFormError = false;
    this.payorForm = this._fb.group({
      sourceSystem: new UntypedFormControl(''),
      jurisdiction: new UntypedFormControl(''),
      payer: new UntypedFormControl(''),
      name: new UntypedFormControl(null, [Validators.required]),
      planCode: new UntypedFormControl(''),
      active: new UntypedFormControl(1, [Validators.required]),
      sortOrder: new UntypedFormControl(''),
      id: new UntypedFormControl(0, [Validators.required])
    })
  }
  get formcontrols() {
    return this.payorForm.controls;
  }
  public getInterfacePayerDetailsList() {
    try {
      this._dasboardService.getInterfacePayerDetailsList(this.interfaceId).subscribe(res => {
        console.log(res)
        this.payerlist = res;
      })
    } catch (error) {

    }
  }
  public getInterfacePayerDetailsById(id) {
    this.displayMapping = false;
    try {
      this._dasboardService.getInterfacePayerDetailsById(id).subscribe(
        res => {
          console.log(res);
          let obj: any = res;
          // console.log(obj)
          this.payorForm.get('sourceSystem').setValue(obj.sourceSystem);
          this.payorForm.get('jurisdiction').setValue(obj.jurisdiction);
          this.payorForm.get('payer').setValue(obj?.payerCode);
          this.payorForm.get('planCode').setValue(obj?.planCode);
          this.payorForm.get('active').setValue(obj.active);
          this.payorForm.get('sortOrder').setValue(obj.sortOrder);
          this.payorForm.get('id').setValue(obj.id);
          this.payorForm.get('name').setValue(obj.name);

        }
      )
    } catch (error) {

    }
  }
  public saveInterfacePayerDetails() {
    this.payorFormError = true;
    if (this.payorForm.valid) {
      let obj = { "interfaceDefinitionId": this.interfaceId, "id": this.payorForm.value.id, "name": this.payorForm.value.name, "sourceSystem": this.payorForm.value.sourceSystem, "jurisdiction": this.payorForm.value.jurisdiction, "payerCode": this.payorForm.value.payer, "planCode": this.payorForm.value.planCode, "active": this.payorForm.value.active, "userId": this.userData.userId, sortOrder: this.payorForm.value.sortOrder==null?'':this.payorForm.value.sortOrder};
      try {
        this._dasboardService.saveInterfacePayerDetails(obj).subscribe(
          res => {
            let data: any = res;
            if(data.saveFlag==0){
              Swal.fire({
                title: 'Error',
                text: data.saveMsg,
                icon: 'error',
                // timer: 1200,
                showConfirmButton: true
              });
            }else{
            Swal.fire({
              title: 'success',
              text: data.saveMsg,
              icon: 'success',
              timer: 1200,
              showConfirmButton: false
            });
            this.getInterfacePayerDetailsList();
            this.form()
          }
        }
        )
      } catch (error) {

      }
    }
  }

  public onActivechange(event, type?) {
    if (type == 'mapping') {
      this.payorMappingForm.get('active').setValue(event.target.checked ? 1 : 0);
    } else {
      this.payorForm.get('active').setValue(event.target.checked ? 1 : 0);
    }


  }


  //lookups

  public officeLookup = [];
  public payorPlanLookup=[];
  public getInterfaceOffices() {
    try {
          this._dasboardService.getInterfaceOffices(this.interfaceId).subscribe(
            res=>{
              console.log(res);
              let data:any=res
              this.officeLookup=data;
            }
          )
    } catch (error) {

    }
  }
  public getPayorPlanList(){
    if(this.payorMappingForm.value.officeId==0){
         this.payorPlanLookup=[];
         this.payorMappingForm.get('pocPayorPlanId').setValue(0);

    }else{
    try {
      this._dasboardService.getPayorPlanList(this.payorMappingForm.value.officeId).subscribe(res=>{
        console.log(res);
        let data:any=res;
        this.payorPlanLookup=data;
      })
    } catch (error) {

    }}
  }

  // Mapping functionlity will start from here




  public payorMapForm() {
    this.payorFormError = false;
    this.payorMappingForm = this._fb.group({
      pocPayorPlanId: new UntypedFormControl(0, [Validators.required]),
      officeId: new UntypedFormControl(0, [Validators.required]),
      active: new UntypedFormControl(1, [Validators.required]),
      interfacePocPayersMapId: new UntypedFormControl(0)
    })
  }
  get payorMapFormControls() {
    return this.payorMappingForm.controls;
  }
  public updatePayormapping(obj) {
    console.log(obj)
    this.payorMappingForm.get('pocPayorPlanId').setValue(obj.pocPayorPlanId);
    this.payorMappingForm.get('officeId').setValue(obj.officeId);
    this.payorMappingForm.get('active').setValue(obj.active);
    this.payorMappingForm.get('interfacePocPayersMapId').setValue(obj.interfacePocPayersMapId);

    this.getPayorPlanList();

  }

  public payorMapping(obj) {
    this.displayMapping = true;
    this.payorName = obj.name;
    this.payorMapForm();
    this.getInterfaceOffices();
    this.getInterfacePocPayersMapList(obj.id);

  }
  public getInterfacePocPayersMapList(id?) {
    this.interfacePayerDetailsId=id;
    try {
      this._dasboardService.getInterfacePocPayersMapList(id).subscribe(res => {
        console.log(res)
        let data: any = res;
        this.payorMappingList = data;
      })
    } catch (error) {

    }
  }

  public getIntarfacePayorMappingById(id) {

  }

  public saveInterfacePayorMapping() {
    console.log(this.payorMappingForm.valid)
    this.payorFormError = true;
    if (this.payorMappingForm.value.pocPayorPlanId != 0 && this.payorMappingForm.value.officeId != 0) {
      let obj={"interfacePayerDetailsId":this.interfacePayerDetailsId,"id":this.payorMappingForm.value.interfacePocPayersMapId,"officeId":this.payorMappingForm.value.officeId,"payorPlanId":this.payorMappingForm.value.pocPayorPlanId,"active":this.payorMappingForm.value.active,"userId":this.userData.userId}
      try {
        this._dasboardService.saveInterfacePayorMapping(obj).subscribe(
          res => {
            console.log(res);
            let data: any = res;
            if (data.saveFlag == 1) {
              Swal.fire({
                title: 'success',
                text: data.saveMsg,
                icon: 'success',
                timer: 1200,
                showConfirmButton: false
              });
              this.getInterfacePocPayersMapList(this.interfacePayerDetailsId);
              this.payorMapForm();
            } else {
              Swal.fire({
                title: 'Failed',
                text: data.saveMsg,
                icon: 'error',
                timer: 1200,
                showConfirmButton: false
              });
            }
          })
      } catch (error) {

      }
    }
  }








  public getInterFaceDetails() {
    this.displayMapping=false;
    var data = JSON.parse(sessionStorage.getItem('interfaceData'));
    this.userData = JSON.parse(sessionStorage.getItem('userData'));
    // console.log(data.name)
    this.interfaceName = data?.label;
    this.interfaceId = data?.id;
    this.form();
    this.getInterfacePayerDetailsList();
  }
}
