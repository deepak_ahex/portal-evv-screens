import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EvvServiceService } from 'src/app/services/evv-service.service';

@Component({
  selector: 'app-pending-dashboard',
  templateUrl: './pending-dashboard.component.html',
  styleUrls: ['./pending-dashboard.component.scss']
})
export class PendingDashboardComponent implements OnInit {
  public InterfaceDetailsList: any = [];
  public InterfaceDataSetDetailsList: any = [];



  private payload: any;
  public DashboardData:any;
  constructor(private evvServiceService: EvvServiceService,private router:Router) {

   }

  ngOnInit(): void {
    this.getDashboardData();
  }
  getDashboardData(): void {
    this.payload = {interfaceDefinitionId: 0};
    this.evvServiceService.getEvvDashBoardData(JSON.stringify(this.payload)).subscribe((res) => {
      console.log(res);
      this.DashboardData = res;
    });
  }

  navigate(event){
    this.router.navigateByUrl(event)
  }



}
