import { DatePipe } from '@angular/common';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DashboardService } from 'src/app/services/dashboard.service';
import { EvvServiceService } from 'src/app/services/evv-service.service';
import { exportdetails } from '../../reconcilation/reconcilation.component';
import $ from 'jquery'
import Swal from 'sweetalert2';

@Component({
  selector: 'app-pending-reason',
  templateUrl: './pending-reason.component.html',
  styleUrls: ['./pending-reason.component.scss']
})
export class PendingReasonComponent implements OnInit {

  @ViewChild('ps') ps;
  @ViewChild('dcs') dcs;
  @ViewChild('service') service;


  public modalRef: BsModalRef;
  public interfaceId = null;
  public interfaceList = [];
  public psFilter = [];
  public dcsFilter = [];
  public serviceFilter = [];
  public payorFilter = [];
  public startDate: Date = new Date();
  public endDate: Date = new Date();
  public lowerBound: number = 1;
  public perPage: number = 100;
  public upperBound: number = 100;
  public payorId: number = null;

  public psFilterselected: any = null;
  public dcsFilterSelected: any = null;
  public serviceFilterSelected: any = null;
  public visitRequestData = [];
  public responseMessage = '';
  public userData: any;
  public totalCount = 0;

  public currentCorrection: any;
  public correctioncheckall = 0;
  public correctionSaveButton: boolean = false;
  public resubmitTransactionList: Array<any> = [];
  public complaintlistArrayDisplay = [];
  public appliedInterfaceId = 0;
  public visiitDetails = [];

  private appliedpsId = 0;
  private applieddcsId = 0;
  private appliedpayorId = 0;
  private appliedServiceId = 0;
  private appliedStartdate = '';
  private appliedEnddate = '';
  public orderType = 'asc';
  public orderBy = 'scheduled start date';
  public currentorderby = 'scheduled start date';
  public currentorderType = 'asc';
  public visiitDetailsCheckedAll: boolean = false;
  public statusList=[{name:'Exported',id:1},{name:'Accepted',id:2},{name:'Rejected',id:3}]

  constructor(public bsModalService: BsModalService, public dashboardservice: DashboardService, public evvServiceService: EvvServiceService, public datepipe: DatePipe, private modalService: BsModalService) {
    this.userData = JSON.parse(sessionStorage.getItem('userData'));
    this.startDate.setDate(this.endDate.getDate() - 7);
  }

  ngOnInit(): void {
    this.getInterfaceByDataSet();
  }

  public getRejectedVisitList() {
    this.visiitDetails = [];
    this.visiitDetailsCheckedAll = false;
    let obj = { "interfaceDefinitionId": this.appliedInterfaceId, "psId": this.appliedpsId, "dcsId": this.applieddcsId, "serviceId": this.appliedServiceId, "payorId": this.appliedpayorId, "startDate": this.appliedStartdate, "endDate": this.appliedEnddate, "lowerBound": this.lowerBound, "upperBound": this.upperBound, "orderBy": this.orderBy, "order": this.orderType,"officeId":this.appliedOfficeId};
    try {
      this.dashboardservice.getRejectedVisitList(JSON.stringify(obj)).subscribe(res => {
        console.log(res);;
        this.resubmitTransactionList = res.rejectedVisitsArray;
        this.totalCount = res.totalCount
      })
    } catch (error) {

    }

  }

  public getInterfaceByDataSet(): void {

    this.dashboardservice.getInterfaceByDataSet('Completed Visits').subscribe((res) => {
      let data: any = res;
      console.log(data)
      this.interfaceList = data;
    });
  }
  public getFilterDataByDataSet() {
    this.dashboardservice.getFilterDataByDataSet(this.interfaceId,'Completed Visits').subscribe(data => {
      console.log(data);
      this.filterDataByDataSetResponse = data;

      if(this.includeInActivePS){
        this.psFilter = data.psList;
      }else{
        this.psFilter = data.psList.filter(x => x.active == 1);
      }

      if(this.includeInActiveDCS){
        this.dcsFilter = data.dcsList;
      }else{
        this.dcsFilter = data.dcsList.filter(x => x.active == 1);
      }
      this.serviceFilter = data.serviceList;
      this.payorFilter = data.PayerList;
    })
  }



  public onGo() {

    let startdate = Date.parse(this.datepipe.transform(this.startDate, 'MM/dd/yyyy'));
    let endate = Date.parse(this.datepipe.transform(this.endDate, 'MM/dd/yyyy'));
    let sysdate = new Date
    if (this.endDate == null || this.startDate == null) {
      Swal.fire({
        title: 'Invalid Dates',
        text: 'Start Date and End Date are Mandatory fields',
        icon: 'warning',
        // timer: 1200,
        showConfirmButton: true
      });
    } else
      if (this.interfaceId == null) {
        Swal.fire({
          title: 'Invalid Search',
          text: 'Please select an Interface !',
          icon: 'warning',
          // timer: 1200,
          showConfirmButton: true
        });
      } else if (startdate > endate || endate > Date.parse(this.datepipe.transform(sysdate, 'MM/dd/yyyy'))) {
        Swal.fire({
          title: 'Invalid Dates',
          text: endate > Date.parse(this.datepipe.transform(sysdate, 'MM/dd/yyyy')) ? `End Date cannot be greater than ${this.datepipe.transform(sysdate, 'MM/dd/yyyy')} ` : 'Start Date cannot be greater than End Date',
          icon: 'warning',
          // timer: 1200,
          showConfirmButton: true
        });
      }
      else {
        this.appliedInterfaceId = this.interfaceId??0;
        this.appliedpsId = this.psFilterselected??0;
        this.applieddcsId = this.dcsFilterSelected??0;
        this.appliedServiceId = this.serviceFilterSelected??0;
        this.appliedpayorId = this.payorId??0;
        this.appliedOfficeId=this.officeId??0;
        this.appliedStartdate = this.datepipe.transform(this.startDate, 'MM/dd/yyyy');
        this.appliedEnddate = this.datepipe.transform(this.endDate, 'MM/dd/yyyy');
        this.pageChange();
      }

    console.log("ps", typeof (this.psFilter), this.psFilterselected, "dcs", this.dcsFilterSelected, "servicde", this.serviceFilterSelected)



  }
  public pageChange() {
    this.lowerBound = 1;
    this.upperBound = this.perPage;
    if (this.appliedInterfaceId != 0) {
      this.getRejectedVisitList();

    }
  }
  public selectEvent(event, flag) {
    console.log(event)
    if (flag == 'ps') {
      this.psFilterselected = event.psId
    } else if (flag == 'dcs') {
      this.dcsFilterSelected = event.dcsId;
    } else if (flag == "service") {
      this.serviceFilterSelected = event.serviceId;
    }

  }
  public filterClear(flag) {
    console.log("clear")
    if (flag == 'ps') {
      this.psFilterselected = 0;
      this.ps.close()
    } else if (flag == 'dcs') {
      this.dcsFilterSelected = 0;
      this.dcs.close()
    } else if (flag == "service") {
      this.serviceFilterSelected = 0;
      this.service.close()

    }

  }
  public filterChange() {
    // this.psFilterselected != 0 ? this.ps.clear() : '';
    // this.dcsFilterSelected != 0 ? this.dcs.clear() : '';
    // this.serviceFilterSelected != 0 ? this.service.clear() : '';
    // this.payorId = 0;
    // this.officeId=0;

    // if (this.interfaceId == 0) {
    //   this.psFilter = [];
    //   this.payorFilter = [];
    //   this.serviceFilter = [];
    //   this.officeList=[];
    // } else {
      this.getFilterDataByDataSet();
      this.getInterfaceOffices();

    // }


  }
  public prevpage(): void {
    this.lowerBound = this.lowerBound - this.perPage;
    this.upperBound = this.upperBound - this.perPage;
    this.getRejectedVisitList();

  }
  public nextPage(): void {
    this.lowerBound = this.lowerBound + this.perPage;
    this.upperBound = this.upperBound + this.perPage;
    this.getRejectedVisitList();

  }

  onresponse(temlate: TemplateRef<any>, messsage) {
    this.modalRef = this.bsModalService.show(temlate, {
      class: '  bd-example-modal-lg'
    })
    this.responseMessage = messsage

  }
  public getExportedDataDetails(data, recordDetails: TemplateRef<any>) {
    console.log(data)
    try {
      this.dashboardservice.getExportedDataDetails(data.exportedDataId, data.interfaceDefinitionId, data.interfaceDatasetId,'exported_visit_data').subscribe(
        res => {
          console.log(res);
          this.visitRequestData = res;
          this.modalRef = this.modalService.show(recordDetails);

        },
        err => {
          Swal.fire({
            title: 'Failed to Load',
            text: 'Something Went Wrong',
            icon: 'error',
            // timer: 1200,
            showConfirmButton: true
          });
        }
      )
    } catch (error) {

    }

  }
  public getCorrectionFileds(temlate: TemplateRef<any>, data) {
    this.currentCorrection = data;
    this.correctioncheckall = 0;
    this.correctionSaveButton = false;
    let obj = { "exportedDataId": data.exportedDataId, "interfaceDefinitionId": data.interfaceDefinitionId, "interfaceDatasetId": data.interfaceDatasetId }
    try {
      this.dashboardservice.getCorrectionFileds(JSON.stringify(obj)).subscribe(res => {
        console.log(res);
        this.complaintlistArrayDisplay = res.correctionFiledsArray;

        this.complaintlistArrayDisplay.map(x => {
          x.checked = 0;
          x.newColumnName = x.interfaceColumnName;
          x.newColumnValue = x?.interfaceColumnValue?x.interfaceColumnValue:'';
        })

        this.modalRef = this.bsModalService.show(temlate, {
          class: '  bd-example-modal-lg'
        })
        console.log(this.complaintlistArrayDisplay)
      })
    } catch (error) {

    }

  }

  public correctionChecked(i, event, flag?: boolean) {
    this.correctionSaveButton = false;
    if (flag) {
      this.correctioncheckall = event.target.checked ? 1 : 0;
    } else {
      this.complaintlistArrayDisplay[i].checked = event.target.checked ? 1 : 0;
    }
    this.complaintlistArrayDisplay.map(x => {
      if (x.checked == 1) {
        this.correctionSaveButton = true;
      }

    })
  }

  public onvisitCheck(visitId, event) {
    if (event.target.checked) {
      this.visiitDetails.push(visitId);
      console.log(this.visiitDetails)

    } else {
      this.visiitDetails = this.visiitDetails.filter(x => x != visitId);
      console.log(this.visiitDetails)
    }

  }
  public onCheckAll(event) {
    if (event.target.checked) {
      this.visiitDetailsCheckedAll = true;
      this.visiitDetails = this.resubmitTransactionList.map(x => {
        return x.visitDetailsId
      })
    } else {
      this.visiitDetailsCheckedAll = false;
      this.visiitDetails = [];
    }
  }
  public repushRejectedRecords() {
    let obj = { "visitDetailIds": this.visiitDetails.toString(), "userId": this.userData.userId }
    try {
      this.dashboardservice.repushRejectedRecords(obj).subscribe(res => {
        console.log(res);
        if (res.saveFlag == '1') {
          Swal.fire('Success', res.saveMsg, 'success');
          this.getRejectedVisitList();
          this.visiitDetailsCheckedAll = false;
        } else {
          Swal.fire('Failed', 'Failed to save Data', 'error');

        }
      })
    } catch (error) {

    }
  }
  public dismissRejectedRecords() {
    let obj = { "visitDetailIds": this.visiitDetails.toString(), "userId": this.userData.userId };

    Swal.fire({
      // title: 'Auto close alert!',
      text: 'Dismissing the selected record(s) will remove them from this screen and are not reversible. Are you sure you want to proceed?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Yes, Dismiss'
      }).then(result=>{
        if (result.isConfirmed) {

    try {
      this.dashboardservice.dismissRejectedRecords(obj).subscribe(res => {
        console.log(res);
        if (res.saveFlag == '1') {
          Swal.fire('Success', res.saveMsg, 'success');
          this.getRejectedVisitList();
          this.visiitDetailsCheckedAll = false;
        } else {
          Swal.fire('Failed', 'Failed to save Data', 'error');

        }
      })
    } catch (error) {

    }
  }
})
  }

  public resubmitCorrectionRecord() {
    let resubmitColumnsList = [];

    this.complaintlistArrayDisplay.map(x => {
      let obj = {
        "id": x.resbmitColumnMappingId,
        "isChecked": x.checked,
        "interfaceColumnName": x.interfaceColumnName,
        "interfaceColumnValue": x.checked == 1 ? x.newColumnValue : x.interfaceColumnValue,
        "interfaceColumnOldValue": x?.interfaceColumnValue?x.interfaceColumnValue:'',
        "updatedTableName": x.updatedTableName,
        "updatedColumnName": x.updatedColumnName
      }
      resubmitColumnsList.push(obj);

    })


    let postJsonObj = {
      "resubmitColumnsList": resubmitColumnsList,
      "checkAll": this.correctioncheckall,
      "visitDetailsId": this.currentCorrection.visitDetailsId,
      "interfaceDefinitionId": this.currentCorrection.interfaceDefinitionId,
      "interfaceDatasetId": this.currentCorrection.interfaceDatasetId,
      "psId": this.currentCorrection.psId,
      "interfacePayorId": this.currentCorrection.interfacePayorId,
      "searchPSId": this.appliedpsId,
      "searchDCSId": this.applieddcsId,
      "searchServiceId": this.appliedServiceId,
      "searchInterfacePayorId": this.appliedpayorId,
      "searchStartDate": this.appliedStartdate,
      "searchEndDate": this.appliedEnddate,
      "userId": this.userData.userId,
      "searchOfficeId":this.appliedOfficeId
    }
    try {
      this.dashboardservice.resubmitCorrectionRecord(postJsonObj).subscribe(res => {
        console.log(res);
        if (res.saveFlag == '1') {
          Swal.fire('Success', res.saveMsg, 'success');
          this.getRejectedVisitList();
          this.modalRef.hide()
        } else {
          Swal.fire('Failed', 'Failed to save Data', 'error');

        }
      })
    } catch (error) {

    }
  }


  public oncorrectionSave() {

    let alertmesages = [];
    let nullmessages=[];
    this.complaintlistArrayDisplay.map(x => {
      if (x.checked == 1) {
        console.log(x.newColumnValue.length==0)
        if (x.interfaceColumnValue == x.newColumnValue) {
          alertmesages.push(x.interfaceColumnName)
          console.log(x.newColumnValue.length==0)
        }else{

        }
        x.newColumnValue.length==0? nullmessages.push(x.interfaceColumnName):null;

      }
    })
    if (alertmesages.length > 0) {
      Swal.fire({
        title: 'Invalid',
        html: `<h4>No Change found in </h4><br><span  style="color: #4272a8;"> <b>${alertmesages.join(', ')}</b></span>`,
        icon: 'warning',
        // timer: 1200,
        showConfirmButton: true
      });
    }else if(nullmessages.length>0){
      Swal.fire({
        title: 'Invalid ',
        html:`<br><h4>Below ${nullmessages.length>1?'values':'value'}  cannot be empty </h4><b> <span style="color: #4272a8;"> ${nullmessages.join(', ')}<span></b>`,
        icon: 'warning',
        // timer: 1200,
        showConfirmButton: true
      });
    }
     else {
      this.correctionSaveButton ? this.resubmitCorrectionRecord() : this.modalRef.hide();
    }
  }

  public sortorder(orderBy, orderType) {
    if (this.currentorderby != orderBy || this.currentorderType != orderType) {
      this.orderType = orderType;
      this.orderBy = orderBy;
      this.currentorderby = orderBy;
      this.currentorderType = orderType;
      this.pageChange();
    }
  }

  public officeList = [];
  public officeId=null;
  private appliedOfficeId=0;
  public getInterfaceOffices() {
    try {
      this.dashboardservice.getInterfaceOffices(this.interfaceId).subscribe(res => {
        console.log(res);
        let data: any = res;
        this.officeList = data;
      })
    } catch (error) {

    }
  }

  public includeInActiveDCS=false;
  public filterDataByDataSetResponse: any;
  public includeInActivePS = false;
  public onIncludeInactiveChange(event, flag) {
    flag=="PS"? this.includeInActivePS = event:this.includeInActiveDCS=event;
    console.log(event);
    if (event) {
      if (flag == 'PS') {
        this.psFilter = this.filterDataByDataSetResponse.psList;
      }else{
        this.dcsFilter = this.filterDataByDataSetResponse.dcsList;
      }
    } else {
      if (flag == 'PS') {
        this.psFilter = this.filterDataByDataSetResponse.psList.filter(x => x.active == 1);
      }else{
        this.dcsFilter = this.filterDataByDataSetResponse.dcsList.filter(x => x.active == 1);
      }

    }


  }

}
