import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingReasonComponent } from './pending-reason.component';

describe('PendingReasonComponent', () => {
  let component: PendingReasonComponent;
  let fixture: ComponentFixture<PendingReasonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PendingReasonComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PendingReasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
