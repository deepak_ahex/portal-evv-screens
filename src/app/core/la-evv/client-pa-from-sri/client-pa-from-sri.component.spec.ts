import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientPaFromSriComponent } from './client-pa-from-sri.component';

describe('ClientPaFromSriComponent', () => {
  let component: ClientPaFromSriComponent;
  let fixture: ComponentFixture<ClientPaFromSriComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientPaFromSriComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientPaFromSriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
