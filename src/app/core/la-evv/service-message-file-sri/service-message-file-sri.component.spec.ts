import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceMessageFileSriComponent } from './service-message-file-sri.component';

describe('ServiceMessageFileSriComponent', () => {
  let component: ServiceMessageFileSriComponent;
  let fixture: ComponentFixture<ServiceMessageFileSriComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceMessageFileSriComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceMessageFileSriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
