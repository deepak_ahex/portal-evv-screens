import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ReviewComponent } from '../popup/review/review.component';

@Component({
  selector: 'app-service-message-file-sri',
  templateUrl: './service-message-file-sri.component.html',
  styleUrls: ['./service-message-file-sri.component.sass']
})
export class ServiceMessageFileSriComponent implements OnInit {

  bsModalRef?: BsModalRef;
  constructor( private modalService: BsModalService) {
   }

  ngOnInit(): void {
  }
  getReviewModal(){
    this.bsModalRef = this.modalService.show(ReviewComponent, { class: 'full-width-dialog' });
  }
}
