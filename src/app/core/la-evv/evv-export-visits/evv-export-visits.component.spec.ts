import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EvvExportVisitsComponent } from './evv-export-visits.component';

describe('EvvExportVisitsComponent', () => {
  let component: EvvExportVisitsComponent;
  let fixture: ComponentFixture<EvvExportVisitsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EvvExportVisitsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EvvExportVisitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
