import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaEvvClientsComponent } from './la-evv-clients.component';

describe('LaEvvClientsComponent', () => {
  let component: LaEvvClientsComponent;
  let fixture: ComponentFixture<LaEvvClientsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaEvvClientsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaEvvClientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
