import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router';

@Component({
  selector: 'app-la-evv-clients',
  templateUrl: './la-evv-clients.component.html',
  styleUrls: ['./la-evv-clients.component.sass']
})
export class LaEvvClientsComponent implements OnInit {
  public url;
  public filter;
  public title;
  public client = false;
  public pa = false;
    constructor(public route:Router) { }

    ngOnInit(): void {
      this.url = this.route.url;
      if(this.url == '/client-file-from-sri'){
        this.filter = [{lastName:true,firstName:true,UIDClient:true,ssn:true}];
        this.title= "LA EVV Clients";
        this.pa = false;
        this.client = true;
      }
      console.log(this.filter);
      if(this.url == '/pa-file-from-sri'){
        this.filter = [{paNumber:true,procedureCode:true,UIDClient:true,medicalId:true}];
        this.title= "LA EVV PA";
        this.client = false;
        this.pa = true;
      }
    }

}
