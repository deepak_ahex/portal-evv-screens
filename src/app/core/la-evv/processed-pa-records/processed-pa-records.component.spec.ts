import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessedPaRecordsComponent } from './processed-pa-records.component';

describe('ProcessedPaRecordsComponent', () => {
  let component: ProcessedPaRecordsComponent;
  let fixture: ComponentFixture<ProcessedPaRecordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessedPaRecordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessedPaRecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
