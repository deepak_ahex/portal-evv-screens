import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ClientFileFromSriComponent } from '../../client-file-from-sri/client-file-from-sri.component';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.sass']
})
export class ReviewComponent implements OnInit {
public url;
public title;
public pa;
public service;
public client;
  constructor(public route:Router) { }
  ngOnInit(): void {
    this.url = this.route.url;
    if(this.url == '/pa-file-from-sri'){
      this.title = 'LA EVV PA - Review';
      this.pa = true;
      this.service = false;
    }
    else if(this.url == '/service-message-file-sri'){
      this.title = 'LA EVV Service Msg - Review';
      this.pa = false;
      this.service = true;
    }
    else if(this.url == '/client-file-from-sri'){
      this.title = 'LA EVV Client - Review';
      this.pa = false;
      this.client = true;
      this.service= false;
    }
  }

}
