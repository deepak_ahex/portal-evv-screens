import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { LaEvvClientsComponent } from '../../la-evv-clients/la-evv-clients.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-laevv-master',
  templateUrl: './laevv-master.component.html',
  styleUrls: ['./laevv-master.component.scss']
})
export class LaevvMasterComponent implements OnInit {

  bsModalRef?: BsModalRef;

  constructor( private modalService: BsModalService) {
   }

  ngOnInit(): void {
    Swal.fire({
      title: 'Please process client record then come to pa records.',
      showCancelButton: true,
      confirmButtonText: 'Ok, continue',
      icon: 'question'
    })
  }
  openClients(){
    this.bsModalRef = this.modalService.show(LaEvvClientsComponent, { class: 'full-width-dialog' });
  }
}


