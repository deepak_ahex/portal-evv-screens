import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LaevvMasterComponent } from './laevv-master.component';

describe('LaevvMasterComponent', () => {
  let component: LaevvMasterComponent;
  let fixture: ComponentFixture<LaevvMasterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LaevvMasterComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LaevvMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
