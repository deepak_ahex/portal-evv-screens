import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LaevvMasterPaComponent } from './laevv-master-pa.component';

describe('LaevvMasterPaComponent', () => {
  let component: LaevvMasterPaComponent;
  let fixture: ComponentFixture<LaevvMasterPaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LaevvMasterPaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LaevvMasterPaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
