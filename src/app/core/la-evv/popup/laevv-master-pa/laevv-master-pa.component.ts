import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { LaEvvClientsComponent } from '../../la-evv-clients/la-evv-clients.component';

@Component({
  selector: 'app-laevv-master-pa',
  templateUrl: './laevv-master-pa.component.html',
  styleUrls: ['./laevv-master-pa.component.scss']
})
export class LaevvMasterPaComponent implements OnInit {


  bsModalRef?: BsModalRef;

  constructor( private modalService: BsModalService) {
   }

  ngOnInit(): void {
  }
  openClients(){
    this.bsModalRef = this.modalService.show(LaEvvClientsComponent, { class: 'full-width-dialog' });
  }

}
