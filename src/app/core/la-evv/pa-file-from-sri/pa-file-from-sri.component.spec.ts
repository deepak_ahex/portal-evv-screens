import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaFileFromSriComponent } from './pa-file-from-sri.component';

describe('PaFileFromSriComponent', () => {
  let component: PaFileFromSriComponent;
  let fixture: ComponentFixture<PaFileFromSriComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaFileFromSriComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaFileFromSriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
