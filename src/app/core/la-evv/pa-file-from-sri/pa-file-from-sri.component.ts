import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { LaEvvClientsComponent } from '../la-evv-clients/la-evv-clients.component';
import { LaevvMasterPaComponent } from '../popup/laevv-master-pa/laevv-master-pa.component';
import { ReviewComponent } from '../popup/review/review.component';
// import { ApiserviceService } from 'src/app/services/apiservice.service';

@Component({
  selector: 'app-pa-file-from-sri',
  templateUrl: './pa-file-from-sri.component.html',
  styleUrls: ['./pa-file-from-sri.component.sass']
})
export class PaFileFromSriComponent implements OnInit {
  bsModalRef?: BsModalRef;
  constructor( private modalService: BsModalService) {
   }
  ngOnInit(): void {
  }
  getReviewModal(){
    this.bsModalRef = this.modalService.show(ReviewComponent, { class: 'full-width-dialog' });
  }
  openLaEvvClients() {
    this.bsModalRef = this.modalService.show(LaevvMasterPaComponent, { class: 'full-width-dialog' });
  }
}
