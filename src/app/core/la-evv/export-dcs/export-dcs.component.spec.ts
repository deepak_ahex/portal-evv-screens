import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportDcsComponent } from './export-dcs.component';

describe('ExportDcsComponent', () => {
  let component: ExportDcsComponent;
  let fixture: ComponentFixture<ExportDcsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExportDcsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExportDcsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
