import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientFileFromSriComponent } from './client-file-from-sri.component';

describe('ClientFileFromSriComponent', () => {
  let component: ClientFileFromSriComponent;
  let fixture: ComponentFixture<ClientFileFromSriComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientFileFromSriComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientFileFromSriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
