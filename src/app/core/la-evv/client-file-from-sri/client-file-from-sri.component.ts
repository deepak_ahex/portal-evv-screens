import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
// import { ApiserviceService } from 'src/app/services/apiservice.service';
import { ReviewComponent } from '../popup/review/review.component';
import { LaEvvClientsComponent } from '../la-evv-clients/la-evv-clients.component';
import { LaevvMasterComponent } from '../popup/laevv-master/laevv-master.component';

@Component({
  selector: 'app-client-file-from-sri',
  templateUrl: './client-file-from-sri.component.html',
  styleUrls: ['./client-file-from-sri.component.sass']
})
export class ClientFileFromSriComponent implements OnInit {
  bsModalRef?: BsModalRef;
  public fileType= [{name:'Client'},{name:'PA'}];
  constructor( private modalService: BsModalService) {
   }

  ngOnInit(): void {
  }
  getReviewModal(){
    this.bsModalRef = this.modalService.show(ReviewComponent, { class: 'full-width-dialog' });
  }
  openLaEvvClients() {
    this.bsModalRef = this.modalService.show(LaevvMasterComponent, { class: 'full-width-dialog' });
  }
}
