import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, Validators } from '@angular/forms';
import { DashboardService } from 'src/app/services/dashboard.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-interface-status',
  templateUrl: './interface-status.component.html',
  styleUrls: ['./interface-status.component.scss']
})
export class InterfaceStatusComponent implements OnInit {

  public interfaceName: string = '';
  public interfaceId: number;
  public interfaceStatusForm: UntypedFormGroup;
  public formError: boolean = false;
  public exportTableLookup=[];
  public userData: any;
  public statusList = [];
  public interFaceDatasetList = [];
  constructor(private _fb: UntypedFormBuilder, private _dashBoardService: DashboardService) {
    this.getInterFaceDetails()
  }
  ngOnInit(): void {
  }
  public createForm() {
    this.formError = false;
    this.interfaceStatusForm = this._fb.group({
      id: new UntypedFormControl(0),
      interfaceDatasetId: new UntypedFormControl(null, [Validators.required]),
      userName: new UntypedFormControl('',),
      password: new UntypedFormControl('',),
      url: new UntypedFormControl('', [Validators.required]),
      datasetTypeId: new UntypedFormControl(null, [Validators.required]),
      clientId: new UntypedFormControl('',),
      clientSecret: new UntypedFormControl(''),
      active: new UntypedFormControl(1),
      providerId:new UntypedFormControl(null,Validators.required),
      account:new UntypedFormControl(''),
      oldDatsetTypeid:new UntypedFormControl(0)
    })
  }

  get formControls() {
    return this.interfaceStatusForm.controls;
  }
  public getInterfaceDatasetList() {
    try {
      this._dashBoardService.getInterfaceDatasetList(this.interfaceId).subscribe(res => {
        this.interFaceDatasetList = res;
      })
    } catch (error) {

    }
  }
  public getStatusMediumDetailsList() {
    console.log("resss")
    try {
      this._dashBoardService.getStatusMediumDetailsList(this.interfaceId).subscribe(
        res => {
          console.log(res)
          this.statusList = res;
        }
      )
    } catch (error) {

    }
  }

  public saveStatusMediumDetails() {
    this.formError = true;
    let obj = {
      "interfaceDefinitionId": this.interfaceId,
      "datasetTypeId":this.interfaceStatusForm.value.datasetTypeId,
      "interfaceDatasetId": this.interfaceStatusForm.value.interfaceDatasetId,
      "url":  this.interfaceStatusForm.value.url,
      "userName":  this.interfaceStatusForm.value.userName,
      "password":  this.interfaceStatusForm.value.password,
      "clientId": this.interfaceStatusForm.value.clientId,
      "clientSecret": this.interfaceStatusForm.value.clientSecret,
      "account": this.interfaceStatusForm.value.account,
      "providerId": this.interfaceStatusForm.value.providerId,
      "actvie": this.interfaceStatusForm.value.active,
      "id": this.interfaceStatusForm.value.id,
      "userId": this.userData.userId,
      "oldDatsetTypeid":this.interfaceStatusForm.value.oldDatsetTypeid
    }
    console.log("finalOBJ", obj)
    if (this.interfaceStatusForm.valid) {
      try {
        console.log()
        this._dashBoardService.saveStatusMediumDetails(obj).subscribe(res => {
          console.log(res)
          let data: any = res;
          if (data.saveFlag == 1) {
            Swal.fire({
              title: 'success',
              text: data.saveMsg,
              icon: 'success',
              timer: 1200,
              showConfirmButton: false
            });
            this.getStatusMediumDetailsList();
            this.createForm();
          } else {
            Swal.fire({
              title: 'Failed',
              text: data.saveMsg,
              icon: 'error',
              timer: 1200,
              showConfirmButton: false
            });
          }
        })
      } catch (error) {
      }
    }

  }

  public updateForm(obj) {
    console.log(obj)

    this.interfaceStatusForm.get('interfaceDatasetId').setValue(obj.interfaceDatasetId);
    this.interfaceStatusForm.get('datasetTypeId').setValue(obj.datasetTypeId);
    this.interfaceStatusForm.get('userName').setValue(obj.userName);
    this.interfaceStatusForm.get('url').setValue(obj.url);
    this.interfaceStatusForm.get('password').setValue(obj.password);
    this.interfaceStatusForm.get('id').setValue(obj.id);
    this.interfaceStatusForm.get('clientId').setValue(obj.clientId);
    this.interfaceStatusForm.get('clientSecret').setValue(obj.clientSecret);
    this.interfaceStatusForm.get('account').setValue(obj.account);
    this.interfaceStatusForm.get('providerId').setValue(obj.providerId);
    this.interfaceStatusForm.get('active').setValue(obj.active);
    this.interfaceStatusForm.get('id').setValue(obj.id);
    this.interfaceStatusForm.get('oldDatsetTypeid').setValue(obj.datasetTypeId)



 }

 onActiveChange(event){
  this.interfaceStatusForm.get('active').setValue(event.target.checked?1:0);
}













  public getInterFaceDetails() {
    var data = JSON.parse(sessionStorage.getItem('interfaceData'));
    this.userData = JSON.parse(sessionStorage.getItem('userData'));

    // console.log(data.name)
    this.interfaceName = data?.label;
    this.interfaceId = data?.id;
    this.getInterfaceDatasetList();
    this.getStatusMediumDetailsList();
    this.getInterfaceExportedTables();
    this.createForm();
  }



  public getInterfaceExportedTables(){
  this._dashBoardService.getInterfaceExportedTables().subscribe(res=>{
  this.exportTableLookup=res;
  })
}

}
