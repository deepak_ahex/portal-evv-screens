import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';
import { EvvServiceService } from 'src/app/services/evv-service.service';
import Swal from 'sweetalert2';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import {TooltipModule} from 'primeng/tooltip';
@Component({
  selector: 'app-reconcilation',
  templateUrl: './reconcilation.component copy.html',
  styleUrls: ['./reconcilation.component.scss']
})
export class ReconcilationComponent implements OnInit {
  @ViewChild('ps') ps;
  @ViewChild('dcs') dcs;
  @ViewChild('service') service;

  public defaultrecords = 0;
  public site: boolean;
  public exportDetailsArray: Array<exportdetails> = [];
  public interfaceDefinitionId: number = null;
  public psId: number = null;
  public dcsId: number = null;
  public serviceId: number = null;
  public payorId: number = null;
  public exportStatus: number = null;
  public startDate: Date = new Date();
  public endDate: Date = new Date();
  public systemDate: Date = new Date();
  public perpage: number = 10
  public lowerBound: number = 1;
  public upperBound: number = this.perpage;
  public InterfaceDetailsList: Array<any> = [];
  public totalCount: number = 0;
  public payerList: Array<any> = [];
  public dcsList: Array<any> = [];
  public psList: Array<any> = [];
  public serviceList: Array<any> = [];
  public orderType: string = 'asc';
  public orderBy: string = "evd.scheduled_begin_date";
  public currentorderby: string = 'schedulestart';
  public modalRef: BsModalRef;
  public visitsLogModalRef: BsModalRef;
  public statusList=[{name:'Exported',id:1},{name:'Accepted',id:2},{name:'Rejected',id:3}]

  public visitRequestData = [];
  constructor(public dashboardservice: DashboardService, public evvServiceService: EvvServiceService, public datepipe: DatePipe, private modalService: BsModalService) {
    this.startDate.setDate(this.endDate.getDate() - 7);

  }


  ngOnInit(): void {
    TooltipModule;
    // this.getReconciliationCompVisitList();

    this.getInterfaceByDataSet();
  }
  public applyfilter() {
    if (this.endDate == null || this.startDate == null) {
      Swal.fire({
        title: 'Invalid Dates',
        text: 'Start Date and End Date are Mandatory fields',
        icon: 'warning',
        // timer: 1200,
        showConfirmButton: true
      });
    } else
      if (this.interfaceDefinitionId == null) {
        Swal.fire({
          title: 'Invalid Search',
          text: 'Please select an Interface !',
          icon: 'warning',
          // timer: 1200,
          showConfirmButton: true
        });
      } else {
        this.defaultrecords++;
        this.pageChange();
      }
  }
  public getReconciliationCompVisitList() {
    try {
      let params = { "interfaceDefinitionId": this.interfaceDefinitionId, "psId": this.psId??0, "dcsId": this.dcsId??0, "serviceId": this.serviceId??0, "payorId": this.payorId??0, "exportStatus": this.exportStatus??0, "startDate": this.datepipe.transform(this.startDate, 'MM/dd/yyyy'), "endDate": this.datepipe.transform(this.endDate, 'MM/dd/yyyy'), "lowerBound": this.lowerBound, "upperBound": this.upperBound, "orderBy": this.orderBy, "order": this.orderType }
      console.log(params)
      this.dashboardservice.getReconciliationCompVisitList(JSON.stringify(params)).subscribe(res => {
        console.log(res)
        let data: any = res;
        // if(window.location.hostname=="localhost"){
        //   data={"exportDetailsArray":[{"siteCode":"76091","procedureCode":"CCPS15","scheduledEndDateTime":"04/19/2022 01:15 AM","exportStatus":"Accepted","interfaceDefinitionId":"29","dcsName":"BANKS, FANNIE","exportedOn":"04/21/2022 09:00 PM","psName":"SEGAL, JANIECE","interfaceDatasetId":"78","departureDateTime":"04/19/2022 01:15 AM","scheduledBeginDateTime":"04/18/2022 11:00 PM","arrivalDateTime":"04/18/2022 11:00 PM","id":"1042298","interfacePayer":"Community Care Service Program","batchNumber":"11039"},{"siteCode":"76091","procedureCode":"CCPS15","scheduledEndDateTime":"04/19/2022 01:15 AM","exportStatus":"Accepted","interfaceDefinitionId":"29","dcsName":"BANKS, FANNIE","exportedOn":"04/21/2022 09:00 PM","psName":"SEGAL, JANIECE","interfaceDatasetId":"78","departureDateTime":"04/19/2022 01:15 AM","scheduledBeginDateTime":"04/18/2022 11:00 PM","arrivalDateTime":"04/18/2022 11:00 PM","id":"1042299","interfacePayer":"Community Care Service Program","batchNumber":"11039"},{"siteCode":"76091","procedureCode":"CCPS15","scheduledEndDateTime":"04/20/2022 01:00 AM","exportStatus":"Accepted","interfaceDefinitionId":"29","dcsName":"BANKS, FANNIE","exportedOn":"04/21/2022 10:12 AM","psName":"SEGAL, JANIECE","interfaceDatasetId":"78","departureDateTime":"04/20/2022 01:00 AM","scheduledBeginDateTime":"04/19/2022 11:00 PM","arrivalDateTime":"04/19/2022 11:00 PM","id":"1042276","interfacePayer":"Community Care Service Program","batchNumber":"11029"},{"siteCode":"76091","procedureCode":"CCPS15","scheduledEndDateTime":"04/20/2022 01:00 AM","exportStatus":"Accepted","interfaceDefinitionId":"29","dcsName":"BANKS, FANNIE","exportedOn":"04/21/2022 10:12 AM","psName":"SEGAL, JANIECE","interfaceDatasetId":"78","departureDateTime":"04/20/2022 01:00 AM","scheduledBeginDateTime":"04/19/2022 11:00 PM","arrivalDateTime":"04/19/2022 11:00 PM","id":"1042277","interfacePayer":"Community Care Service Program","batchNumber":"11029"}],"totalCount":4}
        // }
        this.exportDetailsArray = data.exportDetailsArray;
        this.totalCount = data.totalCount;
      })
    } catch (error) {

    }
  }
  public getFilterDataByDataSet() {
    this.dashboardservice.getFilterDataByDataSet(this.interfaceDefinitionId??0, 'Completed Visits').subscribe(res => {
      console.log(res);
      this.filterDataByDataSetResponse = res;
      let data: any = res;

      if(this.includeInActivePS){
        this.psList = data.psList;
      }else{
        this.psList = data.psList.filter(x => x.active == 1);
      }

      if(this.includeInActiveDCS){
        this.dcsList = data.dcsList;
      }else{
        this.dcsList = data.dcsList.filter(x => x.active == 1);
      }



      this.serviceList = data.serviceList;
      this.payerList = data.PayerList;


    })
  }
  public getInterfaceByDataSet(): void {

    this.dashboardservice.getInterfaceByDataSet('Completed Visits').subscribe((res) => {
      let data: any = res;
      console.log(data)
      this.InterfaceDetailsList = data;
    });
  }
  public selectEvent(event, flag) {
    console.log(event)
    if (flag == 'ps') {
      this.psId = event.psId
    } else if (flag == 'dcs') {
      this.dcsId = event.dcsId;
    } else if (flag == "service") {
      this.serviceId = event.serviceId;
    }

  }
  public filterClear(flag) {
    console.log("clear")
    if (flag == 'ps') {
      this.psId = 0;
      this.ps.close()
    } else if (flag == 'dcs') {
      this.dcsId = 0;
      this.dcs.close()
    } else if (flag == "service") {
      this.serviceId = 0;
      this.service.close()

    }

  }
  public prevpage(): void {
    this.lowerBound = this.lowerBound - this.perpage;
    this.upperBound = this.upperBound - this.perpage;
    this.getReconciliationCompVisitList();

  }
  public nextPage(): void {
    this.lowerBound = this.lowerBound + this.perpage;
    this.upperBound = this.upperBound + this.perpage;
    this.getReconciliationCompVisitList();

  }
  public pageChange() {
    this.lowerBound = 1;
    this.upperBound = this.perpage;
    this.getReconciliationCompVisitList();
  }
  public filterChange() {
  //   this.psId != 0 ? this.ps.clear() : '';
  //   this.dcsId != 0 ? this.dcs.clear() : '';
  //   this.serviceId != 0 ? this.service.clear() : '';
  //   this.payorId = 0;

  this.interfaceDefinitionId>0?this.getFilterDataByDataSet():'';

  }
  public sortorder(orderBy, orderType) {
    this.orderType = orderType;
    this.currentorderby = orderBy;
    if (orderBy == 'ps') {
      this.orderBy = "evd.ps_name";
    } else
      if (orderBy == 'payor') {
        this.orderBy = "ipd.name"
      } else
        if (orderBy == 'schedulestart') {
          this.orderBy = "evd.scheduled_begin_date";
        } else if (orderBy == 'scheduleend') {
          this.orderBy = "evd.scheduled_end_date";
        } else if (orderBy == 'arrivaltime') {
          this.orderBy = "to_date(evd.arrival_date_time,'MM/DD/YYYY hh:mi AM')";

        } else if (orderBy == 'departuretime') {
          this.orderBy = "to_date(evd.departure_date_time,'MM/DD/YYYY hh:mi AM')";
        }
    this.pageChange();
  }
  public datchange() {
    // console.log(this.endDate)
    console.log("startdate", this.datepipe.transform(this.startDate, 'MM/dd/yyyy'))
    console.log("enddate", this.datepipe.transform(this.endDate, 'MM/dd/yyyy'))
    console.log(this.endDate > this.systemDate)
    if (this.endDate != null && this.startDate != null) {
      if (this.endDate < this.startDate) {
        this.endDate = new Date(this.datepipe.transform(this.startDate, 'MM/dd/yyyy'))
      }
      if (this.endDate > this.systemDate) {
        this.endDate = new Date(this.datepipe.transform(this.systemDate, 'MM/dd/yyyy'))

      }
      if (this.startDate > this.systemDate) {
        this.startDate = new Date(this.datepipe.transform(this.systemDate, 'MM/dd/yyyy'))
      }
      if (this.startDate > this.endDate) {
        this.startDate = new Date(this.datepipe.transform(this.endDate, 'MM/dd/yyyy'))
      }
      console.log("finalstartdate", this.datepipe.transform(this.startDate, 'MM/dd/yyyy'))
      console.log("finalenddate", this.datepipe.transform(this.endDate, 'MM/dd/yyyy'))
    }
  }
  public getExportedDataDetails(data, recordDetails: TemplateRef<any>) {
    console.log(data)
    try {
      this.dashboardservice.getExportedDataDetails(data.id, data.interfaceDefinitionId, data.interfaceDatasetId, 'exported_visit_data').subscribe(
        res => {
          console.log(res);
          this.visitRequestData = res;
          this.modalRef = this.modalService.show(recordDetails, { id: 1 });

        },
        err => {
          Swal.fire({
            title: 'Failed to Load',
            text: 'Something Went Wrong',
            icon: 'error',
            // timer: 1200,
            showConfirmButton: true
          });
        }
      )
    } catch (error) {

    }

  }

  public exportDetailsLogsArray = [];
  public getReconciliationCompVisitLog(item, template: TemplateRef<any>) {
    try {
      let obj = { visitDetailsId: item.visitDetailsId, id: item.id };
      this.dashboardservice.getReconciliationCompVisitLog(JSON.stringify(obj)).subscribe(res => {
        console.log(res);
        this.exportDetailsLogsArray = res.exportDetailsArray;
        this.visitsLogModalRef = this.modalService.show(template, { id: 2, class: 'mx-w-95' });

      })
    } catch (error) {

    }
  }

  public includeInActiveDCS=false;
  public filterDataByDataSetResponse: any;
  public includeInActivePS = false;
  public onIncludeInactiveChange(event, flag) {
    console.log(event);
    flag=="PS"? this.includeInActivePS = event:this.includeInActiveDCS=event;

    if (event) {
      if (flag == 'PS') {
        this.psList = this.filterDataByDataSetResponse.psList;
      }else{
        console.log('ONDCS active== all')

        this.dcsList = this.filterDataByDataSetResponse.dcsList;
      }
    } else {
      if (flag == 'PS') {
        this.psList = this.filterDataByDataSetResponse.psList.filter(x => x.active == 1);
      }else{
        console.log('ONDCS active==1')
        this.dcsList = this.filterDataByDataSetResponse.dcsList.filter(x => x.active == 1);
      }

    }


  }

}


export interface exportdetails {

  siteCode: string,
  procedureCode: string,
  scheduledEndDateTime: string,
  exportStatus: string,
  psName: string,
  dcsName: string,
  departureDateTime: string,
  scheduledBeginDateTime: string,
  arrivalDateTime: string,
  interfacePayer: string,
  responseMessage: string,
  exportedOn: string,
  batchNumber: number

}
