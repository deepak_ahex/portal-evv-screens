import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ReconcilationComponent } from './reconcilation.component';

describe('ReconcilationComponent', () => {
  let component: ReconcilationComponent;
  let fixture: ComponentFixture<ReconcilationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ReconcilationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReconcilationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
