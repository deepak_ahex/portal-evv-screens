import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ExportEvvDataComponent } from './export-evv-data.component';

describe('ExportEvvDataComponent', () => {
  let component: ExportEvvDataComponent;
  let fixture: ComponentFixture<ExportEvvDataComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ExportEvvDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportEvvDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
