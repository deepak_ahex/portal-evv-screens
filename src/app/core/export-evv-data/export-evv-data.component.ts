import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service'
import Swal from 'sweetalert2';


@Component({
  selector: 'app-export-evv-data',
  templateUrl: './export-evv-data.component.html',
  styleUrls: ['./export-evv-data.component.scss']
})
export class ExportEvvDataComponent implements OnInit {
  public perPage = 10;
  public currentPage = 1;
  public startDate: Date = new Date();
  public endDate: Date = new Date;
  public interfaceDefinitionId: number = null;
  public getExportingVisitListResponse: any;
  public userData: any;
  public exportingVisitArray: Array<any> = [];
  public interfaceList: Array<any> = [];
  public fileFormatDetails: any;
  public headerArray = []

  constructor(private dashboardService: DashboardService, public datePipe: DatePipe) {
    this.userData = JSON.parse(sessionStorage.getItem('userData'));
    this.startDate.setDate(new Date().getDate() - 15);

  }

  ngOnInit(): void {
    this.getInterfaceList();

  }

  private getExportingVisitList() {
    try {
      let obj = { interfaceDefinitionId: this.interfaceDefinitionId != null ? this.interfaceDefinitionId : 0, startDate: this.datePipe.transform(this.startDate, 'MM/dd/yyyy'), endDate: this.datePipe.transform(this.endDate, 'MM/dd/yyyy') }
      this.dashboardService.getExportingVisitList(JSON.stringify(obj)).subscribe(res => {
        console.log(res);
        if (res["errorMsg"].length > 0) {
          Swal.fire({
            text: res["errorMsg"],
            icon: 'error'
          })
        } else {
          this.getExportingVisitListResponse = res;
          this.exportingVisitArray = this.getExportingVisitListResponse.exportingVisitArray;
          this.headerArray = this.getExportingVisitListResponse.headerArray;
          this.getFileFormatDetails();
        }
      }, err => {
        console.log("#########")

        Swal.fire({
          title: err.status,
          text: err.statusText,
          icon: 'error'
        })
      }
      )

    } catch (error) {

    }
  }

  public getFileFormatDetails() {
    try {
      this.dashboardService.getFileFormatDetails(this.getExportingVisitListResponse.interfaceDatasetId).subscribe(res => {
        console.log(res);
        this.fileFormatDetails = res;
      })

    } catch (err) {
      Swal.fire({
        title: err.status,
        text: err.statusText,
        icon: 'error'
      })
    }
  }
  public saveDownloadingVisitData(csvData) {
    let obj = {
      exportingVisitArray: this.getExportingVisitListResponse.exportingVisitArray,
      fileContent: btoa(csvData),// Base64 encode the String
      fileName: this.fileFormatDetails.fileName + '.' + this.fileFormatDetails.fileType,
      interfaceDefinitionId: this.interfaceDefinitionId != null ? this.interfaceDefinitionId : 0,
      interfaceDataSetId: this.getExportingVisitListResponse.interfaceDatasetId,
      userId: this.userData.userId
    }
    try {
      this.dashboardService.saveDownloadingVisitData(obj).subscribe(res => {
        console.log(res);
        if (res[`errorMsg`]) {
          Swal.fire({
            text: res[`errorMsg`],
            icon: 'error'
          })
        } else {
          this.download(csvData);
          Swal.fire('','Successfully downloaded', 'success');
          this.getExportingVisitList();

        }
      }, err => {
        Swal.fire({
          html: err.message,
          icon: 'error'
        });

      })
    } catch (error) {
      console.log("#### catch")
    }
  }

  private download(csvData) {
    let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
    let dwldLink = document.createElement("a");
    let url = URL.createObjectURL(blob);
    let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
    if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
      dwldLink.setAttribute("target", "_blank");
    }
    dwldLink.setAttribute("href", url);
    dwldLink.setAttribute("download", this.fileFormatDetails.fileName + '.' + this.fileFormatDetails.fileType);
    dwldLink.style.visibility = "hidden";
    document.body.appendChild(dwldLink);
    dwldLink.click();
    document.body.removeChild(dwldLink)

  }
  public getInterfaceList() {
    try {
      this.dashboardService.getInterfaceList().subscribe(res => {
        this.interfaceList = res;
      }, err => {
        // this.interfaceList = [{ "name": "Coastal Care FL", "id": 30 }]
      })
    } catch (error) {

    }
  }

  public onGo() {
    if (this.interfaceDefinitionId == null) {
      Swal.fire('Invalid Interface', 'Please select any Interface', 'warning');
    } else
      if (this.startDate > this.endDate) {
        Swal.fire('Invalid Dates', 'Start date cannot be greater than End date', 'warning');
      } else {
        this.getExportingVisitList();
      }
  }

  public downloadFile() {
    let csvData = this.convertcsv(this.headerArray);
    this.saveDownloadingVisitData(csvData);
  }

  public convertcsv(headerList) {
    var str = '';
    let row = '';

    for (let i = 0; i < headerList.length; i++) {
      // console.log(index)
      if (i == headerList.length - 1) {
        row += (this.fileFormatDetails.quoteChar + headerList[i] + this.fileFormatDetails.quoteChar)

      } else {
        row += (this.fileFormatDetails.quoteChar + headerList[i] + this.fileFormatDetails.quoteChar) + this.fileFormatDetails.delimiter;
      }
    }
    str += row + '\r\n';
    for (let item = 0; item < this.exportingVisitArray.length; item++) {
      let line = '';
      for (let i = 0; i < headerList.length; i++) {
        if (line != '') line += this.fileFormatDetails.delimiter;

        if (this.exportingVisitArray[item][`field${i + 1}`] != undefined) {
          line += (this.fileFormatDetails.quoteChar + this.exportingVisitArray[item][`field${i + 1}`] + this.fileFormatDetails.quoteChar);
        }
        else {
          line += (this.fileFormatDetails.quoteChar + "" + this.fileFormatDetails.quoteChar);

        }
        // console.log(item,`field${i + 1}`,this.exportingVisitArray[item][`field${i + 1}`])

      }
      str += line + '\r\n';
    }
    return str;


  }
  public pageChange(e) {
    console.log(e)
    this.currentPage = e;
  }


}
