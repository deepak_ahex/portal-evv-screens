import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DcoumentComponent } from './dcoument.component';

describe('DcoumentComponent', () => {
  let component: DcoumentComponent;
  let fixture: ComponentFixture<DcoumentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DcoumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DcoumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
