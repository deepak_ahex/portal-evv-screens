import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { DashboardService } from 'src/app/services/dashboard.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-dcoument',
  templateUrl: './dcoument.component.html',
  styleUrls: ['./dcoument.component.scss']
})
export class DcoumentComponent implements OnInit {
  public interfaceName = '';
  public interfaceId: number;
  public fileSize: number;
  public userData: any;

  public documentForm: UntypedFormGroup;
  public documentList = [];
  public formError: boolean = false;
  public fileData:string;

  constructor(private _fb: UntypedFormBuilder, private dashbordService: DashboardService) {
    this.getInterFaceDetails();
  }
  ngOnInit(): void {
    this.createForm();
  }
  public createForm() {
    this.formError = false;
    this.fileData=null;
    this.fileName='';
    this.documentForm = this._fb.group({
      documentName: new UntypedFormControl(null, Validators.required),
      description: new UntypedFormControl(null, Validators.required),
      active: new UntypedFormControl(1),
      id: new UntypedFormControl(0),
      interfaceAttachmentId: new UntypedFormControl(0),
      fileContent: new UntypedFormControl(null, [Validators.required]),
      sort: new UntypedFormControl(null, [Validators.required])

    })
  }
  get formControl() {
    return this.documentForm.controls;
  }
  public getInterFaceDetails() {
    var data = JSON.parse(sessionStorage.getItem('interfaceData'));
    this.userData = JSON.parse(sessionStorage.getItem('userData'))
    // console.log(data.name)
    this.interfaceName = data?.label;
    this.interfaceId = data?.id;
    this.getInterfaceDocumentationList();

  }
  public fileName: string = null;
  public fileUpload(event) {
    this.documentForm.get('fileContent').setValue(null)

    console.log(event)
    const file = event.target.files[0];
    this.fileName = event.target.files[0] != undefined ? file.name : null;
    this.fileSize = event.target.files[0] != undefined ? file.size : null;
    this.documentForm.get('documentName').setValue(this.fileName)


    if (event.target.files.length > 0) {

      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        console.log(reader.result);
        let data: any = reader.result;
        this.documentForm.get('fileContent').setValue(data.split('base64,')[1])
        // console.log(this.documentForm.value.fileContent)

      };
    }

  }
  public updateForm(obj) {
    this.documentForm.get('documentName').setValue(obj.fileName)
    this.documentForm.get('description').setValue(obj?.description)
    this.documentForm.get('id').setValue(obj.id)
    this.documentForm.get('active').setValue(obj.active)
    this.documentForm.get('interfaceAttachmentId').setValue(obj.interfaceAttachmentId)
    this.documentForm.get('sort').setValue(obj?.sortOrder)
    // this.fileName=obj.fileName;
  }
  public getInterfaceDocumentationList() {
    try {
      this.dashbordService.getInterfaceDocumentationList(this.interfaceId).subscribe(res => {
        console.log(res)
        this.documentList = res;
      })
    } catch (error) {

    }
  }
  public getInterfaceDocument(id) {
    try {
      this.dashbordService.getInterfaceDocument(id).subscribe(res => {
        console.log(res);
        const data: Blob = new Blob([res[0].fileData], { type: 'string' });
      this.fileData=res[0].fileData;
      this.fileName=res[0].fileName;
      // console.log(data)
      setTimeout(()=>{
        document.getElementById('filedownload').click();

      },100)

    })
  } catch(error) {

  }
}
  public onActiveChange(event) {
  this.documentForm.get('active').setValue(event.target.value ? 1 : 0)
}
  public saveInterfaceDocumentation() {
  this.formError = true;
  let obj = {
    "interfaceDefinitionId": this.interfaceId,
    "id": this.documentForm.value.id,
    "interfaceAttachmentId": this.documentForm.value.interfaceAttachmentId,
    "fileName": this.fileName,
    "fileContent": this.documentForm.value.fileContent,
    "sortOrder": this.documentForm.value.sort,
    "active": this.documentForm.value.active,
    "userId": this.userData.userId,
    "description":this.documentForm.value.description
  }
  if (this.documentForm.valid) {
    try {
      this.dashbordService.saveInterfaceDocumentation(obj).subscribe(data => {
        console.log(data);
        if (data.saveFlag == 1) {
          Swal.fire({
            title: 'success',
            text: data.saveMsg,
            icon: 'success',
            timer: 1200,
            showConfirmButton: false
          });
          this.getInterfaceDocumentationList();
          this.createForm();
        } else {
          Swal.fire({
            title: 'Failed',
            text: data.saveMsg,
            icon: 'error',
            timer: 1200,
            showConfirmButton: false
          });
        }
      })
    } catch (error) {

    }
  }
}
}
