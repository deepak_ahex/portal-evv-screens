import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-interface-poc-services-map',
  templateUrl: './interface-poc-services-map.component.html',
  styleUrls: ['./interface-poc-services-map.component.scss']
})
export class InterfacePocServicesMapComponent implements OnInit {

  public interfaceName:string='';
  public serviceMappingList=[
    {'office':723,pocService:30078,active:1},
    {'office':725,pocService:30078,active:1},
    {'office':186,pocService:3061,active:0},
    {'office':188,pocService:4897,active:0},
    {'office':124,pocService:4897,active:0},
  ]
  constructor() {
    var data=JSON.parse(sessionStorage.getItem('interfaceData'));
    // console.log(data.name)
    this.interfaceName=data.name;

   }

  ngOnInit(): void {
  }
  public getInterFaceDetails() {
    var data = JSON.parse(sessionStorage.getItem('interfaceData'));
    console.log(data.name)
    this.interfaceName = data?.name;
  }

}
