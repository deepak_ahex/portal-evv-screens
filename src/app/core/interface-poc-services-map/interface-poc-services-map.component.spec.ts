import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InterfacePocServicesMapComponent } from './interface-poc-services-map.component';

describe('InterfacePocServicesMapComponent', () => {
  let component: InterfacePocServicesMapComponent;
  let fixture: ComponentFixture<InterfacePocServicesMapComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InterfacePocServicesMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterfacePocServicesMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
