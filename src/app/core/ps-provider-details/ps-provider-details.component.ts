import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { DashboardService } from 'src/app/services/dashboard.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-ps-provider-details',
  templateUrl: './ps-provider-details.component.html',
  styleUrls: ['./ps-provider-details.component.scss']
})
export class PsProviderDetailsComponent implements OnInit {

  public interfaceName = '';
  public formError: boolean = false;
  public interfaceId: number;
  public psProviderForm: UntypedFormGroup;
  public officeLookup = [];
  public providerList = [];
  public userData: any;
  public psList = [];
  public providerIdList = [];


  constructor(private _fb: UntypedFormBuilder, private _dashBoard: DashboardService) {
    this.getInterFaceDetails();
  }
  ngOnInit(): void {
  }
  public createForm() {
    this.formError = false;
    this.psProviderForm = this._fb.group({
      officeId: new UntypedFormControl(null, Validators.required),
      providerId: new UntypedFormControl(null,[Validators.required]),
      psId: new UntypedFormControl(null, [Validators.required]),
      id: new UntypedFormControl(0)
    })
  }
  get formcontrols() {
    return this.psProviderForm.controls;
  }
  public updateProvideForm(obj) {
    this.psProviderForm.get('officeId').setValue(obj.officeId);
    this.psProviderForm.get('providerId').setValue(obj.providerId);
    this.psProviderForm.get('psId').setValue((obj.psId));
    this.psProviderForm.get('id').setValue(obj.id);
    this.onOfficeChange(false);
  }
  public getInterFaceDetails() {
    var data = JSON.parse(sessionStorage.getItem('interfaceData'));
    this.userData = JSON.parse(sessionStorage.getItem('userData'));

    // console.log(data.name)
    this.interfaceName = data?.label;
    this.interfaceId = data?.id;

    this.createForm();
    this.getInterfaceOffices();
    this.getPSProviderDetailsList();


  }

  public savePSProviderDetails() {
    this.formError = true;
    if (this.psProviderForm.valid) {
      let obj = {
        "interfaceDefinitionId": this.interfaceId,
        "id": this.psProviderForm.value.id,
        "officeId": this.psProviderForm.value.officeId,
        "providerId": this.psProviderForm.value.providerId,
        "psId": this.psProviderForm.value.psId,
        "userId": this.userData.userId
      }



      try {
        this._dashBoard.savePSProviderDetails(obj).subscribe(data => {
          console.log(data);
          if (data.saveFlag == 1) {
            Swal.fire({
              title: 'success',
              text: data.saveMsg,
              icon: 'success',
              timer: 1200,
              showConfirmButton: false
            });
            this.getPSProviderDetailsList();
            this.createForm();
          } else {
            Swal.fire({
              title: 'Failed',
              text: data.saveMsg,
              icon: 'error',
              timer: 1200,
              showConfirmButton: false
            });
          }

        })
      } catch (error) {

      }
    }

  }

  public getPSProviderDetailsList() {
    try {
      this._dashBoard.getPSProviderDetailsList(this.interfaceId).subscribe(res => {
        this.providerList = res;
        console.log(res)
      })
    } catch (error) {

    }
  }
  public getInterfaceOffices() {
    try {
      this._dashBoard.getInterfaceOffices(this.interfaceId).subscribe(
        res => {
          console.log(res);
          let data: any = res;
          this.officeLookup = data;
        }
      )
    } catch (error) {

    }
  }

  public onOfficeChange(flag:boolean){
    if(this.psProviderForm.value.officeId==null){
      this.psProviderForm.get('providerId').setValue(null);
      this.psProviderForm.get('psId').setValue(null);
    }else{
      if(flag){
        this.psProviderForm.get('providerId').setValue(null);
        this.psProviderForm.get('psId').setValue(null);
      }
      this.getPSList();
      this.getOfficeProviderDetails();
    }

  }
  public getPSList() {
    try {
      this._dashBoard.getPSList(this.psProviderForm.value.officeId).subscribe(res => {
        console.log(res);
        this.psList=res

      })
    } catch (error) {

    }
  }
  public getOfficeProviderDetails  (){
    try {
      this._dashBoard.getOfficeProviderDetails(this.interfaceId,this.psProviderForm.value.officeId).subscribe(res => {
        console.log(res);
        this.providerIdList=res;

      })
    } catch (error) {

    }
  }
}
