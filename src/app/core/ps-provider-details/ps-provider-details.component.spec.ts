import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PsProviderDetailsComponent } from './ps-provider-details.component';

describe('PsProviderDetailsComponent', () => {
  let component: PsProviderDetailsComponent;
  let fixture: ComponentFixture<PsProviderDetailsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PsProviderDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PsProviderDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
