import { Component, OnInit } from '@angular/core';
import Sunburst from 'sunburst-chart';
import * as  d3 from 'd3';
import { ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';

declare let $: any;
// import { color } from 'd3';
const color = d3.scaleOrdinal(d3.schemePaired);

@Component({
  selector: 'app-dashboard-chart',
  templateUrl: './dashboard-chart.component.html',
  styleUrls: ['./dashboard-chart.component.scss']
})
export class DashboardChartComponent implements OnInit {
  public activityEndDate: Date = new Date();
  public activityStartDate: Date = new Date();
  public sunburstChart = Sunburst();
  public sunburstChart2 = Sunburst();
  public pieChartId="1"
  data = [
    {
      "color":'#1f78b4',
      "name": 'CareBridge',
      "children": [
        {
          "color":'#1f78b4',
          "name": "CompletedVisits",
          "value": 120
        }
      ]
    },
    {
      "color":'#b2df8a',
      "name": "HHA NJ",
      "children": [
        {
          "color":'#b2df8a',
          "name": "CompletedVisits",
          "value": 100,
        },
        {
          "color":'#b2df8a',
          "name": "DCS",
          "value": 10,
        },
      ]
    },
    {
      "color":'#fb9a99',
      "name": "Sandata AZ",
      "children": [
        {
          "color":'#fb9a99',
          "name": "PS",
          "value": 10,
        },
        {
          "color":'#fb9a99',
          "name": "DCS",
          "value": 20,
        },
        {
          "color":'#fb9a99',
          "name": "CompletedVisits",
          "value": 200,
        },
      ]
    },
    {
      "color":'#d84143',
      "name": "Sandata NC",
      "children": [
        {
          "color":'#d84143',
          "name": "PS",
          "value": 15,
        },
        {
          "color":'#d84143',
          "name": "DCS",
          "value": 30,
        },
        {
          "color":'#d84143',
          "name": "CompletedVisits",
          "value": 300,
        },
      ]
    },


  ]
  constructor() { }
  ngOnInit(): void {

    this.drawChart();
   }
  public drawChart(): void {
    // d3.select('#canvas').selectAll('svg').remove();
    var mydata: any = {
      "name": "DashBoard",
      // "count": "TotalVisitsCoun" + this.totalcount.toString(),
      "color": 'lightblue',
      "children": this.data
    }
    this.sunburstChart.data(mydata)
      .size((node: any) => {
        return node.value;
      })
      // .color((parent) => color(parent ? parent.name : parent.name)).children('children')
      .color('color') .tooltipContent((d, node) => `Count: <i>${node.value}</i>`)
      .label(
        (d: any) => {
          if (d.name != undefined) {
            return d.name;

          } else {
            return d.interfaceName
          }
        })
      .height(500)
      .width(500).onClick((node)=>{console.log(node)
       node?.children? this.sunburstChart.focusOnNode(node):''
      })
      .minSliceAngle(0).excludeRoot(true)


      (document.getElementById('canvas'))


    console.log(mydata)

  }




  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'left',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };

  public pieChartLabels: Label[] = [['Accepted'],['Rejected']];
  public pieChartData:number[] = [110, 10 ];
  public pieChartType:string = 'pie';
  public pieChartPlugins = [pluginDataLabels];

  public pieChartColors = [
    {
      backgroundColor: ['#8bc2ef','#f997b1'],
    }
  ];

  public chartClicked(e:any):void {
    console.log(e);
  }

  public chartHovered(e:any):void {
    console.log(e);
  }



}
