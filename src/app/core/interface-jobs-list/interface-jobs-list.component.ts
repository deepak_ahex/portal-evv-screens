import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DashboardService } from 'src/app/services/dashboard.service';
import Swal from 'sweetalert2';
declare var $: any;
@Component({
  selector: 'app-interface-jobs-list',
  templateUrl: './interface-jobs-list.component.html',
  styleUrls: ['./interface-jobs-list.component.scss']
})
export class InterfaceJobsListComponent implements OnInit {

  public interfaceJobRunTimes: Array<any> = [];
  public interfaceName: string = '';
  public interfaceJobLogList = [];

  public interfaceFilter = [];
  public datasetFilter = [];
  public jobNameFilter = [];
  public interfaceSelectedFilter = null;
  public datasetSelectedFilter = null;
  public jobNameSelectedFilter = null;
  public activeFilter = null;
  private localJoblist = [];
  public checkedList = [];
  public checkedAllFlag: boolean = false;
  public userDetails;


  constructor(public dashboardService: DashboardService, public _fb: UntypedFormBuilder) {
    this.userDetails = JSON.parse(sessionStorage.getItem('userData'))
  }

  ngOnInit(): void {
    this.getInterfaceJobRunTimesList();


  }





  public getInterfaceJobRunTimesList() {
    this.checkedList = [];
    this.checkedAllFlag = false;
    try {
      this.dashboardService.getInterfaceJobRunTimesList(0).subscribe(res => {
        this.interfaceJobRunTimes = res;
        this.localJoblist = res;
        this.datasetFilter = [...new Set(this.interfaceJobRunTimes.map((x) => x.interfaceDataset))];
        this.interfaceFilter = [...new Set(this.interfaceJobRunTimes.map((x) => x.interfaceDefinition))];
        this.jobNameFilter = [...new Set(this.interfaceJobRunTimes.map((x) => x.jobName))];


      })
    } catch (error) {

    }
  }
  public datasetName = '';
  public getInterfaceJobLog(data, template) {
    this.interfaceJobLogList = [];
    this.datasetName = data.interfaceDataset;
    this.interfaceName=data.interfaceDefinition;
    try {
      this.dashboardService.getInterfaceJobLog(data.interfaceDefinitionId, data.interfaceDatasetId).subscribe(
        res => {
          this.interfaceJobLogList = res;


        }
      )
    } catch (error) {

    }
  }

  public ongo() {
    console.log(this.interfaceSelectedFilter, this.datasetSelectedFilter, this.jobNameSelectedFilter, this.activeFilter);
    console.log(this.localJoblist)
    this.checkedList = [];
    this.checkedAllFlag = false;
    this.interfaceJobRunTimes = this.localJoblist;
    console.log(this.interfaceJobRunTimes)
    if (this.interfaceSelectedFilter != null) {
      this.interfaceJobRunTimes = this.interfaceJobRunTimes.filter(x => {
        if (x.interfaceDefinition == this.interfaceSelectedFilter) {
          return x
        }
      })

    }
    if (this.datasetSelectedFilter != null) {
      this.interfaceJobRunTimes = this.interfaceJobRunTimes.filter(x => {
        if (x.interfaceDataset == this.datasetSelectedFilter) {
          return x
        }
      })

    }
    if (this.jobNameSelectedFilter != null) {
      this.interfaceJobRunTimes = this.interfaceJobRunTimes.filter(x => {
        if (x.jobName == this.jobNameSelectedFilter) {
          return x
        }
      })

    }
    if (this.activeFilter != null && this.activeFilter!=2) {
      console.log(typeof (this.activeFilter), this.activeFilter)
      this.interfaceJobRunTimes = this.interfaceJobRunTimes.filter(x => {
        if (x.active == this.activeFilter) {
          return x
        }
      })
    }
  }
  public checkedAll(event) {
    this.checkedList = []
    if (event.target.checked) {
      this.checkedAllFlag = true;
      this.checkedList = this.interfaceJobRunTimes.map(x => x.id);

    } else {
      this.checkedAllFlag = false;
      this.checkedList = [];
    }

  }
  public onvisitCheck(event, id) {
    if (event.target.checked) {
      this.checkedList.push(id);
      console.log(this.checkedList)

    } else {
      this.checkedList = this.checkedList.filter(x => x != id);
      console.log(this.checkedList)
    }

  }


  public updateInterfacejobRunTimes(flag) {

    Swal.fire({
      title: 'Confirm, Do you want to save the changes?',
      showDenyButton: true,
      confirmButtonText: `Save`,
      denyButtonText: `Cancel`,
      denyButtonColor: '#dc3545',
      confirmButtonColor: '#76bd43'

    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {

        try {
          let obj = {
            "ids": this.checkedList.toString(),
            "active": flag,
            "userId": this.userDetails.userId
          }
          console.log(obj);
          this.dashboardService.updateInterfacejobRunTimes(obj).subscribe(res => {
            console.log(res);
            if (res.saveFlag == 1) {
              Swal.fire(res.saveMsg, '', 'success')
              this.getInterfaceJobRunTimesList();
            }
            else {
              Swal.fire('Data save failed', '', 'error')
            }

          })

        } catch (error) {

        }
      } else if (result.isDenied) {
      }
    })




  }

  public reset(){
    this.checkedList = [];
    this.checkedAllFlag = false;
    this.interfaceSelectedFilter = null;
    this.datasetSelectedFilter = null;
    this.jobNameSelectedFilter = null;
    this.activeFilter = null;
    this.ongo();

  }
}
