import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InterfaceJobsListComponent } from './interface-jobs-list.component';

describe('InterfaceJobsListComponent', () => {
  let component: InterfaceJobsListComponent;
  let fixture: ComponentFixture<InterfaceJobsListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InterfaceJobsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterfaceJobsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
