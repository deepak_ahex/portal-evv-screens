import { DatePipe } from '@angular/common';
import { ViewChild } from '@angular/core';
import { TemplateRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DashboardService } from 'src/app/services/dashboard.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-reconciliation-ps',
  templateUrl: './reconciliation-ps.component copy.html',
  styleUrls: ['./reconciliation-ps.component.scss']
})
export class ReconciliationPsComponent implements OnInit {
  @ViewChild('ps') ps;

  public defaultMessage=0;
  public modalRef: BsModalRef;
  public interfaceList = [];
  public exportDetailsArray=[];
  public totalRecordCount=0;
  public interFaceId = null;
  public psId = null;
  public psList = [];
  public startDate: Date = new Date();
  public endDate: Date = new Date();
  public exportStatusId = null;
  public lowerBound=0;
  public perPage=10;
  public upperBound=10;

  private appliedInterfaceId = 0;
  private appliedPsId = 0;
  private appliedStartDate = "";
  private appliedEndDate = "";
  private appliedExportStatusId = 0;

  public visitRequestData = [];
  public modelRef:BsModalRef;
  public statusList=[{name:'Exported',id:1},{name:'Accepted',id:2},{name:'Rejected',id:3}]


  constructor(public dashBoardService: DashboardService,public datePipe:DatePipe,public modelService:BsModalService) {
    this.startDate.setDate(new Date().getDate()-7)
  }

  ngOnInit(): void {
    this.getInterfaceByDataSet();
  }

  public getInterfaceByDataSet() {
    try {
      this.dashBoardService.getInterfaceByDataSet('PS').subscribe(res => {
        console.log(res);
        this.interfaceList = res;
      })

    } catch (error) {

    }
  }
  public filterChange() {
    // this.psId != 0 ? this.ps.clear() : '';
    if (this.interFaceId != 0) {
      this.getFilterDataByDataSet();
    }
    this.psId = null;



  }
  public getFilterDataByDataSet() {
    this.dashBoardService.getFilterDataByDataSet(this.interFaceId, 'PS').subscribe(res => {
      console.log(res);
      this.filterDataByDataSetResponse = res;
      let data: any = res;

      if(this.includeInActivePS){
        this.psList = data.psList;
      }else{
        this.psList = data.psList.filter(x => x.active == 1);
      }



    })
  }

  public getReconciliationPSList(){
    try {
      let jsonObj={"interfaceDefinitionId":this.appliedInterfaceId,"psId":this.appliedPsId,"startDate":this.appliedStartDate,"endDate":this.appliedEndDate,"exportStatus":this.appliedExportStatusId,"lowerBound":this.lowerBound,"upperBound":this.upperBound,"orderBy":"","order":""}
      this.dashBoardService.getReconciliationPSList(JSON.stringify(jsonObj)).subscribe(res=>{
        console.log(res);
        this.exportDetailsArray=res.exportDetailsArray;
        this.totalRecordCount=res.totalCount;
      })
    } catch (error) {

    }
  }

  public onGo(){
    if(this.interFaceId==null){
      Swal.fire({
        title: 'Invalid Search',
        text: 'Please select an Interface !',
        icon: 'warning',
        // timer: 1200,
        showConfirmButton: true
      });
    }else if(this.startDate!=null && this.endDate!=null){
      let startDate=Date.parse(this.datePipe.transform(this.startDate,'MM/dd/yyyy'));
      let endDate= Date.parse(this.datePipe.transform(this.endDate,'MM/dd/yyyy'));
      if(startDate>endDate){
        Swal.fire('','Start date cannot be greater than End date','warning')
      }else{
        this.appliedStartDate=this.datePipe.transform(this.startDate,'MM/dd/yyyy')
        this.appliedEndDate=this.datePipe.transform(this.endDate,'MM/dd/yyyy');
        this.appliedPsId=this.psId==null?0:this.psId;
        this.appliedExportStatusId=this.exportStatusId??0;
        this.appliedInterfaceId=this.interFaceId;
        this.defaultMessage++;
        this.onPageReset();

      }
    }else{
      if (this.endDate == null || this.startDate == null) {
        Swal.fire({
          title: 'Invalid Dates',
          text: 'Start Date and End Date are Mandatory fields',
          icon: 'warning',
          // timer: 1200,
          showConfirmButton: true
        });
      }
    }
  }

  public onPageReset(){
    this.lowerBound=1;
    this.upperBound=this.perPage;
    this.getReconciliationPSList()
  }
  public prevpage(): void {
    this.lowerBound = this.lowerBound - this.perPage;
    this.upperBound = this.upperBound - this.perPage;
    this.getReconciliationPSList();

  }
  public nextPage(): void {
    this.lowerBound = this.lowerBound + this.perPage;
    this.upperBound = this.upperBound + this.perPage;
    this.getReconciliationPSList();

  }
  public  getExportedDataDetails(data, recordDetails: TemplateRef<any>) {
    console.log(data)
    try {
      this.dashBoardService.getExportedDataDetails(data.id, data.interfaceDefinitionId, data.interfaceDatasetId,'exported_ps_data').subscribe(
        res => {
          console.log(res);
          this.visitRequestData = res;
          this.modelRef = this.modelService.show(recordDetails);

        },
        err=>{
          Swal.fire({
            title: 'Failed to Load',
            text: 'Something Went Wrong',
            icon: 'error',
            // timer: 1200,
            showConfirmButton: true
          });        }
      )
    } catch (error) {

    }

  }

  public filterDataByDataSetResponse: any;
  public includeInActivePS = false;
  public onIncludeInactiveChange(event) {
    this.includeInActivePS = event;
    console.log(event);
    if (event) {
        this.psList = this.filterDataByDataSetResponse.psList;

    } else {
      this.psList = this.filterDataByDataSetResponse.psList.filter(x => x.active == 1);
    }


  }
}

