import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ReconciliationPsComponent } from './reconciliation-ps.component';

describe('ReconciliationPsComponent', () => {
  let component: ReconciliationPsComponent;
  let fixture: ComponentFixture<ReconciliationPsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ReconciliationPsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReconciliationPsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
