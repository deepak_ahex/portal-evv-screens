import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DashboardService } from '../../services/dashboard.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { DatePipe } from '@angular/common';
import { base64StringToBlob } from 'blob-util';
import { HtmlParser } from '@angular/compiler';

import * as XLSX from 'xlsx';
type AOA = any[][];




@Component({
  selector: 'app-activity-log',
  templateUrl: './activity-log.component.html',
  styleUrls: ['./activity-log.component.scss']
})
export class ActivityLogComponent implements OnInit {

  public activityEndDate: Date = new Date();
  public activityStartDate: Date = new Date();
  public dataSet: string = '';
  public activityLogList: Array<any> = [];
  public totalCount: number = 0;
  public perPage: number = 10;
  public interfaceDefinitionId: number;
  public lowerBound: number = 1;
  public upperBound: number = this.perPage;
  public interfaceDatasetId: number = 0;
  public modalRef: BsModalRef;
  public BatchInfoperPage=100;

  public datsetFilter: Array<any> = [];

  public fileName: string;
  public fileData: string;
  public fileSize: string;
  public data;
  public pdfData;
  public headersArray: Array<any> = [];
  public exportDetailsArray: Array<any> = [];



  constructor(private dashboardservice: DashboardService, private modalService: BsModalService, private activatedRoute: ActivatedRoute,
    public datepipe: DatePipe,
  ) {
    this.interfaceDefinitionId = this.activatedRoute.snapshot.params.interfaceDefinitionId;
    this.interfaceDatasetId = (this.activatedRoute.snapshot.params.interfaceDatasetId);
    this.interfaceDatasetId != undefined ? this.interfaceDatasetId = +(this.interfaceDatasetId) : this.interfaceDatasetId = 0;
    this.dashboardservice.headerInterfaceDatsetName = (this.activatedRoute.snapshot.params.datasetName);

    console.log(this.interfaceDefinitionId, this.interfaceDatasetId)
    this.activityStartDate.setDate(this.activityEndDate.getDate() - 7);

  }

  ngOnInit(): void {
    this.getActivityLogList();
    this.getInterfaceDatasetList();

  }

  public getActivityLogList() {
    let params = { "interfaceDefinitionId": this.interfaceDefinitionId, "interfaceDatasetId": this.interfaceDatasetId == undefined ? '' : this.interfaceDatasetId, "startDate": this.datepipe.transform(this.activityStartDate, 'MM/dd/yyyy'), "endDate": this.datepipe.transform(this.activityEndDate, 'MM/dd/yyyy'), "lowerBound": this.lowerBound, "upperBound": this.upperBound }

    try {
      this.dashboardservice.getActivityLogList(JSON.stringify(params)).subscribe(
        response => {
          let data: any = response;
          console.log(data);
          this.activityLogList = data.activityLogList;
          this.totalCount = data.totalCount;

        }
      )
    } catch (error) {
      console.log(error)

    }
  }
  public getInterfaceDatasetList() {

    try {
      this.dashboardservice.getInterfaceDatasetList(this.interfaceDefinitionId).subscribe(
        response => {
          let data: any = response;
          console.log(data);
          this.datsetFilter = data;


        }
      )
    } catch (error) {
      console.log(error)

    }
  }
  public pageNext() {
    this.lowerBound = this.lowerBound + this.perPage;
    this.upperBound = this.upperBound + this.perPage;
    this.getActivityLogList();
  }
  public pagePrev() {
    this.lowerBound = this.lowerBound - this.perPage;
    this.upperBound = this.upperBound - this.perPage;

    this.getActivityLogList();

  }
  public perPageChange() {
    this.lowerBound = 1;
    this.upperBound = this.perPage;
    this.getActivityLogList();
  }
  public openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {
      class: 'documentpopup'
    });


  }



  public getAttachmentContent(templatecsv: TemplateRef<any>, templatepdf: TemplateRef<any>, id, interfaceBatchInfoId) {




    try {
      this.dashboardservice.getBatchDetails(id, interfaceBatchInfoId).subscribe(res => {
        console.log(res);
        let data: any = res;
        this.exportDetailsArray = [];
        let localExportDetailsArray = [];
        this.headersArray = data.headersArray;
        localExportDetailsArray = data.exportDetailsArray;
        this.fileData = data.interfaceAttachment.fileData;
        this.fileName = data.interfaceAttachment.fileName;
        this.fileSize = data.interfaceAttachment.fileSize;
        console.log()
        localExportDetailsArray.map(
          x => {
            let data = x;
            let splited = data.split('|', this.headersArray.length);
            this.exportDetailsArray.push(splited)
          }
        )
        // if (Object.keys(data.interfaceAttachment).length > 0) {
        //   let doctype = this.fileName.split(/\.(?=[^\.]+$)/)[1]
        //   console.log(doctype)

        //   if (doctype === 'csv' || doctype === 'xlsx') {

        //     console.log(this.exportDetailsArray)
        //     // this.openModal(templatecsv)
        //     // const wb: XLSX.WorkBook = XLSX.read(this.fileData, { type: 'base64' });
        //     // const wsname: string = wb.SheetNames[0];
        //     // const ws: XLSX.WorkSheet = wb.Sheets[wsname];
        //     // this.data = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
        //     // console.log(this.data.length)


        //   } else if (doctype == 'txt') {
        //     this.pdfData = atob(this.fileData)
        //     // this.openModal(templatecsv)
        //   }

        // }
        this.openModal(templatecsv)


      }

      )
    } catch (error) {

    }

  }

  public fileUpload(event) {
    console.log(event.target.files.length)
    const file = event.target.files[0];
    this.fileName = event.target.files[0] != undefined ? file.name : null;
    if (event.target.files.length > 0) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        console.log(reader.result);
        let data: any = reader.result
        this.pdfData = (atob(data.split('base64,')[1]))
      };
    }

  }















}
