import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ResubmitScheduledVisitsComponent } from './resubmit-scheduled-visits.component';

describe('ResubmitScheduledVisitsComponent', () => {
  let component: ResubmitScheduledVisitsComponent;
  let fixture: ComponentFixture<ResubmitScheduledVisitsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ResubmitScheduledVisitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResubmitScheduledVisitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
