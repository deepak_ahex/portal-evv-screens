import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InterfaceServicesComponent } from './interface-services.component';

describe('InterfaceServicesComponent', () => {
  let component: InterfaceServicesComponent;
  let fixture: ComponentFixture<InterfaceServicesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InterfaceServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterfaceServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
