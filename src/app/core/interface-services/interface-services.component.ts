import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { DashboardService } from 'src/app/services/dashboard.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-interface-services',
  templateUrl: './interface-services.component.html',
  styleUrls: ['./interface-services.component.scss']
})
export class InterfaceServicesComponent implements OnInit {
  public interfaceName: string = '';
  public userData: any;
  public interfaceId: number;
  public displaymapping: boolean = false;
  public servieFrom: UntypedFormGroup;
  public servieMappingFrom: UntypedFormGroup;
  public serviceName = '';
  public payerlist = [];

  public formError: boolean = false;
  public ServiceList = []
  public serviceMappingList = [];
  public mappedPayorPlanList = [];
  public mappaedServiceList = [];
  public officeLookup = [];
  public interfaceServicesId: number;
  public interfacePayerDetailsId: number = null;

  constructor(private _fb: UntypedFormBuilder, private _dashboardService: DashboardService) {
    this.getInterFaceDetails();

  }

  ngOnInit(): void {
    console.log('interfaceservice')
  }
  public createForm() {
    this.formError = false;
    this.servieFrom = this._fb.group({
      code: new UntypedFormControl('', [Validators.required]),
      name: new UntypedFormControl('', [Validators.required]),
      active: new UntypedFormControl(1, [Validators.required]),
      id: new UntypedFormControl(0, [Validators.required]),
      interfacePayerDetailsId: new UntypedFormControl(null, [Validators.required])

    })

  }
  get formControls() {
    return this.servieFrom.controls;
  }

  public getInterfacePayerDetailsList() {
    try {
      this._dashboardService.getInterfacePayerDetailsList(this.interfaceId).subscribe(res => {
        console.log(res)
        this.payerlist = res;
      })
    } catch (error) {

    }
  }

  ngOnDestroy() {
    console.log("ng destroy interface")
  }

  public getInterfaceServicesList() {
    try {
      this._dashboardService.getInterfaceServicesList(this.interfaceId, this.interfacePayerDetailsId).subscribe(
        res => {
          console.log(res)
          let data: any = res;
          this.ServiceList = data;
        }
      )
    } catch (error) {

    }
  }
  public getInterfaceServicesById(id) {
    this.displaymapping = false;

    try {
      this._dashboardService.getInterfaceServicesById(id).subscribe(
        res => {
          console.log(res);
          let obj: any = res;
          this.servieFrom.get('code').setValue(obj.code);
          this.servieFrom.get('name').setValue(obj.name);
          this.servieFrom.get('active').setValue(obj.active);
          this.servieFrom.get('id').setValue(obj.id);
          this.servieFrom.get('interfacePayerDetailsId').setValue(obj.interfacePayerDetailsId);

        }
      )
    } catch (error) {

    }
  }
  public saveInterfaceServices() {
    this.formError = true;
    if (this.servieFrom.valid) {
      try {
        let obj = { "interfacePayerDetailsId": this.servieFrom.value.interfacePayerDetailsId, "id": this.servieFrom.value.id, "name": this.servieFrom.value.name, "code": this.servieFrom.value.code, "active": this.servieFrom.value.active, "userId": this.userData.userId };
        this._dashboardService.saveInterfaceServices(obj).subscribe(res => {
          console.log(res);
          let data: any = res;
          if (data.saveFlag == 1) {
            Swal.fire({
              title: 'success',
              text: data.saveMsg,
              icon: 'success',
              timer: 1200,
              showConfirmButton: false
            });
            this.getInterfaceServicesList();
            this.createForm();
          } else {
            Swal.fire({
              title: 'Failed',
              text: data.saveMsg,
              icon: 'error',
              timer: 1200,
              showConfirmButton: false
            });
          }
        })
      } catch (error) {

      }
    }
  }
  public onActivechange(event, type?) {
    if (type == 'mapping') {
      this.servieMappingFrom.get('active').setValue(event.target.checked ? 1 : 0);
    } else {
      this.servieFrom.get('active').setValue(event.target.checked ? 1 : 0);
    }


  }

  public serviceMappingForm() {
    this.formError = false;
    this.servieMappingFrom = this._fb.group({
      officeId: new UntypedFormControl(null, Validators.required),
      serviceId: new UntypedFormControl(null, Validators.required),
      active: new UntypedFormControl(1),
      serviceMapId: new UntypedFormControl(0),
      payorPlanId: new UntypedFormControl(null, Validators.required),
    })
  }
  get formcontrolsmap() {
    return this.servieMappingFrom.controls
  }
  public updateServiceMappingForm(obj) {
    console.log(obj)
    this.servieMappingFrom.get('officeId').setValue(obj.officeId);
    this.servieMappingFrom.get('serviceId').setValue(obj.pocServiceId);
    this.servieMappingFrom.get('active').setValue(obj.active);
    this.servieMappingFrom.get('serviceMapId').setValue(obj.interfacePocServiceMapId);
    this.servieMappingFrom.get('payorPlanId').setValue(+(obj.payorPlanId));

    this.getMappedPayorPlanList();
    this.getServiceList(obj.officeId, obj.payorPlanId)
  }


  public getInterfacePocServiceMapList(item) {
    this.serviceMappingForm();
    this.formError = false;
    this.displaymapping = true;
    this.serviceName = item.name;
    this.interfaceServicesId = item.id;
    try {
      this._dashboardService.getInterfacePocServiceMapList(this.interfaceServicesId).subscribe(res => {
        console.log(res);
        this.serviceMappingList = res;
      })
    } catch (error) {

    }
  }


  public getMappedPayorPlanList(flag?: boolean) {
    let officeId = this.servieMappingFrom.value.officeId;

    if (officeId != 0 && officeId != null) {
      try {
        if (flag) {
          this.servieMappingFrom.get('serviceId').setValue(null);
          this.servieMappingFrom.get('payorPlanId').setValue(null);
        }
        this._dashboardService.getMappedPayorPlanList(officeId, this.interfaceId).subscribe(res => {
          console.log(res);
          this.mappedPayorPlanList = res;
        })
      } catch (error) {

      }
    } else {
      this.servieMappingFrom.get('serviceId').setValue(null);
      this.servieMappingFrom.get('payorPlanId').setValue(null);
    }
  }
  public getServiceList(officeId, payorPlanId, flag?: boolean) {
    if (payorPlanId != 0 && payorPlanId !== null) {
      try {
        if (flag) {
          this.servieMappingFrom.get('serviceId').setValue(null);
        }
        this._dashboardService.getServiceList(officeId, payorPlanId).subscribe(res => {
          console.log(res);
          this.mappaedServiceList = res;
        })
      } catch (error) {

      }
    }
    else {
      this.servieMappingFrom.get('serviceId').setValue(null);

    }
  }
  public saveInterfacePocServiceMap() {
    this.formError = true;
    if (this.servieMappingFrom.valid) {
      let obj = {
        "interfaceServicesId": this.interfaceServicesId,
        "id": this.servieMappingFrom.value.serviceMapId,
        "officeId": this.servieMappingFrom.value.officeId,
        "serviceId": this.servieMappingFrom.value.serviceId,
        "active": this.servieMappingFrom.value.active,
        "userId": this.userData.userId,
        "payorPlanId": this.servieMappingFrom.value.payorPlanId,
      }
      console.log(obj)
      try {
        this._dashboardService.saveInterfacePocServiceMap(obj).subscribe(res => {
          console.log(res);
          let data: any = res;
          if (data.saveFlag == 1) {
            this.getInterfacePocServiceMapList({ name: this.serviceName, id: this.interfaceServicesId });
            Swal.fire({
              title: 'success',
              text: data.saveMsg,
              icon: 'success',
              timer: 1200,
              showConfirmButton: false
            });
            // this.getInterfaceServicesList();
            this.createForm();
          } else {
            Swal.fire({
              title: 'Failed',
              text: data.saveMsg,
              icon: 'error',
              timer: 1200,
              showConfirmButton: false
            });
          }
        })
      } catch (error) {

      }
    }

  }
  public getInterFaceDetails() {
    var data = JSON.parse(sessionStorage.getItem('interfaceData'));
    this.userData = JSON.parse(sessionStorage.getItem('userData'));

    // console.log(data.name)
    this.interfaceName = data?.label;
    this.interfaceId = data?.id;
    this.getInterfaceServicesList();
    this.getInterfacePayerDetailsList();
    this.getInterfaceOffices();
    this.createForm();
  }
  public getInterfaceOffices() {
    try {
      this._dashboardService.getInterfaceOffices(this.interfaceId).subscribe(
        res => {
          console.log(res);
          let data: any = res;
          this.officeLookup = data;
        }
      )
    } catch (error) {

    }
  }
}
