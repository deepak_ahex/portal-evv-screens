import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InterfaceMediumComponent } from './interface-medium.component';

describe('InterfaceMediumComponent', () => {
  let component: InterfaceMediumComponent;
  let fixture: ComponentFixture<InterfaceMediumComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InterfaceMediumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterfaceMediumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
