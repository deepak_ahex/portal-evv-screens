import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { trLocale } from 'ngx-bootstrap/chronos';
import { DashboardService } from 'src/app/services/dashboard.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-interface-medium',
  templateUrl: './interface-medium.component.html',
  styleUrls: ['./interface-medium.component.scss']
})
export class InterfaceMediumComponent implements OnInit {

  public interfaceName: string = '';
  public interfaceId: number;
  public interfaceMediumForm: UntypedFormGroup;
  public interfaceMediumFormWs: UntypedFormGroup;
  public formError: boolean = false;
  public displayForm: string = "SFTP"
  public fileName: string = '';
  public userData: any;
  public interfaceMediumList = [
    // {
    //   "id": "",
    //   "interfaceDatasetId": "",
    //   "name": "",
    //   "interfaceSiteNumber": "",
    //   "interfaceTaxId": "",
    //   "sftpUrl": "",
    //   "sftpPort": "",
    //   "sftpUserName": "",
    //   "sftpPassword": "",
    //   "sftpKeyFileName": "",
    //   "sftpFromFolder": "",
    //   "sftpToFolder": "",
    //   "sftpFileName": "",
    //   "sftpFileDateFormat": "",
    //   "sftpPrivateKeyForEncrypt": "",
    //   "sftpPublicKeyForEncrypt": "",
    //   "sftpPrivateKeyForDecrypt": "",
    //   "sftpPublicKeyForDecrypt": "",
    //   "wsUrl": "",
    //   "wsPort": "",
    //   "wsUserName": "",
    //   "wsPassword": "",
    //   "wsRecordsLimit": "",
    //   "effectiveFrom": "",
    //   "clientId": "",
    //   "clientSecret": "",
    //   "scope": "",
    //   "jsonKeyName": "",
    //   "active": ""
    // }
  ];
  public interFaceDatasetList = [];
  constructor(private _fb: UntypedFormBuilder, private _dashBoardService: DashboardService) {
    this.getInterFaceDetails()
  }
  ngOnInit(): void {
  }
  public createForm() {
    this.formError = false;
    this.displayForm="SFTP";
    this.interfaceMediumForm = this._fb.group({
      id: new UntypedFormControl(0),
      interfaceDatasetId: new UntypedFormControl(null, [Validators.required]),
      sftpUserName: new UntypedFormControl('', [Validators.required]),
      sftpUrl: new UntypedFormControl('', [Validators.required]),
      sftpPort: new UntypedFormControl(null, [Validators.required]),
      sftpFromFolder: new UntypedFormControl('',),
      sftpToFolder: new UntypedFormControl('', ),
      sftpPassword: new UntypedFormControl('',),
      sftpKeyFileName: new UntypedFormControl('',),
      sftpFileName:new UntypedFormControl(''),
      sftpFileDateFormat:new UntypedFormControl(''),
      wsRecordsLimit:new UntypedFormControl(''),
      clientId: new UntypedFormControl('',),
      clientSecret: new UntypedFormControl('',),
      scope: new UntypedFormControl('',),
      jsonKeyName: new UntypedFormControl('',),
      active: new UntypedFormControl(true ),
      interfaceTaxId: new UntypedFormControl('',)
    });

    this.interfaceMediumFormWs = this._fb.group({
      id: new UntypedFormControl(0),
      interfaceDatasetId: new UntypedFormControl(null, [Validators.required]),
      wsUserName: new UntypedFormControl('',),
      wsUrl: new UntypedFormControl('', [Validators.required]),
      wsPort:new UntypedFormControl(''),
      wsPassword: new UntypedFormControl('',),
      wsRecordsLimit: new UntypedFormControl(''),
      clientId: new UntypedFormControl('',),
      clientSecret: new UntypedFormControl(''),
      scope: new UntypedFormControl(''),
      jsonKeyName: new UntypedFormControl(''),
      active: new UntypedFormControl(true),
      interfaceTaxId: new UntypedFormControl('')
    })
  }
  get formControls() {
    return this.interfaceMediumForm.controls;
  }
  get formControlsws() {
    return this.interfaceMediumFormWs.controls;
  }
  public saveMediumForm() {
    this.formError = true;
  }
  public updateForm(obj) {
    console.log(obj)
    if(obj.sftpUrl.length>0){
      this.displayForm='SFTP';
    this.interfaceMediumForm.get('interfaceDatasetId').setValue(obj.interfaceDatasetId);
    this.interfaceMediumForm.get('sftpUserName').setValue(obj.sftpUserName);
    this.interfaceMediumForm.get('sftpUrl').setValue(obj.sftpUrl);
    this.interfaceMediumForm.get('sftpPassword').setValue(obj.sftpPassword);
    this.interfaceMediumForm.get('sftpKeyFileName').setValue(obj.sftpKeyFileName);
    this.interfaceMediumForm.get('sftpFileName').setValue(obj.sftpFileName);
    this.interfaceMediumForm.get('sftpFileDateFormat').setValue(obj.sftpFileDateFormat);

    this.interfaceMediumForm.get('sftpPort').setValue(obj.sftpPort);
    this.interfaceMediumForm.get('id').setValue(obj.id);
    this.interfaceMediumForm.get('sftpFromFolder').setValue(obj.sftpFromFolder);
    this.interfaceMediumForm.get('sftpToFolder').setValue(obj.sftpToFolder);
    this.interfaceMediumForm.get('clientId').setValue(obj.clientId);
    this.interfaceMediumForm.get('clientSecret').setValue(obj.clientSecret);
    this.interfaceMediumForm.get('scope').setValue(obj.scope);
    this.interfaceMediumForm.get('jsonKeyName').setValue(obj.jsonKeyName);
    this.interfaceMediumForm.get('active').setValue(obj.active==1?true:false);
    this.interfaceMediumForm.get('interfaceTaxId').setValue(obj.interfaceTaxId);
    this.interfaceMediumForm.get('wsRecordsLimit').setValue(obj.wsRecordsLimit);



  }else{
    this.displayForm='WS';
    this.interfaceMediumFormWs.get('interfaceDatasetId').setValue(obj.interfaceDatasetId);
    this.interfaceMediumFormWs.get('wsUserName').setValue(obj.wsUserName);
    this.interfaceMediumFormWs.get('wsUrl').setValue(obj.wsUrl);
    this.interfaceMediumFormWs.get('wsPort').setValue(obj.wsPort);
    this.interfaceMediumFormWs.get('wsPassword').setValue(obj.wsPassword);
    this.interfaceMediumFormWs.get('id').setValue(obj.id);
    this.interfaceMediumFormWs.get('wsRecordsLimit').setValue(obj.wsRecordsLimit);
    this.interfaceMediumFormWs.get('clientId').setValue(obj.clientId);
    this.interfaceMediumFormWs.get('clientSecret').setValue(obj.clientSecret);
    this.interfaceMediumFormWs.get('scope').setValue(obj.scope);
    this.interfaceMediumFormWs.get('jsonKeyName').setValue(obj.jsonKeyName);
    this.interfaceMediumFormWs.get('active').setValue(obj.active==1?true:false);
    this.interfaceMediumFormWs.get('interfaceTaxId').setValue(obj.interfaceTaxId);


  }
  }
  public getInterFaceDetails() {
    var data = JSON.parse(sessionStorage.getItem('interfaceData'));
    this.userData = JSON.parse(sessionStorage.getItem('userData'));

    // console.log(data.name)
    this.interfaceName = data?.label;
    this.interfaceId = data?.id;
    this.getInterfaceDatasetList();
    this.getInterfaceMediumDetailsList();
    this.createForm();
  }
  public getInterfaceMediumDetailsList() {
    console.log("resss")
    try {
      this._dashBoardService.getInterfaceMediumDetailsList(this.interfaceId).subscribe(
        res => {
          console.log(res)
          this.interfaceMediumList = res;
        }
      )
    } catch (error) {

    }
  }

  public saveInterfaceMediumDetails() {
    this.formError = true;
    let obj = {
      "id": this.displayForm == 'SFTP' ? this.interfaceMediumForm.value.id : this.interfaceMediumFormWs.value.id,
      "interfaceDatasetId": this.displayForm == 'SFTP' ? this.interfaceMediumForm.value.interfaceDatasetId : this.interfaceMediumFormWs.value.interfaceDatasetId,
      // "interfaceSiteNumber": this.displayForm == 'SFTP' ? this.interfaceMediumForm.value.interfaceSiteNumber : this.interfaceMediumFormWs.value.interfaceSiteNumber,
      "interfaceTaxId": this.displayForm == 'SFTP' ? this.interfaceMediumForm.value.interfaceTaxId : this.interfaceMediumFormWs.value.interfaceTaxId,
      "sftpUrl": this.displayForm == 'SFTP' ? this.interfaceMediumForm.value.sftpUrl : '',
      "sftpPort": this.displayForm == 'SFTP' ? this.interfaceMediumForm.value.sftpPort : '',
      "sftpUserName": this.displayForm == 'SFTP' ? this.interfaceMediumForm.value.sftpUserName : '',
      "sftpPassword": this.displayForm == 'SFTP' ? this.interfaceMediumForm.value.sftpPassword : '',
      "sftpKeyFileName": this.displayForm == 'SFTP' ? this.interfaceMediumForm.value.sftpKeyFileName : '',
      "sftpFromFolder": this.displayForm == 'SFTP' ? this.interfaceMediumForm.value.sftpFromFolder : '',
      "sftpToFolder": this.displayForm == 'SFTP' ? this.interfaceMediumForm.value.sftpToFolder : '',
      "sftpFileName": this.displayForm == 'SFTP' ? this.interfaceMediumForm.value.sftpFileName : '',
      "sftpFileDateFormat": this.displayForm == 'SFTP' ? this.interfaceMediumForm.value.sftpFileDateFormat??'' : '',
      "wsUrl": this.displayForm == 'WS' ? this.interfaceMediumFormWs.value.wsUrl : '',
      "wsPort": this.displayForm == 'WS' ? this.interfaceMediumFormWs.value.wsPort : '',
      "wsUserName": this.displayForm == 'WS' ? this.interfaceMediumFormWs.value.wsUserName : '',
      "wsPassword": this.displayForm == 'WS' ? this.interfaceMediumFormWs.value.wsPassword : '',
      "wsRecordsLimit": this.displayForm == 'WS' ? this.interfaceMediumFormWs.value.wsRecordsLimit??'' : this.interfaceMediumForm.value.wsRecordsLimit??'',
      // "effectiveFrom": this.displayForm == 'SFTP' ? this.interfaceMediumForm.value.effictiveFrom : this.interfaceMediumFormWs.value.effictiveFrom,
      "clientId": this.displayForm == 'SFTP' ? this.interfaceMediumForm.value.clientId : this.interfaceMediumFormWs.value.clientId,
      "clientSecret": this.displayForm == 'SFTP' ? this.interfaceMediumForm.value.clientSecret : this.interfaceMediumFormWs.value.clientSecret,
      "scope": this.displayForm == 'SFTP' ? this.interfaceMediumForm.value.scope : this.interfaceMediumFormWs.value.scope,
      "jsonKeyName": this.displayForm == 'SFTP' ? this.interfaceMediumForm.value.jsonKeyName : this.interfaceMediumFormWs.value.jsonKeyName,
      "active": this.displayForm == 'SFTP' ? this.interfaceMediumForm.value.active==true?1:0 : this.interfaceMediumFormWs.value.active==true?1:0,
      "userId": this.userData.userId,
      "interfaceDefinitionId": this.interfaceId,

    }
    console.log("finalOBJ",obj)
    if (this.displayForm === 'SFTP') {
      if (this.interfaceMediumForm.valid) {
        try {
          this._dashBoardService.saveInterfaceMediumDetails(obj).subscribe(res => {
            console.log(res)
            let data: any = res;
            if (data.saveFlag == 1) {
              Swal.fire({
                title: 'success',
                text: data.saveMsg,
                icon: 'success',
                timer: 1200,
                showConfirmButton: false
              });
              this.getInterfaceMediumDetailsList();
              this.createForm();
            } else {
              Swal.fire({
                title: 'Failed',
                text: data.saveMsg,
                icon: 'error',
                timer: 1200,
                showConfirmButton: false
              });
            }
          })
        } catch (error) {
        }
      }
    } else {
      if (this.interfaceMediumFormWs.valid) {
        try {
          this._dashBoardService.saveInterfaceMediumDetails(obj).subscribe(res => {
            console.log(res)
            let data: any = res;
            if (data.saveFlag == 1) {
              Swal.fire({
                title: 'success',
                text: data.saveMsg,
                icon: 'success',
                timer: 1200,
                showConfirmButton: false
              });
              this.getInterfaceMediumDetailsList();
              this.createForm();
            } else {
              Swal.fire({
                title: 'Failed',
                text: data.saveMsg,
                icon: 'error',
                timer: 1200,
                showConfirmButton: false
              });
            }
          })
        } catch (error) {

        }
      }
    }

  }

  public fileUpload(event) {
    console.log(event)
    console.log(event.target.files.length)
    const file = event.target.files[0];
    this.fileName = event.target.files[0] != undefined ? file.name : null;
    if (event.target.files.length > 0) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        // console.log(reader.result);
      };
    }

  }
  public getInterfaceDatasetList() {
    try {
      this._dashBoardService.getInterfaceDatasetList(this.interfaceId).subscribe(res => {
        this.interFaceDatasetList = res;
      })
    } catch (error) {

    }
  }

}
