import { DatePipe } from '@angular/common';
import { TemplateRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-error-log',
  templateUrl: './error-log.component.html',
  styleUrls: ['./error-log.component.scss']
})
export class ErrorLogComponent implements OnInit {

  public activityEndDate: Date = new Date();
  public activityStartDate: Date = new Date();
  public dataSet: string = '';
  public activityErrorList: Array<any> = [];
  public totalCount: number = 0;
  public perPage: number = 10;
  public interfaceDefinitionId: number;
  public lowerBound: number = 1;
  public upperBound: number = this.perPage;
  public interfaceDatasetId:number;
  public modalRef: BsModalRef;
  public errormessage:string;
  public datsetFilter:Array<any>=[];






  constructor(private dashboardservice: DashboardService,private modalService: BsModalService,private datepipe:DatePipe, private activatedRoute: ActivatedRoute) {

    this.interfaceDefinitionId = this.activatedRoute.snapshot.params.interfaceDefinitionId;
    this.dashboardservice.headerInterfaceDatsetName=this.activatedRoute.snapshot.params.datasetName
    this.interfaceDatasetId=(this.activatedRoute.snapshot.params.interfaceDatasetId);
    console.log(this.interfaceDefinitionId,this.interfaceDatasetId)
    this.activityStartDate.setDate(this.activityEndDate.getDate() - 7);

  }

  ngOnInit(): void {
    this.getActivityErrorLogList();
    this.getInterfaceDatasetList();

  }

  public getActivityErrorLogList() {
    let params = { "interfaceDefinitionId": this.interfaceDefinitionId, "interfaceDatasetId": this.interfaceDatasetId==undefined?'':this.interfaceDatasetId, "startDate":this.datepipe.transform(this.activityStartDate,'MM/dd/yyyy'), "endDate": this.datepipe.transform(this.activityEndDate,'MM/dd/yyyy'), "lowerBound": this.lowerBound, "upperBound": this.upperBound }

    try {
      this.dashboardservice.getActivityErrorLogList(JSON.stringify(params)).subscribe(
        response => {
          let data: any = response;
          console.log(data);
          this.activityErrorList = data.activityErrorLogList;
          this.totalCount = data.totalCount;

        }
      )
    } catch (error) {
      console.log(error)

    }
  }
  public pageNext(){
    this.lowerBound=this.lowerBound+this.perPage;
    this.upperBound=this.upperBound+this.perPage;
    this.getActivityErrorLogList();
  }
  public pagePrev(){
    this.lowerBound=this.lowerBound-this.perPage;
    this.upperBound=this.upperBound-this.perPage;
    this.getActivityErrorLogList();

  }
  public perPageChange(){
    this.lowerBound=1;
    this.upperBound=this.perPage;
    this.getActivityErrorLogList();
  }

  public openModal(template: TemplateRef<any>,msg) {
    this.errormessage=msg;
    // this.errormessage="Batch ID - 57 Rejected File Response :: Error in Exception :: com.assh.pocevv.common.exceptions.POCEVVException: com.assh.pocevv.error.key:general.error --- Root Cause ---: {com.assh.pocevv.common.exceptions.POCEVVException: com.assh.pocevv.error.key:general.error --- Root Cause ---: {java.sql.SQLException: Inserted value too large for column: VCR1042,VCR1048,VCR1067,VCR2012,VCR2013,VCR2023,VCR2025,VCR2042,VCR1048,VCR1067,VCR2012,VCR2013,VCR2023,VCR1013,VCR1042,VCR1048,VCR1067,VCR2012,VCR2013,VCR2042,VCR1013,VCR1019,VCR1020,VCR1021,VCR1026,VCR1027,VCR1028,VCR1042,VCR1048,VCR1067,VCR2012,VCR2013,VCR2042,VCR1042,VCR1048,VCR1067,VCR2012,VCR2013,VCR2023,VCR1019,VCR1020,VCR1021,VCR1026,VCR1027,VCR1028,VCR1042,VCR1048,VCR1067,VCR2012,VCR2013,VCR2023,VCR2042,VCR2042,VCR1048,VCR1067,VCR2012,VCR2013,VCR2023,VCR1042,VCR1048,VCR1067,VCR2012,VCR2013,VCR2023}}.Batch ID - 59 Rejected File Response :: TEST_VISITS_NJ_61-1312327_ERROR_20201127091101.txt not exists.Batch ID - 60 Rejected File Response :: TEST_VISITS_NJ_61-1312327_ERROR_20201127091102.txt not exists."

    this.modalRef = this.modalService.show(template,{
      class:'documentpopup'
    });
  }
  public getInterfaceDatasetList() {

    try {
      this.dashboardservice.getInterfaceDatasetList(this.interfaceDefinitionId).subscribe(
        response => {
          let data: any = response;
          console.log(data);
          this.datsetFilter=data;


        }
      )
    } catch (error) {
      console.log(error)

    }
  }

}
