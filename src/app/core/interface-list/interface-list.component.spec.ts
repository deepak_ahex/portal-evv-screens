import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InterfaceListComponent } from './interface-list.component';

describe('InterfaceListComponent', () => {
  let component: InterfaceListComponent;
  let fixture: ComponentFixture<InterfaceListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InterfaceListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterfaceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
