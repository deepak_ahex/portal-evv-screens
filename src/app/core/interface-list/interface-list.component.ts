import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EvvServiceService } from 'src/app/services/evv-service.service';

@Component({
  selector: 'app-interface-list',
  templateUrl: './interface-list.component.html',
  styleUrls: ['./interface-list.component.scss']
})
export class InterfaceListComponent implements OnInit {
data: any = [];
  lookUpData: any = [];
  constructor(private evvServiceService: EvvServiceService,
    private router:Router) { }
    perPage: any = 10;

  ngOnInit(): void {
  //   this.data={"lookupList":[{"lookupId":2,"interfaceDefinitionId":17,"interfaceDefinitionName":"Sandata","lookupName":"Sandata Adjust Reasons"},
  //   {"lookupId":3,"interfaceDefinitionId":17,"interfaceDefinitionName":"Sandata","lookupName":"Sandata Bill Type"},
  //   {"lookupId":4,"interfaceDefinitionId":17,"interfaceDefinitionName":"Sandata","lookupName":"Sandata Cancel Code"},
  //   {"lookupId":5,"interfaceDefinitionId":17,"interfaceDefinitionName":"Sandata","lookupName":"Sandata Discipline"},
  //   {"lookupId":6,"interfaceDefinitionId":17,"interfaceDefinitionName":"Sandata","lookupName":"Sandata Event Codes"},
  //   {"lookupId":7,"interfaceDefinitionId":17,"interfaceDefinitionName":"Sandata","lookupName":"Sandata Services"}],
  // "totalCount":6}
  this.getLookUpLList();
  }
 p(){

 }
  getLookUpLList(){
    const payload = {interfaceDefinitionId: 0, lowerBound: 1 , upperBound: 10}
    this.evvServiceService.getInterfaceLookUp(JSON.stringify(payload)).subscribe((res) => {
      console.log(res)
      this.lookUpData = res;
    });
  }
  goToLookUpsList(lookupName){
    sessionStorage.setItem('lookupName', lookupName )
    this.router.navigate(['/lookUpList']);
  }
  }
  
