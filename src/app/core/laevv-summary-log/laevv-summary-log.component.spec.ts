import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LaevvSummaryLogComponent } from './laevv-summary-log.component';

describe('LaevvSummaryLogComponent', () => {
  let component: LaevvSummaryLogComponent;
  let fixture: ComponentFixture<LaevvSummaryLogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LaevvSummaryLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaevvSummaryLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
