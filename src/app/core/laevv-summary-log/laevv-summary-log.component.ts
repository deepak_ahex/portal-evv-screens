import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { DashboardService } from 'src/app/services/dashboard.service';
import * as FileSaver from 'file-saver'
import * as XLSX from 'xlsx';

declare var $: any;
@Component({
  selector: 'app-laevv-summary-log',
  templateUrl: './laevv-summary-log.component.html',
  styleUrls: ['./laevv-summary-log.component.scss']
})
export class LaevvSummaryLogComponent implements OnInit {

  public paToggle = false;
  public visits = false;
  public errorMsgToggle = false;
  modalRef?: BsModalRef;

  public summaryCounts;
  public clientMaster = [];
  public clientAltList = [];
  public clientNoPSList = [];
  public clientWithNoPSList = [];
  public authServiceMatchedList = [];
  public authServiceNotMatchedList = [];
  public totalAuthUnitsNotMatchedList = [];
  public weeklyUnitsNotMatchedList = [];
  public rejectedBySRIVisitList = [];
  public errorMessageList = [];
  public unMatchedAuthsList=[];


  public clientTabs = [false, false, false];
  public authTabs = [false, false, false, false,false];



  constructor(private modalService: BsModalService, public dashBoardService: DashboardService) { }
  ngOnInit(): void {
    this.rotate();
    this.getSummaryCounts();
  }

  public getSummaryCounts() {
    try {
      this.dashBoardService.getSummaryCounts().subscribe(res => {
        console.log(res);
        this.summaryCounts = res;
        this.errorMessageList = res.errorMessageList;
      })
    } catch (error) {

    }

  }



  rotate() {
    $(".rotate").click(function () {
      $(this).toggleClass("down");
    });
  }



  // below methods are client POPup Related API's call

  public getClientList(client?: TemplateRef<any>) {
    try {
      this.clientTabs = [true, false, false];

      this.dashBoardService.getClientList().subscribe(res => {
        console.log(res);
        this.clientMaster = res;
        if (client) {
          this.modalRef = this.modalService.show(client, { class: 'mx-w-95' });

        }


      })
    } catch (error) {

    }
  }

  public getClientAltList(clientAlt?: TemplateRef<any>) {
    try {
      this.dashBoardService.getClientAltList().subscribe(res => {
        console.log(res);
        this.clientTabs = [false, true, false];

        this.clientAltList = res;
        if (clientAlt) {
          this.modalRef = this.modalService.show(clientAlt, { class: 'mx-w-95' });

        }
      })
    } catch (error) {

    }
  }

  public getClientNoPSList(clientAlt?: TemplateRef<any>) {
    try {
      this.dashBoardService.getClientNoPSList().subscribe(res => {
        console.log(res);
        this.clientTabs = [false, false, true];
        this.clientNoPSList = res;
        if (clientAlt) {
          this.modalRef = this.modalService.show(clientAlt, { class: 'mx-w-95' });

        }
      })
    } catch (error) {

    }
  }


  // below methods are Auth POPup Related API's call
  public getAuthServiceMatchedList(auth?: TemplateRef<any>) {
    try {
      this.authTabs = [true, false, false, false,false];
      this.dashBoardService.getAuthServiceMatchedList().subscribe(res => {
        console.log(res);
        this.authServiceMatchedList = res;
        if (auth) {
          this.modalRef = this.modalService.show(auth, { class: 'mx-w-95' });
        }
      })
    } catch (error) {

    }
  }
  public getAuthServiceNotMatchedList() {
    try {
      this.authTabs = [false, true, false, false,false];

      this.dashBoardService.getAuthServiceNotMatchedList().subscribe(res => {
        console.log(res);
        this.authServiceNotMatchedList = res;
      })
    } catch (error) {

    }
  }
  public getTotalAuthUnitsNotMatchedList() {
    try {
      this.authTabs = [false, false, true, false,false];
      this.dashBoardService.getTotalAuthUnitsNotMatchedList().subscribe(res => {
        console.log(res);
        this.totalAuthUnitsNotMatchedList = res;
      })
    } catch (error) {

    }
  }
  public getWeeklyUnitsNotMatchedList() {
    try {
      this.authTabs = [false, false, false, true,false];

      this.dashBoardService.getWeeklyUnitsNotMatchedList().subscribe(res => {
        console.log(res);
        this.weeklyUnitsNotMatchedList = res;

      })
    } catch (error) {

    }
  }

  public getUnMatchedAuths() {
    try {
      this.authTabs = [false, false, false, false,true];
      this.dashBoardService.getUnMatchedAuths().subscribe(res => {
        console.log(res);
        this.unMatchedAuthsList = res;
      })
    } catch (error) {

    }
  }

  public lowerBound = 1;
  public upperBound = 10;
  public perPage = 10;
  public totalCount = 0;

  public pageNext() {
    this.lowerBound = this.lowerBound + this.perPage;
    this.upperBound = this.upperBound + this.perPage;
    this.getRejectedBySRIVisitList();
  }
  public pagePrev() {
    this.lowerBound = this.lowerBound - this.perPage;
    this.upperBound = this.upperBound - this.perPage;
    this.getRejectedBySRIVisitList();

  }
  public perPageChange() {
    this.lowerBound = 1;
    this.upperBound = this.perPage;
    this.getRejectedBySRIVisitList();
  }
  public getRejectedBySRIVisitList(week?: TemplateRef<any>) {
    try {
      let obj = { lowerBound: this.lowerBound, upperBound: this.upperBound }
      this.dashBoardService.getRejectedBySRIVisitList(JSON.stringify(obj)).subscribe(res => {
        console.log(res);
        this.rejectedBySRIVisitList = res.rejectedVisitsList;
        this.totalCount = res.totalCount;

        if (week) {
          this.modalRef = this.modalService.show(week, {id:1, class: 'mx-w-95' });
        }
      })
    } catch (error) {

    }
  }



  exportexcel(): void {
    var wscols = [
      { wch: 10 },
      { wch: 10 },
      { wch: 10 },
      { wch: 20 },
      { wch: 25 }
    ];
    /* pass here the table id */
    let element = document.getElementById('excel-table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    ws["!cols"] = wscols    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Summary');

    /* save to file */
    XLSX.writeFile(wb, 'Summary.xlsx');

  }

  public clientMasterDownload() {

    let data = []
    data = this.clientMaster.map(item => {
      return {
        "SSN": item.ssn,
        "LNAME": item.lName,
        "FNAME": item.fName,
        "POC_SSN": item.pocSsn
      }
    })
    var wscols=[
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
    ]

    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
    worksheet["!cols"] = wscols
    const workbook: XLSX.WorkBook = { Sheets: { 'Client_master': worksheet }, SheetNames: ['Client_master'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, 'Client_master');


  }
  public clienAlternateIdtMasterDownload() {

    let data = []
    data = this.clientAltList.map(item => {
      return {
        "SSN": item.ssn,
        "LNAME": item.lName,
        "FNAME": item.fName,
        "UID_CLIENT":item.uidClient,
        "open":item.open,
        "closed":item.closed,
      }
    })
    var wscols=[
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
    ]

    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
    worksheet["!cols"] = wscols
    const workbook: XLSX.WorkBook = { Sheets: { 'Client_AlternateID_Master': worksheet }, SheetNames: ['Client_AlternateID_Master'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, 'Client_AlternateID_Master');


  }
  public clienWithNoPSDownload() {

    let data = []
    data = this.clientNoPSList.map(item => {
      return {
        "SSN": item.ssn,
        "LNAME": item.lName,
        "FNAME": item.fName,
        "UID_CLIENT":item.uidClient
      }
    })
    var wscols=[
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
    ]

    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
    worksheet["!cols"] = wscols
    const workbook: XLSX.WorkBook = { Sheets: { 'Clients_with_no_PS': worksheet }, SheetNames: ['Clients_with_no_PS'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, 'Clients_with_no_PS');


  }


  public authServiceMatchedDownload(){

    let data = []
    data = this.authServiceMatchedList.map(item => {
      return {
        "UID_CLIENT": item.uidClient,
        "CLIENT_NAME":item.clientName,
        "PANUM": item.PaNum,
        "TARGET": item.target,
        "MEDICAID":item.medIcaId,
        "PABEGIN":item.pabegIn,
        "PAEND":item.paend,
        "SRI_PROCCODE":item.sriProcCode,
        "PROCCODE":item.procCode,
        "MODIFIER":item.modifier,
        "CONVERTPROCCODE":item.convertProCode,
        "UNITTYPE":item.uniType,
        "AUTHUNIT":item.authUnit,
        "VOID_CAN":item.voidCan,
        "CANTYPE":item.canType,
        "OVERRIDE":item.override,
        "EDITDATE":item.editDate,
        "EDITSEC":item.editSet,
        "ISEVV":item.isEvv,
        "CPOCBEGIN":item.cpocBegin,
        "CPOCEND":item.cpocEnd,
        "CPOCCAP":item.cpocCap,
        "WEEKCAP":item.weekCap,

        "POC_TOTAL_UNITS":item.pocTotalUnits,
        "POC_WEEKLY_UNITS":item.pocWeeklyUnits,
      }

    })
    var wscols=[
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
    ]

    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
    worksheet["!cols"] = wscols
    const workbook: XLSX.WorkBook = { Sheets: { 'Auth_service_matched': worksheet }, SheetNames: ['Auth_service_matched'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, 'Auth_service_matched');


  }

  public authServiceNotMatchedDownload(){

    let data = []
    data = this.authServiceNotMatchedList.map(item => {
      return {
        "UID_CLIENT": item.uidClient,
        "CLIENT_NAME":item.clientName,
        "PANUM": item.PaNum,
        "TARGET": item.target,
        "MEDICAID":item.medIcaId,
        "PABEGIN":item.pabegIn,
        "PAEND":item.paend,
        "SRI_PROCCODE":item.sriProcCode,
        "PROCCODE":item.procCode,
        "MODIFIER":item.modifier,
        "CONVERTPROCCODE":item.convertProCode,
        "UNITTYPE":item.uniType,
        "AUTHUNIT":item.authUnit,
        "VOID_CAN":item.voidCan,
        "CANTYPE":item.canType,
        "OVERRIDE":item.override,
        "EDITDATE":item.editDate,
        "EDITSEC":item.editSet,
        "ISEVV":item.isEvv,
        "CPOCBEGIN":item.cpocBegin,
        "CPOCEND":item.cpocEnd,
        "CPOCCAP":item.cpocCap,
        "WEEKCAP":item.weekCap,
      }
    })
    var wscols=[
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
    ]

    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
    worksheet["!cols"] = wscols
    const workbook: XLSX.WorkBook = { Sheets: { 'Auth_service_not_matched': worksheet }, SheetNames: ['Auth_service_not_matched'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, 'Auth_service_not_matched');


  }

  public totalAuthUnitsNotMatchedDownload(){

    let data = []
    data = this.totalAuthUnitsNotMatchedList.map(item => {
      return {
        "UID_CLIENT": item.uidClient,
        "CLIENT_NAME":item.clientName,
        "PANUM": item.paNum,
        "TARGET": item.target,
        "MEDICAID":item.medicaid,
        "PABEGIN":item.paBegIn,
        "PAEND":item.paEnd,
        "SRI_PROCCODE":item.sriProcCode,
        "PROCCODE":item.procCode,
        "MODIFIER":item.modifier,
        "CONVERTPROCCODE":item.convertProCode,
        "UNITTYPE":item.unitType,
        "AUTHUNIT":item.authUnit,
        "VOID_CAN":item.voidCan,
        "CANTYPE":item.canType,
        "OVERRIDE":item.override,
        "EDITDATE":item.editDate,
        "EDITSEC":item.editSec,
        "ISEVV":item.isEvv,
        "CPOCBEGIN":item.cpocBegin,
        "CPOCEND":item.cpocEnd,
        "CPOCCAP":item.cpocCap,
        "WEEKCAP":item.weekCap,
        "POC_TOTAL_UNITS":item.pocTotalUnits,
        "POC_WEEKLY_UNITS":item.pocWeeklyUnits,
        "Diff in weekly Units":item.diffInWeeklyUnits
      }
    })
    var wscols=[
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
    ]

    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
    worksheet["!cols"] = wscols
    const workbook: XLSX.WorkBook = { Sheets: { 'Total_Auth_Units_Not_matched': worksheet }, SheetNames: ['Total_Auth_Units_Not_matched'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, 'Total_Auth_Units_Not_matched');


  }

  public weeklyUnitsNotMatchedDownload(){

    let data = []
    data = this.weeklyUnitsNotMatchedList.map(item => {
      return {
        "UID_CLIENT": item.uidClient,
        "CLIENT_NAME":item.clientName,
        "PANUM": item.paNum,
        "TARGET": item.target,
        "MEDICAID":item.medicaid,
        "PABEGIN":item.paBegIn,
        "PAEND":item.paEnd,
        "SRI_PROCCODE":item.sriProcCode,
        "PROCCODE":item.procCode,
        "MODIFIER":item.modifier,
        "CONVERTPROCCODE":item.convertProCode,
        "UNITTYPE":item.unitType,
        "AUTHUNIT":item.authUnit,
        "VOID_CAN":item.voidCan,
        "CANTYPE":item.canType,
        "OVERRIDE":item.override,
        "EDITDATE":item.editDate,
        "EDITSEC":item.editSec,
        "ISEVV":item.isEvv,
        "CPOCBEGIN":item.cpocBegin,
        "CPOCEND":item.cpocEnd,
        "CPOCCAP":item.cpocCap,
        "WEEKCAP":item.weekCap,
        "POC_TOTAL_UNITS":item.pocTotalUnits,
        "POC_WEEKLY_UNITS":item.pocWeeklyUnits,
        "Diff in weekly Units":item.diffInWeeklyUnits
      }
    })
    var wscols=[
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
    ]

    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
    worksheet["!cols"] = wscols
    const workbook: XLSX.WorkBook = { Sheets: { 'Weekly_Units_Not_matched': worksheet }, SheetNames: ['Weekly_Units_Not_matched'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, 'Weekly_Units_Not_matched');


  }

  public getUnMatchedAuthsDownload(){

    let data = []
    data = this.unMatchedAuthsList.map(item => {
      return {

        "PS":item.ps,
        "SSN":item.ssn,
        "ALTERNATE_ID":item.alternateId,
        "POLICY_NUMBER":item.policyNumber,
        "AUTHORIZATION_NUMBER":item.authorizationNumber,
        "BEGIN_DATE":item.beginDate,
        "END_ DATE":item.endDate,
        "POC_PROCEDURE_CODE":item.pocProcedureCode,
        "SRI_PROCEDURE_CODE":item.sriProcedureCode
      }
    })
    var wscols=[
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
    ]

    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
    worksheet["!cols"] = wscols
    const workbook: XLSX.WorkBook = { Sheets: { 'Unmatched_Auth_Service': worksheet }, SheetNames: ['Unmatched_Auth_Service'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, 'Unmatched_Auth_Service');


  }


  public rejectedBySriDownload(totalCount){

    // let totalcount=totalCount;


    let obj = { lowerBound: 1, upperBound: totalCount }
    this.dashBoardService.getRejectedBySRIVisitList(JSON.stringify(obj)).subscribe(res => {
      console.log(res);
      let data = []
      data = res.rejectedVisitsList.map(item => {
        return {
          "SERVDATE": item.servDate,
          "PROCCODE": item.procCode,
          "ENTERDATE": item.enterDate,
          "ENTERSEC":item.enterSec,
          "EDITBY":item.editBy,
          "EDITDATE":item.editDate,
          "SERVBEGIN":item.servBegin,
          "SERVEND":item.servEnd,
          "EVV_DSWID":item.evvDswId,
          "DCS":item.dcs,
          "UID_CLIENT":item.uidClient,
          "PS":item.ps,
          "UID_SERV":item.uidServ,
          "METHOD":item.method,
          "VOID":item.void,
          "IPIN":item.ipIn,
          "IPOUT":item.ipOut,
          "IPEDIT":item.ipEdit,
          "GEOIN":item.geoIn,
          "GEOOUT":item.geoOut,
          "GEOEDIT":item.geoEdit,
          "EDITTIME":item.editTime,
          "TIME":item.time,
          "TIME_DEDUC":item.timeDeduct,
          "ERROR_MSG":item.errorMsg,
          "SRI_PROCESSED":item.sriProcessed,
       }
      })


      var wscols=[
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
        { wch: 15 },
      ]

      const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
      worksheet["!cols"] = wscols
      const workbook: XLSX.WorkBook = { Sheets: { 'Rejected_by_SRI': worksheet }, SheetNames: ['Rejected_by_SRI'] };
      const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, 'Rejected_by_SRI');

    })


  }

public logModelRef:BsModalRef;
public rejectedBySRIVisitLogs=[];
public currentRejectedLog;
  public getRejectedBySRIVisitLog(item,template:TemplateRef<any>){
    try {
      this.currentRejectedLog=item;
      let obj={uidServ:item.uidServ}
      this.dashBoardService.getRejectedBySRIVisitLog(JSON.stringify(obj)).subscribe(res=>{

        console.log(res);
        this.rejectedBySRIVisitLogs=res;
        this.logModelRef=this.modalService.show(template,{id:2,class:'modal-lg'});

      },err=>{
        this.logModelRef=this.modalService.show(template,{id:2,class:'modal-lg'});

      })

    } catch (error) {

    }
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: 'string' });
    /***********`
    *YOUR EXCEL FILE'S NAME
    */
    FileSaver.saveAs(data, fileName + '.xlsx');
  }

}
