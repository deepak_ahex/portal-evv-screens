import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ReconciliationDcsComponent } from './reconciliation-dcs.component';

describe('ReconciliationDcsComponent', () => {
  let component: ReconciliationDcsComponent;
  let fixture: ComponentFixture<ReconciliationDcsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ReconciliationDcsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReconciliationDcsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
