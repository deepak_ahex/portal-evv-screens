import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { DashboardService } from 'src/app/services/dashboard.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-interface-token-credential',
  templateUrl: './interface-token-credential.component.html',
  styleUrls: ['./interface-token-credential.component.scss']
})
export class InterfaceTokenCredentialComponent implements OnInit {

  public tokenForm: UntypedFormGroup;
  public interfaceName: string = '';
  public formError: boolean = false;
  public interfaceId: number;
  public tokenCredntialsList = [];
  public datasetLookup = [];
  public userData: any;
  constructor(private fb: UntypedFormBuilder, private dashboardService: DashboardService) {
    this.getInterFaceDetails();
    this.createForm();
  }
  ngOnInit(): void {
  }
  createForm() {
    this.formError = false;
    this.tokenForm = this.fb.group({
      interfaceDatasetId: new UntypedFormControl(null, [Validators.required]),
      webServiceUrl: new UntypedFormControl(null, [Validators.required]),
      clientId: new UntypedFormControl(''),
      clientSecret: new UntypedFormControl(''),
      id: new UntypedFormControl(0),
      active: new UntypedFormControl(1),
      additionalData: new UntypedFormControl(''),
    })
  }
  public updateTokenForm(obj) {
    console.log(obj)

    this.tokenForm.get('interfaceDatasetId').setValue(+(obj.interfaceDatasetId));
    this.tokenForm.get('webServiceUrl').setValue(obj.wsUrl);
    this.tokenForm.get('clientId').setValue(obj.clientId);
    this.tokenForm.get('clientSecret').setValue(obj.clientSecret);
    this.tokenForm.get('id').setValue(obj.id);
    this.tokenForm.get('additionalData').setValue(obj.additionalData);
    this.tokenForm.get('active').setValue(obj.active)


  }
  public saveInterfaceTokenCredentials() {
    this.formError = true;
    if (this.tokenForm.valid) {

      try {
        let obj = { "interfaceDefinitionId": this.interfaceId, "id": this.tokenForm.value.id, "interfaceDatasetId": this.tokenForm.value.interfaceDatasetId, "wsUrl": this.tokenForm.value.webServiceUrl, "clientId": this.tokenForm.value.clientId, "clientSecret": this.tokenForm.value.clientSecret, "additionalData": this.tokenForm.value.additionalData, "active": this.tokenForm.value.active, "userId": this.userData.userId };
        this.dashboardService.saveInterfaceTokenCredentials(obj).subscribe(data => {
          console.log(data);
          if (data.saveFlag == 1) {
            Swal.fire({
              title: 'success',
              text: data.saveMsg,
              icon: 'success',
              timer: 1200,
              showConfirmButton: false
            });
            this.getInterfaceTokenCredentialsList();
            this.createForm();
          } else {
            Swal.fire({
              title: 'Failed',
              text: data.saveMsg,
              icon: 'error',
              timer: 1200,
              showConfirmButton: false
            });
          }
        })
      } catch (error) {

      }
      console.log(this.tokenCredntialsList)
    }
  }
  get formControl() {
    return this.tokenForm.controls;
  }
  public getInterFaceDetails() {
    var data = JSON.parse(sessionStorage.getItem('interfaceData'));
    this.userData = JSON.parse(sessionStorage.getItem('userData'))
    // console.log(data.name)
    this.interfaceName = data?.label;
    this.interfaceId = data?.id;
    this.getInterfaceTokenCredentialsList();
    this.getInterfaceDatasetList();
  }

  public getInterfaceTokenCredentialsList() {
    try {
      this.dashboardService.getInterfaceTokenCredentialsList(this.interfaceId).subscribe(
        res => {
          console.log(res)
          this.tokenCredntialsList = res;
        }
      )
    } catch (error) {

    }
  }

  public getInterfaceDatasetList() {
    try {
      this.dashboardService.getInterfaceDatasetList(this.interfaceId).subscribe(
        res => {
          console.log(res);
          this.datasetLookup = res;
        }
      )
    } catch (error) {

    }
  }
  public onActivechange(event) {
    this.tokenForm.get('active').setValue(event.target.checked ? 1 : 0)
  }
}
