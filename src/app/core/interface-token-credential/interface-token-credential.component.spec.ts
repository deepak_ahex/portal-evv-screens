import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InterfaceTokenCredentialComponent } from './interface-token-credential.component';

describe('InterfaceTokenCredentialComponent', () => {
  let component: InterfaceTokenCredentialComponent;
  let fixture: ComponentFixture<InterfaceTokenCredentialComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InterfaceTokenCredentialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterfaceTokenCredentialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
