import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InterfacePayorMappingComponent } from './interface-payor-mapping.component';

describe('InterfacePayorMappingComponent', () => {
  let component: InterfacePayorMappingComponent;
  let fixture: ComponentFixture<InterfacePayorMappingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InterfacePayorMappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterfacePayorMappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
