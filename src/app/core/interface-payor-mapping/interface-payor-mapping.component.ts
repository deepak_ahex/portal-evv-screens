import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-interface-payor-mapping',
  templateUrl: './interface-payor-mapping.component.html',
  styleUrls: ['./interface-payor-mapping.component.scss']
})
export class InterfacePayorMappingComponent implements OnInit {

  public interfaceName:string='';
  public payorMappingList=[
    {pocPayorPlan:'SAMPLE1',office:'Office1',active:1},
    {pocPayorPlan:'SAMPLE2',office:'Office2',active:1},
    {pocPayorPlan:'SAMPLE3',office:'Office3',active:0},
    {pocPayorPlan:'SAMPLE4',office:'Office4',active:0},
    {pocPayorPlan:'SAMPLE5',office:'Office5',active:1},

  ];
  constructor() {
    var data=JSON.parse(sessionStorage.getItem('interfaceData'));
    // console.log(data.name)
    this.interfaceName=data.name;
   }

  ngOnInit(): void {
  }
  public getInterFaceDetails() {
    var data = JSON.parse(sessionStorage.getItem('interfaceData'));
    // console.log(data.name)
    this.interfaceName = data?.label;
  }

}
