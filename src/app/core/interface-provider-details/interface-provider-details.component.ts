import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, Validators } from '@angular/forms';
import { DashboardService } from 'src/app/services/dashboard.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-interface-provider-details',
  templateUrl: './interface-provider-details.component.html',
  styleUrls: ['./interface-provider-details.component.scss']
})
export class InterfaceProviderDetailsComponent implements OnInit {

  public interfaceName = '';
  public formError: boolean = false;
  public interfaceId: number;
  public providerForm: UntypedFormGroup;
  public officeLookup=[];
  public providerList =[];
  public userData:any;
  public mappedPayorPlanList=[];

  constructor(private _fb: UntypedFormBuilder, private _dashBoard: DashboardService) {
    this.getInterFaceDetails();
  }
  ngOnInit(): void {
  }
  public createForm() {
    this.formError = false;
    this.providerForm = this._fb.group({
      officeId: new UntypedFormControl(null, Validators.required),
      npi: new UntypedFormControl('',),
      name: new UntypedFormControl('',),
      providerMedicaidId: new UntypedFormControl('',),
      providerId: new UntypedFormControl('',),
      payorPlanId: new UntypedFormControl(null, [Validators.required]),
      provideEin: new UntypedFormControl('', ),
      taxonomy: new UntypedFormControl('', ),
      id:new UntypedFormControl(0)
    })
  }
  get formcontrols() {
    return this.providerForm.controls;
  }
  public updateProvideForm(obj) {
    this.providerForm.get('officeId').setValue(obj.officeId);
    this.providerForm.get('npi').setValue(obj.npi);
    this.providerForm.get('name').setValue(obj.name);
    this.providerForm.get('providerMedicaidId').setValue(obj.providerMedicaidId);
    this.providerForm.get('providerId').setValue(obj.providerId);
    this.providerForm.get('payorPlanId').setValue(+(obj.payorPlanId));
    this.providerForm.get('provideEin').setValue(obj.providerEin);
    this.providerForm.get('taxonomy').setValue(obj.taxonomy);
    this.providerForm.get('id').setValue(obj.id);
    this.getMappedPayorPlanList();
  }
  public getInterFaceDetails() {
    var data = JSON.parse(sessionStorage.getItem('interfaceData'));
    this.userData = JSON.parse(sessionStorage.getItem('userData'));

    // console.log(data.name)
    this.interfaceName = data?.label;
    this.interfaceId = data?.id;

    this.createForm();
    this.getInterfaceOffices();
    this.getProviderDetailsList();


  }

  public saveProviderDetails() {
    this.formError = true;
    if(this.providerForm.valid){
      let obj={"interfaceDefinitionId":this.interfaceId,
      "id":this.providerForm.value.id,
      "officeId":this.providerForm.value.officeId,
      "npi":this.providerForm.value.npi,
      "name":this.providerForm.value.name,
      "taxonomy":this.providerForm.value.taxonomy,
      "providerId":this.providerForm.value.providerId,
      "providerMedicaidId":this.providerForm.value.providerMedicaidId,
      "payorPlanId":this.providerForm.value.payorPlanId,
      "providerEin":this.providerForm.value.provideEin,
      "userId":this.userData.userId}

      try {
        this._dashBoard.saveProviderDetails(obj).subscribe(data=>{
          console.log(data);
          if (data.saveFlag == 1) {
              Swal.fire({
                title: 'success',
                text: data.saveMsg,
                icon: 'success',
                timer: 1200,
                showConfirmButton: false
              });
              this.getProviderDetailsList();
              this.createForm();
            } else {
              Swal.fire({
                title: 'Failed',
                text: data.saveMsg,
                icon: 'error',
                timer: 1200,
                showConfirmButton: false
              });
            }

        })
      } catch (error) {

      }
    }

  }

  public getProviderDetailsList() {
    try {
      this._dashBoard.getProviderDetailsList(this.interfaceId).subscribe(res=>{
        this.providerList=res;
        console.log(res)
      })
    } catch (error) {

    }
  }
  public getInterfaceOffices() {
    try {
      this._dashBoard.getInterfaceOffices(this.interfaceId).subscribe(
        res => {
          console.log(res);
          let data: any = res;
          this.officeLookup = data;
        }
      )
    } catch (error) {

    }
  }
  public getMappedPayorPlanList() {
    let officeId = this.providerForm.value.officeId;
    console.log('OfficeId',officeId)
    if (officeId!= null) {
      try {
        this._dashBoard.getMappedPayorPlanList(officeId, this.interfaceId).subscribe(res => {
          console.log(res);
          this.mappedPayorPlanList = res;
        })
      } catch (error) {

      }
    }else{
      this.providerForm.get('payorPlanId').setValue(null);
      this.mappedPayorPlanList=[];

    }

  }
}
