import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InterfaceProviderDetailsComponent } from './interface-provider-details.component';

describe('InterfaceProviderDetailsComponent', () => {
  let component: InterfaceProviderDetailsComponent;
  let fixture: ComponentFixture<InterfaceProviderDetailsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InterfaceProviderDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterfaceProviderDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
