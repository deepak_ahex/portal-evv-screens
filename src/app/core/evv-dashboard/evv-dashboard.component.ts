import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { EvvServiceService } from 'src/app/services/evv-service.service';

@Component({
  selector: 'app-evv-dashboard',
  templateUrl: './evv-dashboard.component.html',
  styleUrls: ['./evv-dashboard.component.scss']
})
export class EvvDashboardComponent implements OnInit {
  public InterfaceDetailsList: any = [];
  public InterfaceDataSetDetailsList: any = [];



  private payload: any;
  public DashboardData:any;
  constructor(private evvServiceService: EvvServiceService,private router:Router) {

   }

  ngOnInit(): void {
    this.getDashboardData();
  }
  getDashboardData(): void {
    this.payload = {interfaceDefinitionId: 0};
    this.evvServiceService.getEvvDashBoardData(JSON.stringify(this.payload)).subscribe((res) => {
      console.log(res);
      this.DashboardData = res;
    });
  }

  navigate(event){
    this.router.navigateByUrl(event)
  }




}
