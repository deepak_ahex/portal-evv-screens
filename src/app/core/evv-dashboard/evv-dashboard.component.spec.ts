import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EvvDashboardComponent } from './evv-dashboard.component';

describe('EvvDashboardComponent', () => {
  let component: EvvDashboardComponent;
  let fixture: ComponentFixture<EvvDashboardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EvvDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvvDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
