import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MetricDetailsSummaryComponent } from './metric-details-summary.component';

describe('MetricDetailsSummaryComponent', () => {
  let component: MetricDetailsSummaryComponent;
  let fixture: ComponentFixture<MetricDetailsSummaryComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MetricDetailsSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetricDetailsSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
