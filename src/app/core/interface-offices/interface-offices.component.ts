import { Component, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { DashboardService } from 'src/app/services/dashboard.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-interface-offices',
  templateUrl: './interface-offices.component.html',
  styleUrls: ['./interface-offices.component.scss'],
})
export class InterfaceOfficesComponent implements OnInit {
  public interfaceName: string = '';
  public interfaceId: number;
  public formError: boolean = false;
  public officesList = [];
  public officeForm: UntypedFormGroup;
  public CreateOfficeForm: UntypedFormGroup;

  public officeLookup = [];
  public exportedTables = [];
  public userData: any;
  constructor(
    private fb: UntypedFormBuilder,
    private dashBoardSerVice: DashboardService
  ) {
    this.getInterFaceDetails();
    this.getInterfaceExportedTables();
  }

  ngOnInit(): void {}

  public createForm() {
    this.formError = false;
    this.officeForm = this.fb.group({
      dataset: new UntypedFormControl(null, [Validators.required]),
      officeId: new UntypedFormControl(null,[Validators.required]),
      active: new UntypedFormControl(1),
      id: new UntypedFormControl(0),
    });
    this.CreateOfficeForm = this.fb.group({
      dataset: new UntypedFormControl([], [Validators.required]),
      officeId: new UntypedFormControl([],[Validators.required]),
      active: new UntypedFormControl(1),
      id: new UntypedFormControl(0),
    });
  }
  get formcontrols() {
    return this.officeForm.controls;
  }
  get CreateFormcontrols() {
    return this.CreateOfficeForm.controls;
  }
  public getInterFaceDetails() {
    this.createForm();
this.formError=false;
    var data = JSON.parse(sessionStorage.getItem('interfaceData'));
    this.userData = JSON.parse(sessionStorage.getItem('userData'));
    // console.log(data.name);
    this.interfaceName = data?.name;
    this.interfaceId = data?.id;
    this.getInterfaceOfficesList();
    this.getPOCOffices();
  }
  public saveInterfaceOffices() {
    this.formError = true;
    if (this.officeForm.valid && this.officeForm.value.officeId > 0) {
      let obj = {
        interfaceDefinitionId: this.interfaceId,
        id: this.officeForm.value.id,
        officeId: this.officeForm.value.officeId,
        interfaceDataset: this.officeForm.value.dataset,
        active: this.officeForm.value.active,
        userId: this.userData.userId,
      };
      console.log(this.officeForm.valid);
      try {
        this.dashBoardSerVice.saveInterfaceOffices(obj).subscribe((res) => {
          console.log(res);
          let data: any = res;

          if (data.saveFlag == 1) {
            Swal.fire({
              title: 'success',
              text: data.saveMsg,
              icon: 'success',
              timer: 1200,
              showConfirmButton: false,
            });
            this.getInterfaceOfficesList();
            this.createForm();
          } else {
            Swal.fire({
              title: 'Failed',
              text: data.saveMsg,
              icon: 'error',
              timer: 1200,
              showConfirmButton: false,
            });
          }
        });
      } catch (error) {}
    }
  }

  public getInterfaceOfficesList() {
    try {
      this.dashBoardSerVice
        .getInterfaceOfficesList(this.interfaceId)
        .subscribe((res) => {
          console.log(res);
          this.officesList = res;
        });
    } catch (error) {}
  }
  public updateOffices(obj) {
    this.officeForm.get('officeId').setValue(obj.officeId);
    this.officeForm.get('dataset').setValue(obj.interfaceDataset);
    this.officeForm.get('active').setValue(obj.active);
    this.officeForm.get('id').setValue(obj.id);
  }
  public getPOCOffices() {
    try {
      this.dashBoardSerVice.getPOCOffices().subscribe((res) => {
        console.log(res);
        let data: any = res;
        this.officeLookup = data;
      });
    } catch (error) {}
  }
  public onActivechange(event,flag) {

    flag=='create'?this.CreateOfficeForm.get('active').setValue(event.target.checked ? 1 : 0):this.officeForm.get('active').setValue(event.target.checked ? 1 : 0);

    console.log(event.target.checked);
  }

  public getInterfaceExportedTables() {
    try {
      this.dashBoardSerVice.getInterfaceExportedTables().subscribe((res) => {
        this.exportedTables=res;
      });
    } catch (error) {

    }
  }


  public createInterfaceOffices(){
   try {
    this.formError = true;
    console.log(this.CreateOfficeForm.valid,this.CreateOfficeForm.value)
    if (this.CreateOfficeForm.valid ) {
      let obj = {
        interfaceDefinitionId: this.interfaceId,
        id: this.CreateOfficeForm.value.id,
        officeIds: this.CreateOfficeForm.value.officeId.toString(),
        interfaceDatasets: this.CreateOfficeForm.value.dataset.toString(),
        active: this.CreateOfficeForm.value.active,
        userId: this.userData.userId,
      };
      console.log(this.CreateOfficeForm.valid);
      try {
        this.dashBoardSerVice.createInterfaceOffices(obj).subscribe((res) => {
          console.log(res);
          let data: any = res;

          if (data.saveFlag == 1) {
            Swal.fire({
              title: 'success',
              text: data.saveMsg,
              icon: 'success',
              timer: 1200,
              showConfirmButton: false,
            });
            this.getInterfaceOfficesList();
            this.createForm();
          } else {
            Swal.fire({
              title: 'Failed',
              text: data.saveMsg,
              icon: 'error',
              timer: 1200,
              showConfirmButton: false,
            });
          }
        });
      } catch (error) {}
    }
   } catch (error) {

   }
  }
}
