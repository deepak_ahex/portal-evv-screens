import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InterfaceOfficesComponent } from './interface-offices.component';

describe('InterfaceOfficesComponent', () => {
  let component: InterfaceOfficesComponent;
  let fixture: ComponentFixture<InterfaceOfficesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InterfaceOfficesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterfaceOfficesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
