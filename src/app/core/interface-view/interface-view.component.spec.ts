import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InterfaceViewComponent } from './interface-view.component';

describe('InterfaceViewComponent', () => {
  let component: InterfaceViewComponent;
  let fixture: ComponentFixture<InterfaceViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InterfaceViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterfaceViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
