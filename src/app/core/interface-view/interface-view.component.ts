import { DatePipe } from '@angular/common';
import { EventEmitter, Input, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { UntypedFormArray, UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { idLocale } from 'ngx-bootstrap/chronos';
import { EvvServiceService } from 'src/app/services/evv-service.service';
import Swal from 'sweetalert2';
import { DcoumentComponent } from '../dcoument/dcoument.component';
import { DcsProviderDetailsComponent } from '../dcs-provider-details/dcs-provider-details.component';
import { InterfaceJobsComponent } from '../interface-jobs/interface-jobs.component';
import { InterfaceMediumComponent } from '../interface-medium/interface-medium.component';
import { InterfaceOfficesComponent } from '../interface-offices/interface-offices.component';
import { InterfaceProviderDetailsComponent } from '../interface-provider-details/interface-provider-details.component';
import { InterfaceServicesComponent } from '../interface-services/interface-services.component';
import { InterfaceStaticValuesComponent } from '../interface-static-values/interface-static-values.component';
import { InterfaceStatusComponent } from '../interface-status/interface-status.component';
import { InterfaceTokenCredentialComponent } from '../interface-token-credential/interface-token-credential.component';
import { PayorDetailsComponent } from '../payor-details/payor-details.component';
import { PsProviderDetailsComponent } from '../ps-provider-details/ps-provider-details.component';
import { VisitDeletedMediumComponent } from '../visit-deleted-medium/visit-deleted-medium.component';
declare const $: any;

@Component({
  selector: 'app-interface-view',
  templateUrl: './interface-view.component.html',
  styleUrls: ['./interface-view.component.scss'],

})
export class InterfaceViewComponent implements OnInit {
  @ViewChild(PayorDetailsComponent) payorDetails: PayorDetailsComponent;
  @ViewChild(InterfaceStaticValuesComponent) staticValue: InterfaceStaticValuesComponent;
  @ViewChild(InterfaceServicesComponent) service: InterfaceServicesComponent;
  @ViewChild(InterfaceOfficesComponent) offices: InterfaceOfficesComponent;
  @ViewChild(InterfaceTokenCredentialComponent) token: InterfaceTokenCredentialComponent;
  @ViewChild(InterfaceProviderDetailsComponent) provider: InterfaceProviderDetailsComponent;
  @ViewChild(VisitDeletedMediumComponent) visitDelete: VisitDeletedMediumComponent;
  @ViewChild(DcoumentComponent) document: DcoumentComponent;
  @ViewChild(InterfaceJobsComponent) jobComponent: InterfaceJobsComponent;
  @ViewChild(InterfaceMediumComponent) mediumComponent: InterfaceMediumComponent;
  @ViewChild(PsProviderDetailsComponent) PSprovider: PsProviderDetailsComponent;
  @ViewChild(DcsProviderDetailsComponent) DCSprovider: DcsProviderDetailsComponent;
  @ViewChild(InterfaceStatusComponent) statusComponent: InterfaceStatusComponent;










  lookUpData: any = {};
  submitted = false;
  payload: any = {};
  public InterfaceDetailsList: any = [];
  public InterfaceDataSetById: any = { name: 'name' };
  public interface;
  public Dataset;
  public DatasetId;

  public pocDataSetElementsList = [];
  public pocDataSetList;
  public InterfaceElementsDetailsList: Array<any> = [];
  public interfaceStaticValues;
  public dataType;
  public dateFormat;
  public interfaceElementsId: number;
  public lookupArray: Array<any>;
  public lookupSubmit: boolean = false;
  public savelookupdetailsError: boolean = false;



  type: any;
  addForm: UntypedFormGroup
  userData: any;
  InterfaceDataSetDetailsList: Array<any> = [];
  searchTerm: any;
  public perPage: any = 10;
  showType = 'createDataset';
  public lookUpNameList: any = [];

  public lookupDisplay: Array<boolean> = [false, false, false];

  postInterfaceDataSet: any = {}
  constructor(
    private evvServiceService: EvvServiceService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: UntypedFormBuilder,
    public datePipe: DatePipe,
    private router: Router) { }

  public lookupDetailsList: any = [];
  public ExportPOCLookupList: Array<any> = [];

  public displayArray: Array<boolean> = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];

  ngOnInit(): void {
    this.userData = JSON.parse(sessionStorage.getItem('userData'));
    const type = this.activatedRoute.snapshot.params.id;
    this.getInterfaceExportedTables();
    const IntrfaceId = this.activatedRoute.snapshot.params.IntrfaceId;
    const interfaceName = this.activatedRoute.snapshot.params.interfaceName;
    if (type) {
      this.viewContentType(type, +(IntrfaceId), interfaceName);
      this.viewDataInterfaceId = IntrfaceId;
      this.interface = interfaceName;
    }
    this.getInterfaceDetailsList();
    // this.dataSetsInitForm();
    // this.datasetElementInit();
    // this.interfaceLookupInnit();

  }

  getInterfaceDetailsList(): void {
    this.payload = { name: '', active: '' };
    this.evvServiceService.getInterfaceDetailsList(JSON.stringify(this.payload)).subscribe((res) => {
      this.InterfaceDetailsList = res;
      console.log(this.InterfaceDetailsList)

    });
  }
  viewDataInterfaceId: any;
  public interfaceData: any;
  public viewContentType(type, id, name, data?) {
    this.interfaceData = data;
    data ? sessionStorage.setItem('interfaceData', JSON.stringify(data)) : '';
    this.displayArray = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
    this.interface = name;
    this.viewDataInterfaceId = id;


    if (type == 'dataset') {
      this.submitted = false;
      this.savelookupdetailsError = false;
      this.displayArray[1] = true;
      setTimeout(() => {
        this.showType = 'createDataset'
      }, 100);
      this.dataSetsInitForm(id);
      this.datasetElementInit();

    } else if (type == 'lookup') {
      this.lookupDataById = { "interfaceDefinitionId": 0, "sortOrder": null, "active": 1, "id": 0, "title": "", "lookupName": null };
      this.lookupDisplay = [true, false, false]
      this.displayArray[2] = true;
      this.interfaceLookupInnit();

    } else if (type == 'doc') {
      this.displayArray[3] = true;
      this.document?.getInterFaceDetails();

    }
    else if (type == 'lookupdetails') {
      this.displayArray[4] = true;
      this.pocLookupDetails = [];
      this.lookupDetailsList = [];
      this.interfaceLookupList = [];
      this.interfaceLookupId = 0;
      this.lookupmappingSubmit = false;

      this.getExportPOCLookupList();
    }
    else if (type == 'interfacePayorDetails') {
      this.displayArray[5] = true;
      this.payorDetails?.getInterFaceDetails();
    }
    else if (type == 'interfaceJobs') {
      this.displayArray[6] = true;
      this.jobComponent?.getInterFaceDetails();

    }
    else if (type == 'interfacePocServicesMap') {
      this.displayArray[7] = true;
    }
    else if (type == 'interfaceServices') {
      this.displayArray[8] = true;
      this.service?.getInterFaceDetails();

    }
    else if (type == 'interfaceOffice') {
      this.displayArray[9] = true;
    }
    else if (type == 'interfaceMedium') {
      this.displayArray[10] = true;
      this.mediumComponent?.getInterFaceDetails()
    }
    else if (type == 'interfaceOffices') {
      this.displayArray[11] = true;
      this.offices?.getInterFaceDetails();
    }
    else if (type == 'interfaceToken') {
      this.displayArray[12] = true;
      this.token?.getInterFaceDetails();
    }
    else if (type == 'visitMedium') {
      this.displayArray[13] = true;
      this.visitDelete?.getInterFaceDetails();
    }
    else if (type == 'providerDetails') {
      this.displayArray[14] = true;
      this.provider?.getInterFaceDetails();
    }
    else if (type == 'staticValues') {
      this.displayArray[15] = true;
      this.staticValue?.getInterFaceDetails();
    }
    else if (type == 'psproviderDetails') {
      this.displayArray[16] = true;
      this.PSprovider?.getInterFaceDetails();

    } else if (type == 'dcsproviderDetails') {
      this.displayArray[17] = true;
      this.DCSprovider?.getInterFaceDetails();

    } else if (type == 'statusdetails') {
      this.displayArray[18] = true;
      this.statusComponent?.getInterFaceDetails();
    }

  }
  editInterface(id) {
    sessionStorage.setItem('interfaceId', id)
    this.router.navigate(['/interface']);
  }

  // ---------------------------------------------------------datasets start------------------------------------------- //
  dataSetsInitForm(id, flag?) {
    console.log(id);
    this.submitted=false;
    this.addForm = this.formBuilder.group({
      interfaceDefinitionId: [id],
      name: [null, Validators.required],
      dataFlowId: [null, Validators.required],
      dataMediumId: [null],
      dataFileFormatId: [null],
      dataFileDelimeterId: [null],
      description: [''],
      effectiveFrom: [''],
      interfaceExportedTable: [null, Validators.required],
      isSyncProviderDetails: [0]
    });

    if (flag) {

    } else {
      this.getAllInterfacesDataSets();
      this.getLookUpData();
    }

  }
  p(e) {
  }
  get f() { return this.addForm.controls; }
  getLookUpData(): void {
    const url = 'interface_definition,data_flow,data_medium,data_file_format,data_file_delimeter,data_type,date_format';
    this.evvServiceService.getLookUpData(url).subscribe((res) => {
      this.lookUpData = res;
    });

  }

  getAllInterfacesDataSets(): void {
    // this.payload = { name: '', active: '', interfaceDefinitionId: 0 };
    this.payload = { name: '', active: '', interfaceDefinitionId: this.viewDataInterfaceId };
    this.evvServiceService.getInterfaceDetailsDataSetsList(JSON.stringify(this.payload)).subscribe((res) => {
      let data: any = res
      this.InterfaceDataSetDetailsList = data;
      console.log("getAllInterfacesDataSets", res)
    });
  }

  getInterfacesDataSetById(id): void {
    this.showType = 'editDataset'
    this.evvServiceService.getInterfaceDetailsDataSetById(JSON.stringify(id)).subscribe((res) => {
      this.InterfaceDataSetById = res;
    });
  }
  public interfaceExportedTable = null;
  saveInterfaceDataSet(): void {
    this.submitted = true;
    // let obj =this.InterfaceDataSetDetailsList.find(o=>{
    //   return o.name===this.postInterfaceDataSet.name });
    console.log(this.userData.userId, this.postInterfaceDataSet.interfaceExportedTable)
    // console.log(obj)
    if (this.addForm.valid) {
      let parameters = {
        "id": 0, "name": this.postInterfaceDataSet.name,
        "description": this.postInterfaceDataSet.description == null || undefined ? '' : this.postInterfaceDataSet.description, "effectiveDate": this.postInterfaceDataSet.effectiveDate != undefined ? this.datePipe.transform(this.postInterfaceDataSet?.effectiveDate, 'MM/dd/yyyy') : '',
        "interfaceExportedTable": this.postInterfaceDataSet.interfaceExportedTable,
        "interfaceDefinitionId": this.viewDataInterfaceId, "dataFlowId": this.postInterfaceDataSet.dataFlowId, "dataMediumId": this.postInterfaceDataSet.dataMediumId == null || undefined ? 0 : this.postInterfaceDataSet.dataMediumId, "dataFileFormatId": this.postInterfaceDataSet.dataFileFormatId == null || undefined ? 0 : this.postInterfaceDataSet.dataFileFormatId, "dataFileDelimeterId": this.postInterfaceDataSet.dataFileDelimeterId == null || undefined ? 0 : this.postInterfaceDataSet.dataFileDelimeterId, "active": 1, isSyncProviderDetails: this.postInterfaceDataSet.isSyncProviderDetails
      }
      this.evvServiceService.createInterfaceDataSetByUserId(JSON.stringify(this.userData.userId),
        JSON.stringify(parameters)).subscribe((res) => {
          let data: any = res;
          if (data.saveFlag == 1) {
            console.log(res)
            this.getAllInterfacesDataSets();
            this.submitted = false;
            const swalData: any = res;
            Swal.fire({
              title: 'success',
              text: swalData.saveMsg,
              icon: 'success',
              timer: 1200,
              showConfirmButton: false
            });
            this.postInterfaceDataSet = {
              id: 0,
              name: '',
              description: '',
              interfaceDefinitionId: null,
              dataFlowId: null,
              dataMediumId: null,
              dataFileFormatId: null,
              dataFileDelimeterId: null,
              active: 1
            }
          } else {
            Swal.fire({
              title: 'Error',
              text: data.saveMsg,
              icon: 'error',
              // timer: 1200,
              showConfirmButton: true
            });
          }
        });
    }
    // else if(obj!=undefined){
    //   Swal.fire({
    //     title: 'Invalid Name',
    //     text: 'Datset has already been created with same Interface  ',
    //     icon: 'error',
    //     // timer: 1200,
    //     showConfirmButton: true
    //   });
    // }

  }
  onfileFormatChange(value, flag) {
    if (value == 1 || value == 2 || value == 5) {
    } else {
      console.log('+++++++')
      flag == 'create' ? this.postInterfaceDataSet.dataFileDelimeterId = 0 : this.InterfaceDataSetById.dataFileDelimeterId = 0;

    }
  }
  onExportchange(value, flag) {
    console.log(value)
    if (value == 'exported_visit_data') {
    } else {
      console.log('++++++++')
      flag == 'create' ? this.postInterfaceDataSet.isSyncProviderDetails = 0 : this.InterfaceDataSetById.isSyncProviderDetails == 0;

      console.log(this.postInterfaceDataSet.isSyncProviderDetails, this.InterfaceDataSetById?.isSyncProviderDetails)
    }
  }

  updateInterfaceDataSet(): void {
    console.log(this.InterfaceDataSetById)
    this.submitted = true;
    let effectiveDate = this.datePipe.transform(this.InterfaceDataSetById.effectiveDate, 'MM/dd/yyyy');
    const payload = {
      active: this.InterfaceDataSetById.active,
      dataFileDelimeterId: this.InterfaceDataSetById.dataFileDelimeterId == null ? 0 : this.InterfaceDataSetById.dataFileDelimeterId,
      dataFileFormatId: this.InterfaceDataSetById.dataFileFormatId == null ? 0 : this.InterfaceDataSetById.dataFileFormatId,
      dataFlowId: this.InterfaceDataSetById.dataFlowId,
      dataMediumId: this.InterfaceDataSetById.dataMediumId == null ? 0 : this.InterfaceDataSetById.dataMediumId,
      description: this.InterfaceDataSetById.description,
      id: this.InterfaceDataSetById.id,
      interfaceDefinitionId: this.InterfaceDataSetById.interfaceDefinitionId,
      name: this.InterfaceDataSetById.name,
      effectiveDate: effectiveDate == null ? '' : effectiveDate,
      interfaceExportedTable: this.InterfaceDataSetById.interfaceExportedTable,
      isSyncProviderDetails: this.InterfaceDataSetById.isSyncProviderDetails
    }
    console.log(payload)
    if (payload.name && payload.dataFlowId && payload.interfaceExportedTable.length > 0) {
      console.log(payload)
      this.evvServiceService.createInterfaceDataSetByUserId(JSON.stringify(this.userData.userId),
        JSON.stringify(payload)).subscribe((res) => {
          let data: any = res
          if (data.saveFlag == 1) {
            console.log(res)
            this.submitted = false;
            this.showType = 'createDataset'
            this.getAllInterfacesDataSets();
            const swalData: any = res;
            Swal.fire({
              title: 'success',
              text: swalData.saveMsg,
              icon: 'success',
              timer: 1200,
              showConfirmButton: false
            });
            this.InterfaceDataSetById = {}
          }
          else {
            Swal.fire({
              title: 'Failed',
              text: 'Failed to save',
              icon: 'error',
              // timer: 1200,
              showConfirmButton: true
            });
          }
        });
    }
  }
  cancelupdateInterfaceDataSet() {
    this.submitted = false;
    this.showType = 'createDataset'
    this.getAllInterfacesDataSets();
  }
  dataSetId: any;
  viewDatasetElement(dataSetId: any) {
    console.log(dataSetId)
    this.dataSetId = dataSetId.id;
    this.Dataset = dataSetId.name
    console.log(this.dataSetId)
    this.getAllInterfaceElements();

    // this.showType = 'datasetElement';

  }
  // ------------showDatasetElements start-------//
  private elementId: any;
  public InterfaceElementList: any = [];
  public singleInterfaceDetails: any = {};
  public saveInterface: any = {};
  public lookUpElementsData: any = {};
  public pocDataSets: any;
  public pocElements: any;
  public InterfaceElementById: any = [];
  public showUpdate = true;
  public lookupEle: any = {};
  public lookupEleForm: UntypedFormGroup;
  public getLength: number;
  public showDeleteIcon: boolean;
  public elementItems: any = [];
  public addScheduleOverrideForm: UntypedFormGroup;


  datasetElementInit() {

    this.addScheduleOverrideForm = new UntypedFormGroup({
      openHours: new UntypedFormArray([this.createForm({})], [Validators.required]),
    });
    this.userData = JSON.parse(sessionStorage.getItem('userData'));
    // this.getAllInterfaceElements();
    this.getLookUpDataForElements();
    // this.getPOCDataSets();
    // this.getPOCElements();
  }


  setUpScheduleOverrideForm(data: any) {
    return new UntypedFormGroup({
      openHours: new UntypedFormArray(data.interfaceElementsDetailsList.map((f) => this.createForm(f))),
    });
  }

  createForm(f) {
    return new UntypedFormGroup({
      id: new UntypedFormControl(f.id || 0),
      elementName: new UntypedFormControl(f.elementName || '', Validators.required),
      pocDatasetName: new UntypedFormControl(f.pocDatasetName || null, Validators.required),
      pocElementName: new UntypedFormControl(f.pocElementName || null, Validators.required),
      sortOrder: new UntypedFormControl(f.sortOrder || null, Validators.required),
    });
  }


  get openHoursArray() {
    return (this.addScheduleOverrideForm.get('openHours') as UntypedFormArray);
  }
  changeDataSet(e) {
    if (e == undefined) {
      return;
    } else {
      this.elementItems = [];
      this.pocElements.forEach(ele => {
        if (ele.datasetId === e.datasetId) {
          this.elementItems.push(ele);
        }
      });
    }
  }
  addOpenHours() {
    const newfield = {};
    this.openHoursArray.push(this.createForm(newfield));
  }
  removeOpenHours(j) {
    this.openHoursArray.removeAt(j);
  }

  getAllInterfaceElements(): void {
    this.payload = { interfaceDefinitionId: this.viewDataInterfaceId, interfaceDatasetId: this.dataSetId };
    this.evvServiceService.getInterfaceElementsList(JSON.stringify(this.payload)).subscribe((res) => {
      console.log(res);
      this.InterfaceElementList = res;
      var data: any = res;
      this.pocDataSetList = data.pocDataSetList;
      this.InterfaceElementsDetailsList = data.interfaceElementsDetailsList;
      // let i = 0;
      // this.InterfaceElementsDetailsList.map(x => {
      //   x.pocDatasetId == 0 ? this.InterfaceElementsDetailsList[i].pocDatasetId = null : this.InterfaceElementsDetailsList[i].pocDatasetId = x.pocDatasetId;
      //   x.dataTypeId == 0 ? this.InterfaceElementsDetailsList[i].dataTypeId = null : this.InterfaceElementsDetailsList[i].dataTypeId = x.dataTypeId;
      //   x.dateFormatId == 0 ? this.InterfaceElementsDetailsList[i].dateFormatId = null : this.InterfaceElementsDetailsList[i].dateFormatId = x.dateFormatId;
      //   x.pocDatasetElementsId == 0 ? this.InterfaceElementsDetailsList[i].pocDatasetElementsId = null : this.InterfaceElementsDetailsList[i].pocDatasetElementsId = x.pocDatasetElementsId;
      //   x.interfaceStaticValuesId == 0 ? this.InterfaceElementsDetailsList[i].interfaceStaticValuesId = null : this.InterfaceElementsDetailsList[i].interfaceStaticValuesId = x.interfaceStaticValuesId;
      //   x.interfaceLookupId == 0 ? this.InterfaceElementsDetailsList[i].interfaceLookupId = null : this.InterfaceElementsDetailsList[i].interfaceLookupId = x.interfaceLookupId;
      //   i++;
      // })
      this.interfaceStaticValues = data.interfaceStaticValues;
      this.interfaceElementsId = data.interfaceElementsId;
      this.lookupArray = data.lookupList;
      this.pocDataSetElementsList = this.group(data.pocDataSetElementsList, 'pocDatasetId');
      this.pocDataSetElementsList.sort((a, b) => {
        return a[0].pocDatasetId - b[0].pocDatasetId;
      })

      console.log(this.pocDataSetElementsList);
    });
  }

  getAllInterfaceElementById(id): void {
    this.elementId = id;
    this.showUpdate = false;
    this.evvServiceService.getInterfaceElementById(JSON.stringify(id)).subscribe((res) => {
      const data = JSON.stringify(res);
      const newData = JSON.parse(data);
      this.lookupEle.interfaceDataset = newData.interfaceDatasetId;
      this.lookupEle.interfaceDefinition = newData.interfaceDefinitionId;
      this.InterfaceElementById = newData;
      if (newData.interfaceElementsDetailsList.length > 0) {
        this.getLength = newData.interfaceElementsDetailsList.length;
      }
      this.addScheduleOverrideForm = this.setUpScheduleOverrideForm(newData || {});
    });
  }

  saveInterfaceElement(): void {
    this.submitted = true;
    let id = this.elementId ? this.elementId : 0;
    let interfaceDefinitionId = this.lookupEle.interfaceDefinition;
    let interfaceDatasetId = this.lookupEle.interfaceDataset;
    const userId = this.userData.userId;
    const payload = this.addScheduleOverrideForm.value.openHours;
    if (this.addScheduleOverrideForm.valid) {
      this.evvServiceService.saveInterfaceElements(JSON.stringify(id), JSON.stringify(interfaceDefinitionId),
        JSON.stringify(interfaceDatasetId), JSON.stringify(userId), JSON.stringify(payload)).subscribe((resp) => {
          if (resp) {
            this.getAllInterfaceElements();
            Swal.fire({
              title: 'success',
              text: 'Saved successfully',
              icon: 'success',
              timer: 1200,
              showConfirmButton: false
            });
            id = 0;
            this.lookupEle = {};
            interfaceDefinitionId = null;
            interfaceDatasetId = null;
            this.showUpdate = true;
            this.submitted = false;
            this.reset();
          }
        });
    }
  }
  reset() {
    this.addScheduleOverrideForm = new UntypedFormGroup({
      openHours: new UntypedFormArray([this.createForm({})]),
    });
    this.addScheduleOverrideForm.reset();
  }
  getLookUpDataForElements(): void {
    this.evvServiceService.getLookUpDataForElements().subscribe((res) => {
      this.lookUpElementsData = res;
      let data: any = res;
      this.dataType = data.dataType;
      this.dateFormat = data.dateFormat;
      console.log(res)
    });
  }
  getPOCDataSets(): void {
    this.evvServiceService.getPOCDataSets().subscribe((res) => {
      console.log('lookup1', res)
      this.pocDataSets = res;
    });
  }

  getPOCElements(): void {
    this.evvServiceService.getPOCElements().subscribe((res) => {
      console.log('lookup2', res)
      this.pocElements = res;
      this.elementItems = this.pocElements;
    });
  }





  interfaceLookupInnit() {
    this.showType = 'lookupModify';
    this.getInterfaceLookUpLList();
  }

  public lookupPerPage = 10;
  public lookuplowerBound = 1;
  public lookupUpperBound = this.lookupPerPage;
  public lookupmaxCount: number = 0;

  public lookupperpageChange() {
    this.lookuplowerBound = 1;
    this.lookupUpperBound = this.lookupPerPage;
    this.getInterfaceLookUpLList();
  }


  public lookupPagenext(): void {
    this.lookuplowerBound = this.lookuplowerBound + this.perPage;
    this.lookupUpperBound = this.lookupUpperBound + this.perPage;
    console.log("************", this.lookupUpperBound, this.lookuplowerBound)
    this.getInterfaceLookUpLList();
  }
  public lookupPrevpage(): void {
    this.lookuplowerBound = this.lookuplowerBound - this.perPage;
    this.lookupUpperBound = this.lookupUpperBound - this.perPage;
    this.getInterfaceLookUpLList();

  }
  public getInterfaceLookUpLList() {
    console.log(this.viewDataInterfaceId)
    const payload = { interfaceDefinitionId: this.viewDataInterfaceId, lowerBound: this.lookuplowerBound, upperBound: this.lookupUpperBound }
    console.log(payload)
    this.evvServiceService.getInterfaceLookUp(JSON.stringify(payload)).subscribe((res) => {
      console.log("getInterfaceLookUpLList", res);
      let data: any = res;
      this.lookUpNameList = data.lookupList;
      this.lookupmaxCount = data.totalCount;
    });
  }




  public getLookupDetailsList(id, flag?: boolean) {
    let params = { "lookupId": id, "name": "", "active": "" }
    this.evvServiceService.getLookupDetailsList(JSON.stringify(params)).subscribe(
      res => {
        console.log(res);
        this.lookupDetailsList = res;
        console.log(this.pocLookupDetails)
        let i = 0;
        flag ? this.pocLookupDetails.map(x => {
          this.pocLookupDetails[i].interfaceLookupDetailsId = 0;
          i++;
        }) : ""
      }
    )
  }

  public editLookup(id) {
    this.lookupDisplay = [true, false, false];
    this.getLookupById(id);
  }
  fileupload() {
    $('#chooseFile').bind('change', function () {
      var filename = $("#chooseFile").val();
      if (/^\s*$/.test(filename)) {
        $(".file-upload").removeClass('active');
        $("#noFile").text("No file chosen...");
      }
      else {
        $(".file-upload").addClass('active');
        $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
      }
    });
  }
  public lookupList(id) {
    this.lookupDisplay = [false, true, true];
    console.log(id)
    this.lookUpId = id;
    this.cancellookupdetails();
    this.getLookupDetailsList(id);
  }


  public pocDatsetChange(event, index) {
    console.log(event)
    this.InterfaceElementsDetailsList[index].pocDatasetElementsId = null;
    if (event != undefined) {
      this.InterfaceElementsDetailsList[index].pocDatasetId = event.datasetId;
    }
  }

  public group(pocDataSetElementsList, key) {
    return [...pocDataSetElementsList.reduce((acc, o) =>
      acc.set(o[key], (acc.get(o[key]) || []).concat(o))
      , new Map).values()];
  }
  public pocDatasetClear(index) {
    console.log("Dataset clear");
    this.InterfaceElementsDetailsList[index].pocDatasetId = null;
    this.InterfaceElementsDetailsList[index].pocDatasetElementsId = null;
  }

  public addDatasetRows: number = 1
  public addDataset(i) {
    if (i > 0 && i < 100) {
      for (let j = 1; j <= i; j++) {
        let object = { "id": 0, "interfaceElementName": "", "pocDatasetId": null, "pocDatasetElementsId": null, "interfaceStaticValuesId": null, "interfaceDefaultValue": "", "dataTypeId": null, "dateFormatId": null, "interfaceLookupId": 0, "specialHandlerFunctionName": "", "length": null, "sortOrder": this.InterfaceElementsDetailsList.length+1, isChild: 0, isJsonArray: 0, isParent: 0, jsonKeyName: '', noOfEndBraces: null }

        this.InterfaceElementsDetailsList.push(object)
      }
      console.log(this.InterfaceElementsDetailsList)
    } else {
      Swal.fire({
        title: 'Invalid',
        text: 'Rows should be in between 1 and 99',
        icon: 'warning',
        // timer: 1200,
        // showConfirmButton: false
      });
    }

  }
  public removeDataset(j, item) {
    console.log(item)
    console.log(this.InterfaceElementsDetailsList[j])
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        if (item.id == 0) {
          this.InterfaceElementsDetailsList.splice(j, 1)
        }
        else {
          try {
            this.evvServiceService.deleteInterfaceElementsDetails(item.id).subscribe(
              res => {
                console.log(res);
                let data: any = res;
                if (data.deleteFlag == 1) {

                  this.InterfaceElementsDetailsList.splice(j, 1);
                  Swal.fire({
                    title: 'success',
                    text: 'Deleted successfully ',
                    icon: 'success',
                    // timer: 1200,
                    // showConfirmButton: false
                  });
                }
                else {
                  Swal.fire({
                    title: 'Error',
                    text: 'Failed to Delete ',
                    icon: 'error',
                    // timer: 1200,
                    // showConfirmButton: false
                  });
                }

              }
            )
          } catch (error) {

          }
        }
      }
      else if (result.dismiss === Swal.DismissReason.cancel) {
        console.log("not deleted")
      }
    })
  }
  public saveInterfaceElements() {
    console.log(this.InterfaceElementsDetailsList);
    let listerror = false;
    let arr = [];
    let i = 0
    this.InterfaceElementsDetailsList.map((x) => {
      console.log((x.pocDatasetId != null && (x.pocDatasetElementsId == null)), (x.dataTypeId == 3 && x.dateFormatId == null))
      if (x.interfaceElementName.trim().length == 0 || x.dataTypeId == null || x.sortOrder == null || (x.dataTypeId == 3 && x.dateFormatId == null) || (x.pocDatasetId != null && (x.pocDatasetElementsId == null))) {
        listerror = true;
      } else {
        x.interfaceStaticValuesId = x.interfaceStaticValuesId == null ? 0 : x.interfaceStaticValuesId;
        x.length = x.length == null || x.length == '' ? 0 : x.length;
        x.dateFormatId = x.dateFormatId == null ? 0 : x.dateFormatId;
        x.pocDatasetId = x.pocDatasetId == null ? 0 : x.pocDatasetId;
        x.noOfEndBraces = x.noOfEndBraces == null ? 0 : x.noOfEndBraces;
        x.pocDatasetElementsId = x.pocDatasetElementsId == null ? 0 : x.pocDatasetElementsId;


      }

      arr.push(x);
      i++;
    })
    if (!listerror) {
      this.evvServiceService.saveInterfaceElements(this.interfaceElementsId, this.viewDataInterfaceId,
        this.dataSetId, this.userData.userId, JSON.stringify(arr)).subscribe((resp) => {
          console.log(resp)
          let data: any = resp;
          console.log(data.saveMsg)

          if (data.saveFlag == 1) {

            this.getAllInterfaceElements();

            Swal.fire({
              title: 'success',
              text: 'Saved successfully',
              icon: 'success',
              timer: 1200,
              showConfirmButton: false
            });
            this.lookupEle = {};
            this.reset();
          } else {
            Swal.fire({
              title: 'Error',
              text: 'Failed to save ',
              icon: 'error',
              // timer: 1200,
              // showConfirmButton: false
            });
          }
        });
    } else {
      Swal.fire({
        title: 'Invalid Entry',
        text: 'All Mandatory Fields are Required ',
        icon: 'warning',
        // timer: 1200,
        // showConfirmButton: false
      });
    }

  }

  public lookupDataById: any = { "interfaceDefinitionId": 0, "sortOrder": null, "active": 1, "id": 0, "title": "", "lookupName": null };
  public getLookupById(id) {
    let params = { "id": id }
    this.evvServiceService.getLookupById(JSON.stringify(params)).subscribe(res => {
      console.log(res)
      this.lookupDataById = res;
    })
  }
  public saveLookup() {
    this.lookupSubmit = true;

    // {"id":0,"interfaceDefinitionId":4,"lookupName":"Test Lookup","title":"Test Lookup","sortOrder":1,"active":1,"userId":1}

    let parameters = {
      "id": this.lookupDataById.id,
      "interfaceDefinitionId": this.viewDataInterfaceId,
      "lookupName": this.lookupDataById.lookupName,
      "title": this.lookupDataById.title,
      "sortOrder": this.lookupDataById.sortOrder == null ? 0 : this.lookupDataById.sortOrder,
      "active": this.lookupDataById.active,
      "userId": this.userData.userId
    }
    console.log(parameters)

    if (this.lookupDataById.lookupName != null && this.lookupDataById.title != null) {

      if (this.lookupDataById.lookupName.trim().length > 0 && this.lookupDataById.title.trim().length > 0) {

        this.evvServiceService.saveLookup(JSON.stringify(parameters)).subscribe(
          res => {
            console.log(res);
            let Data: any = res;

            if (Data.saveFlag == 1) {
              Swal.fire({
                title: 'success',
                text: Data.saveMsg,
                icon: 'success',
                timer: 1200,
                showConfirmButton: false
              });
              this.lookupSubmit = false;
              this.getInterfaceLookUpLList();

              console.log(this.lookUpId)
            } else {
              Swal.fire({
                title: 'Failed',
                text: Data.saveMsg,
                icon: 'error',
                timer: 1200,
                showConfirmButton: false
              });
            }
            this.lookupDataById = { "interfaceDefinitionId": 0, "sortOrder": null, "active": 1, "id": 0, "title": "", "lookupName": null };
          })
      }
    }
  }

  public lookupDetailsById: any = { "name": null, "sortOrder": null, "title": null, "active": 1, "id": 0, "value": null };
  public lookUpId;
  public getLookupDetailsById(id) {
    this.savelookupdetailsError = false;
    let params = { "id": id }
    this.evvServiceService.getLookupDetailsById(JSON.stringify(params)).subscribe(
      res => {
        console.log(res)
        this.lookupDetailsById = res;
        this.lookupDetailsById.title = this.lookupDetailsById.name;
      }
    )
  }

  public saveLookupDetails() {
    this.savelookupdetailsError = true;
    let params = {
      "lookupId": this.lookUpId,
      "id": this.lookupDetailsById.id,
      "name": this.lookupDetailsById.name,
      "value": this.lookupDetailsById.value,
      "sortOrder": this.lookupDetailsById.sortOrder == null ? 0 : this.lookupDetailsById.sortOrder,
      "active": this.lookupDetailsById.active,
      "userId": this.userData.userId
    };
    console.log(params);
    if (this.lookupDetailsById.name != null && this.lookupDetailsById.value != null) {
      if (this.lookupDetailsById.name.trim().length > 0 && this.lookupDetailsById.value.trim().length > 0) {
        this.evvServiceService.saveLookupDetails(JSON.stringify(params)).subscribe(
          res => {
            console.log(res);
            this.savelookupdetailsError = false;
            this.lookupDetailsById = { "name": null, "sortOrder": null, "title": null, "active": 1, "id": 0, "value": null };
            const swalData: any = res;
            if (swalData.saveFlag == 1) {
              Swal.fire({
                title: 'success',
                text: swalData.saveMsg,
                icon: 'success',
                timer: 1200,
                showConfirmButton: false
              });
              console.log(this.lookUpId)
              this.getLookupDetailsList(this.lookUpId);
            } else {
              Swal.fire({
                title: 'Failed',
                text: swalData.saveMsg,
                icon: 'error',
                // timer: 1200,
                showConfirmButton: true
              });
            }


          }
        )
      }
    }
  }
  public cancellookup() {
    this.lookupSubmit = false;
    this.lookupDataById = { "interfaceDefinitionId": 0, "sortOrder": null, "active": 1, "id": 0, "title": "", "lookupName": null };

  }
  public cancellookupdetails() {
    this.savelookupdetailsError = false
    this.lookupDetailsById = { "name": null, "sortOrder": null, "title": null, "active": 1, "id": 0, "value": null };

  }

  //-------------------------------Export lookup start-------------------------------------------------------------//

  public pocLookupDetails: Array<any> = [];
  public interfaceLookupList: Array<any> = [];
  public interfaceLookupId: number = 0;
  public pocLookupId: number = 0;
  public lookupmappingSubmit: boolean = false;

  /**
      * function :  getExportPOCLookupList
       * purpose  : getting ExportPOCLookupList to display in the table
    **/

  public getExportPOCLookupList() {
    try {
      this.evvServiceService.getExportPOCLookupList().subscribe(
        res => {
          console.log(res)
          let data: any = res
          this.ExportPOCLookupList = data;
        }
      )
    } catch (error) {

    }
  }

  /**
    * function :  getExportPOCLookupListId
     * purpose  : getting ExportPOCLookupdetails to map
  **/

  public getExportPOCLookupDetailsById(id: number) {
    this.lookupmappingSubmit = false;
    this.pocLookupId = id;

    let obj = { "interfaceDefinitionId": this.viewDataInterfaceId, "pocLookupId": this.pocLookupId };
    try {
      this.evvServiceService.getExportPOCLookupDetailsById(JSON.stringify(obj)).subscribe(
        res => {
          let response: any = res;
          console.log(response)
          this.pocLookupDetails = response.pocLookupDetails;
          this.interfaceLookupList = response.interfaceLookupList;
          this.interfaceLookupId = response.interfaceLookupId;
          this.interfaceLookupId == 0 ? this.lookupDetailsList = [] : this.getLookupDetailsList(this.interfaceLookupId);

        }
      )
    } catch (error) {

    }

  }
  public onchange(index, event) {
    // this.pocLookupDetails[index].interfaceLookupDetailsId = event != undefined ? event.id : null;
    console.log(this.pocLookupDetails[index])

  }

  public saveExportLookupMapping() {
    console.log(this.interfaceLookupId)
    let arr = [];
    let errorFlag: boolean = false;
    this.lookupmappingSubmit = true;
    this.pocLookupDetails.map((x) => {
      if (x.interfaceLookupDetailsId == 0) {
        errorFlag = true;
      } else {
        let obj = {
          "pocLookupDetailsId": x.pocLookupDetailsId, "interfaceLookupDetailsId": x.interfaceLookupDetailsId == null ? 0 : x.interfaceLookupDetailsId
        }
        arr.push(obj);
      }

    })
    if (this.interfaceLookupId == 0 || errorFlag) {
      Swal.fire({
        title: 'Invalid',
        text: 'Please select all Interface Lookup fields',
        icon: 'warning',
        showConfirmButton: true
      });
    } else {

      let params = { "interfaceDefinitionId": this.viewDataInterfaceId, "pocLookupId": this.pocLookupId, "interfaceLookupId": this.interfaceLookupId, "userId": this.userData.userId, "pocLookupDetailsMapping": arr }
      console.log(params)
      try {
        this.evvServiceService.saveExportLookupMapping(JSON.stringify(params)).subscribe(
          res => {
            this.lookupmappingSubmit = false;
            let data: any = res;
            console.log(res)
            if (data.saveFlag == 1) {
              Swal.fire({
                title: 'success',
                text: data.saveMsg,
                icon: 'success',
                timer: 1200,
                showConfirmButton: false
              });
              // this.getExportPOCLookupDetailsById(this.pocLookupId)
              // this.pocLookupDetails=[];
              // this.interfaceLookupId=0;
            } else if (data.saveFlag == 0) {
              Swal.fire({
                title: 'Failed',
                text: 'Failed to save',
                icon: 'error',
                // timer: 1200,
                showConfirmButton: true
              });
            }
          }
        )
      } catch (error) {

      }

    }
  }
  //--------------------------------------export lookup end--------------------------------------//



  //-----------------------------------------document start---------------------------------------------

  public fileName: string = null;
  public fileUpload(event) {
    console.log(event.target.files.length)
    const file = event.target.files[0];
    this.fileName = event.target.files[0] != undefined ? file.name : null;
    if (event.target.files.length > 0) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        // console.log(reader.result);file
      };
    }

  }
  order: string = 'sortOrder';
  //-----------------------------------------document end----------------------------------------------------//
  public exportTableLookup = [];
  public getInterfaceExportedTables() {
    this.evvServiceService.getInterfaceExportedTables().subscribe(res => {
      this.exportTableLookup = res;
    })
  }
}
