import { DatePipe } from '@angular/common';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DashboardService } from 'src/app/services/dashboard.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-resubmittransaction-ps',
  templateUrl: './resubmittransaction-ps.component copy.html',
  styleUrls: ['./resubmittransaction-ps.component.scss']
})
export class ResubmittransactionPsComponent implements OnInit {
  public interfaceId = null;
  public interfaceList = [];
  public psId = null;
  public psList = [];
  public startDate: Date = new Date();
  public endDate: Date = new Date();
  public modalRef: BsModalRef;
  private appliedStartDate = '';
  private appliedEndDate = '';
  private appliedPsId = 0;
  private appliedInterface = 0;
  public lowerBound = 1;
  public upperBound = 100;
  public perPage = 100;
  public visitRequestData = [];

  public exportDetailsArray = [];
  public totalCount = 0;
  public defaultMessage = 0;
  public userData;
  public psIds = [];
  public visiitDetailsCheckedAll = false;





  constructor(private modalService: BsModalService, public dashboardservice: DashboardService, private datePipe: DatePipe) {
    this.userData = JSON.parse(sessionStorage.getItem('userData'));
    this.startDate.setDate(this.endDate.getDate() - 7);


  }

  ngOnInit(): void {
    this.getInterfaceByDataSet()
  }

  public getInterfaceByDataSet(): void {

    this.dashboardservice.getInterfaceByDataSet('PS').subscribe((res) => {
      let data: any = res;
      console.log(data)
      this.interfaceList = data;
    });
  }

  public getFilterDataByDataSet() {
    this.psId = null;
    this.dashboardservice.getFilterDataByDataSet(this.interfaceId, 'PS').subscribe(res => {
      console.log(res);
      let data: any = res;
      this.filterDataByDataSetResponse=res;
      if (this.includeInActivePS) {
        this.psList = data.psList;
    } else {
        this.psList = data.psList.filter(x => x.active == 1);
    }



    })
  }
  public onGo() {
    if (this.interfaceId == null) {
      Swal.fire({
        title: 'Invalid Search',
        text: 'Please select an Interface !',
        icon: 'warning',
        // timer: 1200,
        showConfirmButton: true
      });
    } else if (this.startDate != null && this.endDate != null) {
      let startDate = Date.parse(this.datePipe.transform(this.startDate, 'MM/dd/yyyy'));
      let endDate = Date.parse(this.datePipe.transform(this.endDate, 'MM/dd/yyyy'));
      if (startDate > endDate) {
        Swal.fire('', 'Start date cannot be greater than End date', 'warning')
      } else {
        this.appliedStartDate = this.datePipe.transform(this.startDate, 'MM/dd/yyyy')
        this.appliedEndDate = this.datePipe.transform(this.endDate, 'MM/dd/yyyy');
        this.appliedPsId =this.psId??0;
        this.appliedInterface = this.interfaceId??0;

        this.defaultMessage++;
        this.onPageReset();

      }
    } else {
      if (this.endDate == null || this.startDate == null) {
        Swal.fire({
          title: 'Invalid Dates',
          text: 'Start Date and End Date are Mandatory fields',
          icon: 'warning',
          // timer: 1200,
          showConfirmButton: true
        });
      }
    }
  }
  public getResubmitPSList() {
    let jsonObj = { "interfaceDefinitionId": this.appliedInterface, "psId": this.appliedPsId, "startDate": this.appliedStartDate, "endDate": this.appliedEndDate, "lowerBound": this.lowerBound, "upperBound": this.upperBound, "orderBy": this.orderBy, "order":this.orderType}

    try {
      this.psIds = [];
      this.visiitDetailsCheckedAll = false;
      this.dashboardservice.getResubmitPSList(JSON.stringify(jsonObj)).subscribe(res => {
        console.log(res);

        this.exportDetailsArray = res.exportDetailsArray;
        this.totalCount = res.totalCount;
      })
    } catch (error) {

    }
  }

  public onPageReset() {
    this.lowerBound = 1;
    this.upperBound = this.perPage;
    this.getResubmitPSList()
  }
  public prevpage(): void {
    this.lowerBound = this.lowerBound - this.perPage;
    this.upperBound = this.upperBound - this.perPage;
    this.getResubmitPSList();

  }
  public nextPage(): void {
    this.lowerBound = this.lowerBound + this.perPage;
    this.upperBound = this.upperBound + this.perPage;
    this.getResubmitPSList();

  }
  public repushPSRecords() {
    let jsonObject = { "interfaceDefinitionId": this.interfaceId, "psIds": this.psIds.toString(), "userId": this.userData.userId }

    try {
      this.dashboardservice.repushPSRecords(JSON.stringify(jsonObject)).subscribe(res => {
        console.log(res);
        if (res.saveFlag == '1') {
          Swal.fire('Success', res.saveMsg, 'success');
          this.getResubmitPSList();
          this.visiitDetailsCheckedAll = false;
        } else {
          Swal.fire('Failed', 'Failed to save Data', 'error');

        }
      })
    } catch (error) {

    }

  }
  public onvisitCheck(visitId, event) {
    if (event.target.checked) {
      this.psIds.push(visitId);
      console.log(this.psIds)

    } else {
      this.psIds = this.psIds.filter(x => x != visitId);
      console.log(this.psIds)
    }

  }
  public onCheckAll(event) {
    if (event.target.checked) {
      this.visiitDetailsCheckedAll = true;
      this.psIds = this.exportDetailsArray.map(x => {
        return x.psId
      })
    } else {
      this.visiitDetailsCheckedAll = false;
      this.psIds = [];
    }
  }
  public responseMessage = '';
  onresponse(temlate: TemplateRef<any>, messsage) {
    this.modalRef = this.modalService.show(temlate, {
      class: '  bd-example-modal-lg'
    })
    this.responseMessage = messsage

  }
  public getExportedDataDetails(data, recordDetails: TemplateRef<any>) {
    console.log(data)
    try {
      this.dashboardservice.getExportedDataDetails(data.id, data.interfaceDefinitionId, data.interfaceDatasetId, 'exported_ps_data').subscribe(
        res => {
          console.log(res);
          this.visitRequestData = res;
          this.modalRef = this.modalService.show(recordDetails);

        },
        err => {
          Swal.fire({
            title: 'Failed to Load',
            text: 'Something Went Wrong',
            icon: 'error',
            // timer: 1200,
            showConfirmButton: true
          });
        }
      )
    } catch (error) {

    }

  }
  public orderBy = '';
  public orderType = "";
  public currentorderby = "";
  public currentorderType = "";
  public sortorder(orderBy, orderType) {
    if (this.currentorderby != orderBy || this.currentorderType != orderType) {
      this.orderType = orderType;
      this.orderBy = orderBy;
      this.currentorderby = orderBy;
      this.currentorderType = orderType;
      this.onPageReset();
    }
  }


  public filterDataByDataSetResponse: any;
  public includeInActivePS = false;
  public onIncludeInactiveChange(event) {
    this.includeInActivePS = event;
    console.log(event);
    if (event) {
        this.psList = this.filterDataByDataSetResponse.psList;
    } else {
        this.psList = this.filterDataByDataSetResponse.psList.filter(x => x.active == 1);
    }


  }
}
