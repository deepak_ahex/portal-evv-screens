import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ResubmittransactionPsComponent } from './resubmittransaction-ps.component';

describe('ResubmittransactionPsComponent', () => {
  let component: ResubmittransactionPsComponent;
  let fixture: ComponentFixture<ResubmittransactionPsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ResubmittransactionPsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResubmittransactionPsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
