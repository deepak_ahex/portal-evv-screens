import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InterfaceDatasetComponent } from './interface-dataset.component';

describe('InterfaceDatasetComponent', () => {
  let component: InterfaceDatasetComponent;
  let fixture: ComponentFixture<InterfaceDatasetComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InterfaceDatasetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterfaceDatasetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
