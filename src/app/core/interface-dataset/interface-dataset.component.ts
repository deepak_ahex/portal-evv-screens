import { Component, OnInit, ViewChild } from '@angular/core';
import { EvvServiceService } from 'src/app/services/evv-service.service';
import Swal from 'sweetalert2';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators, PatternValidator, UntypedFormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-interface-dataset',
  templateUrl: './interface-dataset.component.html',
  styleUrls: ['./interface-dataset.component.scss']
})
export class InterfaceDatasetComponent implements OnInit {

  public userData: any;
  public showType = '';
  public exportTableLookup = [];
  public InterfaceDataSetDetailsList = [];
  public perPage = 10;
  public addForm: UntypedFormGroup;
  public InterfaceElementsDetailsList=[];
  public lookups: any = {
    dataflow: [],
    interfaceDefinition: [],
    datafileformat: [],
    datafiledelimeter: [],
    datamedium: []
  };
  public interfaceName: string = '';
  public interfaceId: number;
  public formError = false;

  constructor(private formBuilder: UntypedFormBuilder, public datePipe: DatePipe, public evvService: EvvServiceService) {
    this.getInterFaceDetails();
  }


  ngOnInit() {

    this.getLookupData();
  }
  /**
   * createForm
   */
  public createForm() {
    this.formError = false;
    this.addForm = this.formBuilder.group({
      id: new UntypedFormControl(0),
      interfaceDefinitionId: new UntypedFormControl(this.interfaceId),
      name: new UntypedFormControl(null, [Validators.required]),
      dataFlowId: new UntypedFormControl(null, Validators.required),
      dataMediumId: new UntypedFormControl(null),
      dataFileFormatId: new UntypedFormControl(null),
      dataFileDelimeterId: new UntypedFormControl(null),
      description: new UntypedFormControl(''),
      effectiveDate: new UntypedFormControl('',),
      interfaceExportedTable: new UntypedFormControl(null, Validators.required),
      isSyncProviderDetails: new UntypedFormControl(0),
      active: (1)
    });
  }

  get formcontrols() {
    return this.addForm.controls;
  }
  /**
   * getLookupData is used to call lookup feild in screen
   */
  public getLookupData() {
    try {
    const url = 'interface_definition,data_flow,data_medium,data_file_format,data_file_delimeter,data_type,date_format';

      this.evvService.getLookUpData(url).subscribe(res => {

        this.lookups = res;
        console.log(this.lookups);

      })
    } catch (error) {

    }
  }

  /**
   * getInterfaceExportedTables Method is used to export table lookup
   */
  public getInterfaceExportedTables() {

    try {
      this.evvService.getInterfaceExportedTables().subscribe(res => {
        this.exportTableLookup = res;
      })
    } catch (error) {

    }

  }
  /**
   * getInterfaceDatasetList method is used to get list
   */
  public getInterfaceDatasetList() {
    let data = { "name": "", "active": "", "interfaceDefinitionId": this.interfaceId }
    this.evvService.getInterfaceDetailsDataSetsList(JSON.stringify(data)).subscribe(res => {
      console.log(res);
      this.InterfaceDataSetDetailsList = res;
    })
  }

  /**
   * getInterfaceDataset method to get data of each individual dataset
   */
  public getInterfaceDataset(id) {
    try {
      this.evvService.getInterfaceDetailsDataSetById(id).subscribe(res => {
        this.addForm.get('id').setValue(res.id);
        this.addForm.get('interfaceDefinitionId').setValue(res.interfaceDefinitionId);
        this.addForm.get('name').setValue(res.name);
        this.addForm.get('dataFlowId').setValue(res.dataFlowId);
        this.addForm.get('dataMediumId').setValue(res.dataMediumId);
        this.addForm.get('dataFileFormatId').setValue(res.dataFileFormatId);
        this.addForm.get('dataFileDelimeterId').setValue(res.dataFileDelimeterId);
        this.addForm.get('description').setValue(res.description);
        this.addForm.get('effectiveDate').setValue(res.effectiveDate);
        this.addForm.get('interfaceExportedTable').setValue(res.interfaceExportedTable);
        this.addForm.get('isSyncProviderDetails').setValue(res.isSyncProviderDetails);
        this.addForm.get('active').setValue(res.active);
      })
    } catch (error) {

    }
  }
  public saveInterfaceDataset() {
    console.log(this.addForm.value)
    this.formError = true
    let obj = {
      active: this.addForm.value.active,
      dataFileDelimeterId: this.addForm.value.dataFileDelimeterId,
      dataFileFormatId: this.addForm.value.dataFileFormatId,
      dataFlowId: this.addForm.value.dataFlowId,
      dataMediumId: this.addForm.value.dataMediumId,
      description:this.addForm.value.description,
      effectiveDate:this.addForm.value.effectiveDate!=null&&this.addForm.value.effectiveDate!=''?this.datePipe.transform(this.addForm.value.effectiveDate,'MM/dd/yyyy'):'',
      id: this.addForm.value.id,
      interfaceDefinitionId: this.addForm.value.interfaceDefinitionId,
      interfaceExportedTable: this.addForm.value.interfaceExportedTable,
      isSyncProviderDetails: this.addForm.value.isSyncProviderDetails,
      name: this.addForm.value.name
    }
    console.log(obj)
    if (this.addForm.valid) {
      try {
        this.evvService.createInterfaceDataSetByUserId(this.userData.userId, JSON.stringify(obj)).subscribe(
          res => {
            console.log(res);
            let data: any = res;
            if (data.saveFlag == 1) {
              console.log(res)
              this.getInterfaceDatasetList();
              this.createForm();
              const swalData: any = res;
              Swal.fire({
                title: 'success',
                text: swalData.saveMsg,
                icon: 'success',
                timer: 1200,
                showConfirmButton: false
              });

            } else {
              Swal.fire({
                title: 'Error',
                text: data.saveMsg,
                icon: 'error',
                // timer: 1200,
                showConfirmButton: true
              });
            }
          });


      } catch (error) {

      }
    }
  }

  onfileFormatChange(value) {
    if (value == 1 || value == 2 || value == 5) {
    } else {
      this.addForm.get('dataFileDelimeterId').setValue(0)
    }
  }
  public getInterFaceDetails() {
    var data = JSON.parse(sessionStorage.getItem('interfaceData'));
    this.userData = JSON.parse(sessionStorage.getItem('userData'));

    // console.log(data.name)
    this.interfaceName = data?.label;
    this.interfaceId = data?.id;
    this.getInterfaceDatasetList();
    this.getInterfaceExportedTables();
    this.createForm();
  }





  public dataSetId;
  public Dataset;
  public viewDatasetElement(data: any) {
    console.log(data)
    this.dataSetId = data.id;
    this.Dataset = data.name
    console.log(this.dataSetId)
    this.getAllInterfaceElements();

    // this.showType = 'datasetElement';

  }
  public InterfaceElementList:any=[];
  public pocDataSetList=[]
  public interfaceStaticValues;
  public interfaceElementsId: number;
  public lookupArray: Array<any>;
  public pocDataSetElementsList = [];
  public addDatasetRows:number=1;

  getAllInterfaceElements(): void {
    let payload = { interfaceDefinitionId: this.interfaceId, interfaceDatasetId: this.dataSetId };
    this.evvService.getInterfaceElementsList(JSON.stringify(payload)).subscribe((res) => {
      console.log(res);
      this.InterfaceElementList = res;
      var data: any = res;
      this.pocDataSetList = data.pocDataSetList;
      this.InterfaceElementsDetailsList = data.interfaceElementsDetailsList;
      this.interfaceStaticValues = data.interfaceStaticValues;
      this.interfaceElementsId = data.interfaceElementsId;
      this.lookupArray = data.lookupList;
      this.pocDataSetElementsList = this.group(data.pocDataSetElementsList, 'pocDatasetId');
      this.pocDataSetElementsList.sort((a, b) => {
        return a[0].pocDatasetId - b[0].pocDatasetId;
      })

      console.log(this.pocDataSetElementsList);
    });
  }

  public group(pocDataSetElementsList, key) {
    return [...pocDataSetElementsList.reduce((acc, o) =>
      acc.set(o[key], (acc.get(o[key]) || []).concat(o))
      , new Map).values()];
  }


  public addDataset(i) {
    if (i > 0 && i < 100) {
      for (let j = 1; j <= i; j++) {
        let object = { "id": 0, "interfaceElementName": "", "pocDatasetId": null, "pocDatasetElementsId": null, "interfaceStaticValuesId": null, "interfaceDefaultValue": "", "dataTypeId": null, "dateFormatId": null, "interfaceLookupId": 0, "specialHandlerFunctionName": " ", "length": null, "sortOrder": null, isChild: 0, isJsonArray: 0, isParent: 0, jsonKeyName: '', noOfEndBraces: null }

        this.InterfaceElementsDetailsList.push(object)
      }
      console.log(this.InterfaceElementsDetailsList)
    } else {
      Swal.fire({
        title: 'Invalid',
        text: 'Rows should be in between 1 and 99',
        icon: 'warning',
        // timer: 1200,
        // showConfirmButton: false
      });
    }

  }
  public removeDataset(j, item) {

    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        if (item.id == 0) {
          this.InterfaceElementsDetailsList.splice(j, 1)
        }
        else {
          try {
            this.evvService.deleteInterfaceElementsDetails(item.id).subscribe(
              res => {
                console.log(res);
                let data: any = res;
                if (data.deleteFlag == 1) {

                  this.InterfaceElementsDetailsList.splice(j, 1);
                  Swal.fire({
                    title: 'success',
                    text: 'Deleted successfully ',
                    icon: 'success',
                    // timer: 1200,
                    // showConfirmButton: false
                  });
                }
                else {
                  Swal.fire({
                    title: 'Error',
                    text: 'Failed to Delete ',
                    icon: 'error',
                    // timer: 1200,
                    // showConfirmButton: false
                  });
                }

              }
            )
          } catch (error) {

          }
        }
      }
      else if (result.dismiss === Swal.DismissReason.cancel) {
        console.log("not deleted")
      }
    })
  }
  public pocDatasetClear(index) {
    console.log("Dataset clear");
    this.InterfaceElementsDetailsList[index].pocDatasetId = null;
    this.InterfaceElementsDetailsList[index].pocDatasetElementsId = null;
  }

  public pocDatsetChange(event, index) {
    console.log(event)
    this.InterfaceElementsDetailsList[index].pocDatasetElementsId = null;
    if (event != undefined) {
      this.InterfaceElementsDetailsList[index].pocDatasetId = event.datasetId;
    }
  }
  public saveInterfaceElements() {
    console.log(this.InterfaceElementsDetailsList);
    let listerror = false;
    let arr = [];
    let i = 0
    this.InterfaceElementsDetailsList.map((x) => {
      console.log((x.pocDatasetId != null && (x.pocDatasetElementsId == null)), (x.dataTypeId == 3 && x.dateFormatId == null))
      if (x.interfaceElementName.trim().length == 0 || x.dataTypeId == null || x.sortOrder == null || (x.dataTypeId == 3 && x.dateFormatId == null) || (x.pocDatasetId != null && (x.pocDatasetElementsId == null))) {
        listerror = true;
      } else {
        x.interfaceStaticValuesId = x.interfaceStaticValuesId == null ? 0 : x.interfaceStaticValuesId;
        x.length = x.length == null || x.length == '' ? 0 : x.length;
        x.dateFormatId = x.dateFormatId == null ? 0 : x.dateFormatId;
        x.pocDatasetId = x.pocDatasetId == null ? 0 : x.pocDatasetId;
        x.noOfEndBraces = x.noOfEndBraces == null ? 0 : x.noOfEndBraces;
        x.pocDatasetElementsId = x.pocDatasetElementsId == null ? 0 : x.pocDatasetElementsId;


      }

      arr.push(x);
      i++;
    })
    if (!listerror) {
      this.evvService.saveInterfaceElements(this.interfaceElementsId, this.interfaceId,
        this.dataSetId, this.userData.userId, JSON.stringify(arr)).subscribe((resp) => {
          console.log(resp)
          let data: any = resp;
          console.log(data.saveMsg)

          if (data.saveFlag == 1) {

            this.getAllInterfaceElements();

            Swal.fire({
              title: 'success',
              text: 'Saved successfully',
              icon: 'success',
              timer: 1200,
              showConfirmButton: false
            });
          } else {
            Swal.fire({
              title: 'Error',
              text: 'Failed to save ',
              icon: 'error',
              // timer: 1200,
              // showConfirmButton: false
            });
          }
        });
    } else {
      Swal.fire({
        title: 'Invalid Entry',
        text: 'All Mandatory Fields are Required ',
        icon: 'warning',
        // timer: 1200,
        // showConfirmButton: false
      });
    }

  }
}
