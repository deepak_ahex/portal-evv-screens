import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LaevvActivityLogComponent } from './laevv-activity-log.component';

describe('LaevvActivityLogComponent', () => {
  let component: LaevvActivityLogComponent;
  let fixture: ComponentFixture<LaevvActivityLogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LaevvActivityLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaevvActivityLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
