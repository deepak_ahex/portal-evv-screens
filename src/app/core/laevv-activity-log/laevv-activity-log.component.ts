import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-laevv-activity-log',
  templateUrl: './laevv-activity-log.component.html',
  styleUrls: ['./laevv-activity-log.component.scss']
})
export class LaevvActivityLogComponent implements OnInit {

  public modelRef: BsModalRef;
  public interfaceDatasetId = 0;
  public datsetFilter = [];
  public activityEndDate = new Date();
  public activityStartDate = new Date();
  public lowerBound = 1;
  public upperBound = 10;
  public perPage = 10;
  public activityLogList = [];
  public totalCount = 0;


  public headersArray = [];
  public exportDetailsArray = [];
  public attachment;
  public fileData = '';
  public fileName = '';

  constructor(public dashboardService: DashboardService, public modalService: BsModalService, private datePipe: DatePipe) {
    this.activityStartDate.setDate(new Date().getDate() - 7);

  }

  ngOnInit(): void {
    this.getLAEVVActivityList();
  }


  public getLAEVVActivityList() {
    try {
      let obj = { startDate: this.datePipe.transform(this.activityStartDate, 'MM/dd/yyyy'), endDate: this.datePipe.transform(this.activityEndDate, 'MM/dd/yyyy'), "lowerBound": this.lowerBound, "upperBound": this.upperBound };
      this.dashboardService.getLAEVVActivityList(JSON.stringify(obj)).subscribe(res => {
        console.log(res);


        this.totalCount = res.totalCount;
        this.activityLogList = res.laevvActivityList;
      })

    } catch (error) {

    }

  }

  public getBatchDetails(item, template) {
    try {
      this.dashboardService.getBatchDetailsLaEvv(item.requestedAttachmentId, item.laEvvBatchInfoId).subscribe(res => {
        console.log(res);
        this.headersArray = res.headersArray;
        this.exportDetailsArray = res.exportDetailsArray;
        this.attachment = res.attachment;
        this.fileData = this.attachment.fileData;
        this.fileName = this.attachment.fileName;
        this.modelRef = this.modalService.show(template, { class: "mx-w-95" });
      }, err => {
      })
    } catch (error) {

    }
  }

  public pageNext() {
    this.lowerBound = this.lowerBound + this.perPage;
    this.upperBound = this.upperBound + this.perPage;
    this.getLAEVVActivityList();
  }
  public pagePrev() {
    this.lowerBound = this.lowerBound - this.perPage;
    this.upperBound = this.upperBound - this.perPage;
    this.getLAEVVActivityList();

  }
  public perPageChange() {
    this.lowerBound = 1;
    this.upperBound = this.perPage;
    this.getLAEVVActivityList();
  }

}
