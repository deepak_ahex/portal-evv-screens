import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { WizardInterfaceComponent } from './wizard-interface.component';

describe('WizardInterfaceComponent', () => {
  let component: WizardInterfaceComponent;
  let fixture: ComponentFixture<WizardInterfaceComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ WizardInterfaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WizardInterfaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
