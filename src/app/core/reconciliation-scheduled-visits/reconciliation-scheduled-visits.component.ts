import { DatePipe } from '@angular/common';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DashboardService } from 'src/app/services/dashboard.service';
import { EvvServiceService } from 'src/app/services/evv-service.service';
import Swal from 'sweetalert2';
import { exportdetails } from '../reconcilation/reconcilation.component';

@Component({
  selector: 'app-reconciliation-scheduled-visits',
  templateUrl: './reconciliation-scheduled-visits.component copy.html',
  styleUrls: ['./reconciliation-scheduled-visits.component.scss']
})
export class ReconciliationScheduledVisitsComponent implements OnInit {
  @ViewChild('ps') ps;
  @ViewChild('dcs') dcs;
  @ViewChild('service') service;

  public defaultrecords = 0;
  public site: boolean;
  public exportDetailsArray: Array<exportdetails> = [];
  public interfaceDefinitionId: number = 0;
  public psId: number = null;
  public dcsId: number = null;
  public serviceId: number = null;
  public payorId: number = null;
  public exportStatus: number = null;
  public startDate: Date = new Date();
  public endDate: Date = new Date();
  public systemDate: Date = new Date();
  public perpage: number = 10
  public lowerBound: number = 1;
  public upperBound: number = this.perpage;
  public InterfaceDetailsList: Array<any> = [];
  public totalCount: number = 0;
  public payerList: Array<any> = [];
  public dcsList: Array<any> = [];
  public psList: Array<any> = [];
  public serviceList: Array<any> = [];
  public orderType: string = 'asc';
  public orderBy: string = "evd.scheduled_begin_date_time";
  public currentorderby: string = 'schedulestart';
  public modalRef: BsModalRef;
  public statusList=[{name:'Exported',id:1},{name:'Accepted',id:2},{name:'Rejected',id:3}]

  public visitRequestData = [];
  constructor(public dashboardservice: DashboardService, public evvServiceService: EvvServiceService, public datepipe: DatePipe, private modalService: BsModalService) {
    this.startDate.setDate(this.endDate.getDate() - 7);
  }


  ngOnInit(): void {
    // this.getReconciliationCompVisitList();

    this.getInterfaceByDataSet();
  }
  public applyfilter() {
    if (this.endDate == null || this.startDate == null) {
      Swal.fire({
        title: 'Invalid Dates',
        text: 'Start Date and End Date are Mandatory fields',
        icon: 'warning',
        // timer: 1200,
        showConfirmButton: true
      });
    } else
      if (this.interfaceDefinitionId == null) {
        Swal.fire({
          title: 'Invalid Search',
          text: 'Please select an Interface !',
          icon: 'warning',
          // timer: 1200,
          showConfirmButton: true
        });
      } else {
        this.defaultrecords++;
        this.pageChange();
      }
  }
  public getReconciliationSchdVisitList() {
    try {
      let params = { "interfaceDefinitionId": this.interfaceDefinitionId, "psId": this.psId??0, "dcsId": this.dcsId??0, "serviceId": this.serviceId??0, "payorId": this.payorId??0, "exportStatus": this.exportStatus??0, "startDate": this.datepipe.transform(this.startDate, 'MM/dd/yyyy'), "endDate": this.datepipe.transform(this.endDate, 'MM/dd/yyyy'), "lowerBound": this.lowerBound, "upperBound": this.upperBound, "orderBy": this.orderBy, "order": this.orderType }
      console.log(params)
      this.dashboardservice.getReconciliationSchdVisitList(JSON.stringify(params)).subscribe(res => {
        console.log(res)
        let data: any = res;
        this.exportDetailsArray = data.exportDetailsArray;
        this.totalCount = data.totalCount;
      })
    } catch (error) {

    }
  }
  public getFilterDataByDataSet() {
    this.dashboardservice.getFilterDataByDataSet(this.interfaceDefinitionId,'Scheduled Visits').subscribe(res => {
      console.log(res);

      this.filterDataByDataSetResponse = res;
      let data: any = res;

      if(this.includeInActivePS){
        this.psList = data.psList;
      }else{
        this.psList = data.psList.filter(x => x.active == 1);
      }

      if(this.includeInActiveDCS){
        this.dcsList = data.dcsList;
      }else{
        this.dcsList = data.dcsList.filter(x => x.active == 1);
      }

      this.serviceList = data.serviceList;
      this.payerList = data.PayerList;


    })
  }
  public getInterfaceByDataSet(): void {

    this.dashboardservice.getInterfaceByDataSet('Scheduled Visits').subscribe((res) => {
      let data: any = res;
      console.log(data)
      this.InterfaceDetailsList = data;
    });
  }
  public selectEvent(event, flag) {
    console.log(event)
    if (flag == 'ps') {
      this.psId = event.psId
    } else if (flag == 'dcs') {
      this.dcsId = event.dcsId;
    } else if (flag == "service") {
      this.serviceId = event.serviceId;
    }

  }

  public prevpage(): void {
    this.lowerBound = this.lowerBound - this.perpage;
    this.upperBound = this.upperBound - this.perpage;
    this.getReconciliationSchdVisitList();

  }
  public nextPage(): void {
    this.lowerBound = this.lowerBound + this.perpage;
    this.upperBound = this.upperBound + this.perpage;
    this.getReconciliationSchdVisitList();

  }
  public pageChange() {
    this.lowerBound = 1;
    this.upperBound = this.perpage;
    this.getReconciliationSchdVisitList();
  }
  public filterChange() {
    // this.psId != 0 ? this.ps.clear() : '';
    // this.dcsId != 0 ? this.dcs.clear() : '';
    // this.serviceId != 0 ? this.service.clear() : '';
    // this.payorId = 0;

    this.getFilterDataByDataSet();

  }
  public sortorder(orderBy, orderType) {
    this.orderType = orderType;
    this.currentorderby = orderBy;
    if (orderBy == 'ps') {
      this.orderBy = "evd.ps_name";
    } else
      if (orderBy == 'payor') {
        this.orderBy = "ipd.name"
      } else
        if (orderBy == 'schedulestart') {
          this.orderBy = "evd.scheduled_begin_date_time";
        } else if (orderBy == 'scheduleend') {
          this.orderBy = "evd.scheduled_end_date_time";
        }
    this.pageChange();
  }
  public datchange() {
    // console.log(this.endDate)
    console.log("startdate", this.datepipe.transform(this.startDate, 'MM/dd/yyyy'))
    console.log("enddate", this.datepipe.transform(this.endDate, 'MM/dd/yyyy'))
    console.log(this.endDate > this.systemDate)
    if (this.endDate != null && this.startDate != null) {
      if (this.endDate < this.startDate) {
        this.endDate = new Date(this.datepipe.transform(this.startDate, 'MM/dd/yyyy'))
      }
      if (this.endDate > this.systemDate) {
        this.endDate = new Date(this.datepipe.transform(this.systemDate, 'MM/dd/yyyy'))

      }
      if (this.startDate > this.systemDate) {
        this.startDate = new Date(this.datepipe.transform(this.systemDate, 'MM/dd/yyyy'))
      }
      if (this.startDate > this.endDate) {
        this.startDate = new Date(this.datepipe.transform(this.endDate, 'MM/dd/yyyy'))
      }
      console.log("finalstartdate", this.datepipe.transform(this.startDate, 'MM/dd/yyyy'))
      console.log("finalenddate", this.datepipe.transform(this.endDate, 'MM/dd/yyyy'))
    }
  }
 public  getExportedDataDetails(data, recordDetails: TemplateRef<any>) {
    console.log(data)
    try {
      this.dashboardservice.getExportedDataDetails(data.id, data.interfaceDefinitionId, data.interfaceDatasetId,'exported_schd_visits_data').subscribe(
        res => {
          console.log(res);
          this.visitRequestData = res;
          this.modalRef = this.modalService.show(recordDetails);

        },
        err=>{
          Swal.fire({
            title: 'Failed to Load',
            text: 'Something Went Wrong',
            icon: 'error',
            // timer: 1200,
            showConfirmButton: true
          });        }
      )
    } catch (error) {

    }

  }

  public includeInActiveDCS=false;
  public filterDataByDataSetResponse: any;
  public includeInActivePS = false;
  public onIncludeInactiveChange(event, flag) {
   flag=="PS"? this.includeInActivePS = event:this.includeInActiveDCS=event;
    console.log(event);
    if (event) {
      if (flag == 'PS') {
        this.psList = this.filterDataByDataSetResponse.psList;
      }else{
        this.dcsList = this.filterDataByDataSetResponse.dcsList;
      }
    } else {
      if (flag == 'PS') {
        this.psList = this.filterDataByDataSetResponse.psList.filter(x => x.active == 1);
      }else{
        this.dcsList = this.filterDataByDataSetResponse.dcsList.filter(x => x.active == 1);
      }

    }


  }
}
