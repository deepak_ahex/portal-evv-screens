import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ReconciliationScheduledVisitsComponent } from './reconciliation-scheduled-visits.component';

describe('ReconciliationScheduledVisitsComponent', () => {
  let component: ReconciliationScheduledVisitsComponent;
  let fixture: ComponentFixture<ReconciliationScheduledVisitsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ReconciliationScheduledVisitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReconciliationScheduledVisitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
