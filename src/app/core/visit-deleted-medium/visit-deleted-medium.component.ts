import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { DashboardService } from 'src/app/services/dashboard.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-visit-deleted-medium',
  templateUrl: './visit-deleted-medium.component.html',
  styleUrls: ['./visit-deleted-medium.component.scss']
})
export class VisitDeletedMediumComponent implements OnInit {
  public visitDeleteMediumList = [];

  public interfaceName: string = '';
  public interfaceId: number;
  public visitDetailForm: UntypedFormGroup;
  public formError: boolean = false;
  public datasetLookup = [];
  public userData: any;
  constructor(private _fb: UntypedFormBuilder, private dashboardService: DashboardService) {
    this.getInterFaceDetails();
  }
  ngOnInit(): void {
  }
  public createForm() {
    this.formError = false;
    this.visitDetailForm = this._fb.group({
      id: new UntypedFormControl(0),
      datasetId: new UntypedFormControl(null, [Validators.required]),
      url: new UntypedFormControl(null, [Validators.required]),
      userName: new UntypedFormControl(null, [Validators.required]),
      password: new UntypedFormControl(null, [Validators.required]),
      columnName: new UntypedFormControl(null, [Validators.required])
    })
  }
  public updateVisitDetailForm(obj) {
    console.log(obj)
    this.visitDetailForm.get('id').setValue(obj.id);
    this.visitDetailForm.get('datasetId').setValue(+(obj.interfaceDatasetId));
    this.visitDetailForm.get('url').setValue(obj.url);
    this.visitDetailForm.get('userName').setValue(obj.userName);
    this.visitDetailForm.get('columnName').setValue(obj.columnName);
    this.visitDetailForm.get('password').setValue(obj.password);


  }
  public getVisitDeletedMediumDetailsList() {
    try {
      this.dashboardService.getVisitDeletedMediumDetailsList(this.interfaceId).subscribe(
        res => {
          this.visitDeleteMediumList = res;
        }
      )
    } catch (error) {

    }
  }

  public saveVisitDetailForm() {
    this.formError = true;
    if (this.visitDetailForm.valid) {
      try {
        let obj = { "interfaceDefinitionId": this.interfaceId, "id": this.visitDetailForm.value.id, "interfaceDatasetId": this.visitDetailForm.value.datasetId, "url": this.visitDetailForm.value.url, "userName": this.visitDetailForm.value.userName, "password": this.visitDetailForm.value.password, "columnName": this.visitDetailForm.value.columnName, "active": this.visitDetailForm.value.active, "userId": this.userData.userId }

        this.dashboardService.saveVisitDeleteMediumDetails(obj).subscribe(res => {
          console.log(res)
          let data: any = res;
            if (data.saveFlag == 1) {
              Swal.fire({
                title: 'success',
                text: data.saveMsg,
                icon: 'success',
                timer: 1200,
                showConfirmButton: false
              });
              this.getVisitDeletedMediumDetailsList();
              this.createForm();
            } else {
              Swal.fire({
                title: 'Failed',
                text: data.saveMsg,
                icon: 'error',
                timer: 1200,
                showConfirmButton: false
              });
            }
        })
      } catch (error) {

      }
    }
  }
  get formcontrols() {
    return this.visitDetailForm.controls;
  }
  public getInterFaceDetails() {
    var data = JSON.parse(sessionStorage.getItem('interfaceData'));
    this.userData = JSON.parse(sessionStorage.getItem('userData'));
    // console.log(data.name)
    this.interfaceName = data?.label;
    this.interfaceId = data?.id;
    this.createForm();
    this.getInterfaceDatasetList();
    this.getVisitDeletedMediumDetailsList();
  }
  public getInterfaceDatasetList() {
    try {
      this.dashboardService.getInterfaceDatasetList(this.interfaceId).subscribe(
        res => {
          console.log(res);
          this.datasetLookup = res;
        }
      )
    } catch (error) {

    }
  }
}
