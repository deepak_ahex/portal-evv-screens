import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { VisitDeletedMediumComponent } from './visit-deleted-medium.component';

describe('VisitDeletedMediumComponent', () => {
  let component: VisitDeletedMediumComponent;
  let fixture: ComponentFixture<VisitDeletedMediumComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitDeletedMediumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitDeletedMediumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
